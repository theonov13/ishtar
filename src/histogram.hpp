/*
 * Contains a histogram evaluation class
 * -> make histogram of data vector
 * -> compute progression of mean value / error of a Markov chain
 * -> compute progression and take into account the sign --> r = <OS>/<S>
 * 
 * */



class histogram
{
public:
	
	// Print a histogram with n_bins of 'data' into the datafile 's'
	void make_histogram(std::vector<double> *data, int n_bins, std::string s);	
	
	// Print the progression (mean value and error as a function of the number of elements) of 'data' into the datafile 's'
	void progression(std::vector<double> *data, std::string s);
	
	// Print the progression of the fermionic expectation value <O> = <OS>/<S> into the datafile 's'
	void progression_sign(std::vector<double> *data, std::vector<double> *sign, std::string s);
	
};





void histogram::progression_sign(std::vector< double >* data, std::vector< double >* sign, std::string s)
{
	// open datafile 's'
	std::fstream f;
	f.open( s.c_str(), std::ios::out );
	f.precision( 20 );
	
	// Create temporary empty vectors
	std::vector<double> tmp_data;
	std::vector<double> tmp_sign;

	
	for(int i=0;i<(*data).size();i++) // loop over all elements of the data vector
	{
		double my_result, my_error;
		
		// Add the current elements to data and sign
		tmp_data.push_back( (*data)[i] );
		tmp_sign.push_back( (*sign)[i] );
		
		// Create error evaluation object
		error err( &tmp_sign );
		
		// Obtain mean value and error from OS and S
		double data_mean = err.get_mean( &tmp_data );
		double sign_mean = err.get_mean( &tmp_sign );
		
		double data_sigma = err.get_sigma( &tmp_data ) / sqrt( double( tmp_data.size() ) );
		double sign_sigma = err.get_sigma ( &tmp_sign ) / sqrt( double (tmp_sign.size() ) );
		
		// Get fermionic expectation value <OS>/<S> and the error including cross correlations:
		err.get_result(&tmp_data, &my_result, &my_error);
		
		// Print all results from the Markov chain with 'i' elements into 's'
		f << i << "\t" << my_result << "\t" << my_error << "\t" << data_mean << "\t" << data_sigma << "\t" << sign_mean << "\t" << sign_sigma << "\n";
	}
		
	f.close();
	
}





void histogram::progression(std::vector<double> *data, std::string s)
{
	// open datafile 's'
	std::fstream f;
	f.open( s.c_str(), std::ios::out );
	
	double ans = 0.0; // current mean value
	std::vector<double> tmp; // temporary Markov chain vector
	error err(&tmp); // Create error evaluation object
	
	for(int i=0;i<(*data).size();i++) // loop over all elements in 'data'
	{
		tmp.push_back( (*data)[i] ); // add current element to the Markov chain
		ans += (*data)[i]; // update the mean value
		double sigma = err.get_sigma( &tmp ); // obtain the current error
		f << i << "\t" << ans / double(i+1) << "\t" << sigma / sqrt( double(i+1) ) << "\n";
	}
	
	f.close();
	
	
}


void histogram::make_histogram(std::vector<double> *data, int n_bins, std::string s)
{
	// find maximum and minimum value:
	double min_value = (*data)[0];
	double max_value = min_value;
	
// 	std::cout << "min-start: " << min_value << "\t max-start: " << max_value << std::endl;
	int z=0;
	
	for(auto c=(*data).begin();c!=(*data).end();c++)
	{
		if( (*c) < min_value ) min_value = (*c);
		if( (*c) > max_value ) max_value = (*c);
// 		std::cout << "z: " << z << "\t (*c): " << (*c) << "\n";
		z++;
	}
	
	std::cout << "Make-hist, min_value: " << min_value << "\t max_value: " << max_value << std::endl;
	
	
	// calculate bin width:
	double bin_width = (max_value - min_value) / double(n_bins);
	
	// create histogram vector:
	std::vector<double> my_hist( n_bins, 0.0 );
	
	for(auto c=(*data).begin();c!=(*data).end();c++)
	{
		// calculate the bin index for each value
		int my_bin = int( ( (*c) - min_value ) / bin_width );
		my_hist[my_bin] += 1.0;
	}
	
	// write the histogram into a datafile 's'
	std::fstream f;
	f.open( s.c_str(), std::ios::out );
	
	for(int i=0;i<n_bins;i++)
	{
		f << min_value + ( i + 0.50 )*bin_width << "\t" << my_hist[i] / ( (*data).size() * bin_width ) << "\n";
	}
	
// 	exit(0);
	
}
