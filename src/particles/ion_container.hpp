#pragma once


#include <vector>
#include "ion_species.hpp"


class ion_container
{

public:

    explicit ion_container(int n_part) : n_ions(n_part), ions(n_part){};
    ion_container(): n_ions(0), ions() {};
    void add_ion(ion_species& part);
    void remove_ion(int idx);
    ion_species& get_ion(int idx);
    int get_total_ion_charges();
    void print_ion_positions();
    int get_num_ions();

private:
    std::vector<ion_species> ions;
    int n_ions;

};


void ion_container::add_ion(ion_species &part)
{
    ions.push_back(part);
    n_ions += 1;
}


void ion_container::remove_ion(int idx)
{
    ions.erase(ions.begin() + idx);
    n_ions -= 1;

}


ion_species& ion_container::get_ion(int idx)
{
    return ions[idx];
}


int ion_container::get_total_ion_charges()
{
    int total_charge = 0;
    for (auto i : ions)
    {
        total_charge += i.get_charge();
    }
    return total_charge;
}

void ion_container::print_ion_positions()
{
    for (auto i: ions)
    {
        i.print();
    }
}

int ion_container::get_num_ions()
{
    return n_ions;
}