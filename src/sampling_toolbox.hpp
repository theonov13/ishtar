/*
 * Contains the class: sampling_toolbox
 * 
 */


template <class inter> class sampling_toolbox
{
public:
	
	// Initialize the sampling sampling_toolbox
	void init(Parameters* new_p);
	
	// Sample m new beads by connecting fixed endpoints new_beads[0] and new_beads[1+m];
	void connect(std::vector<Bead>*new_beads, int m);
	
	
	// Sample m beads by connecting fixed endpoints new_beads[0] and new_beads[1+m] in PBC
	// - the end point image is choosen automatically as the nearest
	// - return value is the product of sampling probs
	double PBC_connect( std::vector<Bead>*new_beads, int m );
	double trap_connect( std::vector<Bead>*new_beads, int m );
	
	
	double PBC_connect( std::vector<Bead>*new_beads, int m, std::vector<double> &probs );
	double trap_connect( std::vector<Bead>*new_beads, int m, std::vector<double> &probs );
	
	
	
	
	
	// Return the total prob to sample the particular m beads between the 'fixed' endpoints in new_beads;
	double PBC_backward_connect( std::vector<Bead>*new_beads, int m ); 
	double trap_backward_connect( std::vector<Bead>*new_beads, int m );
	
	
	// Return the total prob to sample the particular m beads between the 'fixed' endpoints in new_beads;
	double PBC_backward_connect( std::vector<Bead>*new_beads, int m, std::vector<double> &probs ); 
	double trap_backward_connect( std::vector<Bead>*new_beads, int m, std::vector<double> &probs );
	
	
	// Return the total prob to sample the particular m beads between the 'fixed' endpoints in new_beads;
	double debug_PBC_backward_connect(bool debug, std::vector<Bead>*new_beads, int m); 
	
	
	
	// A connect method which simultaneously computed both the sampling prob and the reverse sampling prob to perform as much cancelation as possible on the fly
	double combined_PBC_connect( std::vector<Bead>* new_beads, int m );
	double combined_trap_connect( std::vector<Bead>* new_beads, int m );
	
	
	
		// For the wriggle update, return the ratio of new_diffusion_elements to forward-sampling-prob
	double wriggle_PBC_connect( std::vector<Bead>* new_beads, int m );
	double wriggle_trap_connect( std::vector<Bead>* new_beads, int m );
	
	double reverse_wriggle_PBC_connect( std::vector<Bead>* new_beads, int m );
	double reverse_wriggle_trap_connect( std::vector<Bead>* new_beads, int m );
	
	
	
	
	
	// Special functions for standard PIMC
	
	// Generate the position of the new head and return the sampling probability
	double sample_new_head( Bead* tail, Bead* head );
	
	// Get the sampling prob. for an exisitng head
	double reverse_head_prob( Bead* tail, Bead* head );
	
	
	
	
	// sample m new beads between new_beads[0] and new_beads[1+m], return 1 / rho( 0, m+1, tau )
	double standard_PIMC_trap_connect( std::vector<Bead> *new_beads, int m );
	
	
	
private:
	Parameters *p;
	inter my_interaction;
	
};



template <class inter> void sampling_toolbox<inter>::init(Parameters* new_p)
{
	p = new_p;
	my_interaction.init( p );
}


























// For the Wriggle update
// To be called with the "old" trajectory, with the head being yet unchanged
// Returns the ratio of the old diffusion_elements to the "reverse" sampling prob




// m is the total number of beads to be resampled, we do not have to start/end with main slice
template <class inter> double sampling_toolbox<inter>::reverse_wriggle_PBC_connect( std::vector<Bead>*new_beads, int m )
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[1+m].get_real_time();
 
	// index of the last (fixed) bead
	int m_index = m + 1;
	
	double p_tot = 1.0; // Initialize the prob. to sample the total path
	
	
	Bead last_bead;
	last_bead.copy( &(*new_beads)[ 0 ] );
	
 
	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		
		tau_i = (*new_beads)[i].get_real_time();
		tau_i_minus = (*new_beads)[i-1].get_real_time();
		
 		
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);
		
		double sigma_sample = sqrt(tau_sample);
		
		double p_i = 1.0; // Initialize the sampling prob. for point i
		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			
			
			double x_minus = (*new_beads)[i-1].get_coord(iDim);
			
			// First select the nearest image as the end point of the trajectory:
			double x_image = (*new_beads)[m_index].get_coord(iDim);
			
			while( (x_image - x_minus) > 0.50*(*p).length )
			{
				x_image = x_image - (*p).length;
			}
			
			while( (x_image - x_minus) < -0.50*(*p).length )
			{
				x_image = x_image + (*p).length;
			}

			
			// Also select the nearest image of the old coords
			double x_image_reverse = (*new_beads)[m_index].get_coord(iDim);
			double x_minus_reverse = last_bead.get_coord(iDim);
			
			while( (x_image_reverse - x_minus_reverse) > 0.50*(*p).length )
			{
				x_image_reverse = x_image_reverse - (*p).length;
			}
			
			while( (x_image_reverse - x_minus_reverse) < -0.50*(*p).length )
			{
				x_image_reverse = x_image_reverse + (*p).length;
			}
			
			
			
			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_image * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			double xi_reverse = last_bead.get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_image_reverse * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			
			
			
			
			// Sample with a gaussian around xi:
// 			double new_pos = xi + gaussian()*sigma_sample
			double new_pos = (*new_beads)[i].get_coord(iDim);
			
			// Adjust the new coordinate, e.g. for PBC in homogeneous systems
// 			new_pos = my_interaction.adjust_coordinate( new_pos );
			
			double sample_prob = 0.0;
			for(int n = -(*p).n_boxes;n<(*p).n_boxes+1;n++)
			{
				double delta_r = new_pos - xi + n*(*p).length;
				sample_prob += exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
				
// 				double delta_r_reverse = (*new_beads)[i].get_coord( iDim ) - xi_reverse + n*(*p).length; 
// 				sample_prob_reverse += exp( -delta_r_reverse * delta_r_reverse / ( 2.0 * sigma_sample*sigma_sample ) );
			}
			
			
			
			// Update the coordinate of the affected bead
// 			last_bead.set_coord( iDim, (*new_beads)[i].get_coord( iDim ) );
// 			(*new_beads)[i].set_coord( iDim, new_pos );
			
			
			
			
			sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );
// 			sample_prob_reverse = sample_prob_reverse
			
			/*
			std::cout << "x_image: " << x_image << "\t x_image_reverse: " << x_image_reverse << "\t pos: " << (*new_beads)[i].get_coord(iDim) << "\t new_pos: " << new_pos << "\n";
			std::cout << "i: " << i << "\t p_i: " << p_i << "\t sample_prob: " << sample_prob << "\t sample_prob_reverse: " << sample_prob_reverse << "\n";
			*/
			
			p_i = p_i  / sample_prob;
						
			
			
		} // end loop idim
		
// 		double new_diffusion = my_interaction.rho( &(*new_beads)[i-1], &(*new_beads)[i], 1.0 );
// 		(*new_beads)[i-1].set_diffusion_element( new_diffusion );
		double old_diffusion = (*new_beads)[i-1].get_diffusion_element();
		
		p_tot = p_tot * p_i * old_diffusion;

	} // end loop all new coords. 
	
	
	return p_tot*(*new_beads)[m].get_diffusion_element();
	
}












// For the Wriggle update
// To be called with the "old" trajectory, with the head being yet unchanged
// Returns the ratio of the old diffusion_elements to the "reverse" sampling prob




// m is the total number of beads to be resampled, we do not have to start/end with main slice
template <class inter> double sampling_toolbox<inter>::reverse_wriggle_trap_connect( std::vector<Bead>*new_beads, int m )
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[1+m].get_real_time();
 
	// index of the last (fixed) bead
	int m_index = m + 1;
	
	double p_tot = 1.0; // Initialize the prob. to sample the total path
	
	
	Bead last_bead;
	last_bead.copy( &(*new_beads)[ 0 ] );
	
 
	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		
		tau_i = (*new_beads)[i].get_real_time();
		tau_i_minus = (*new_beads)[i-1].get_real_time();
		
 		
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);
		
		double sigma_sample = sqrt(tau_sample);
		
		double p_i = 1.0; // Initialize the sampling prob. for point i
		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			
			
			double x_minus = (*new_beads)[i-1].get_coord(iDim);
			
			// First select the nearest image as the end point of the trajectory:
			double x_image = (*new_beads)[m_index].get_coord(iDim);
			
			/*
			while( (x_image - x_minus) > 0.50*(*p).length )
			{
				x_image = x_image - (*p).length;
			}
			
			while( (x_image - x_minus) < -0.50*(*p).length )
			{
				x_image = x_image + (*p).length;
			}
			*/

			
			// Also select the nearest image of the old coords
			double x_image_reverse = (*new_beads)[m_index].get_coord(iDim);
			double x_minus_reverse = last_bead.get_coord(iDim);
			
			/*
			while( (x_image_reverse - x_minus_reverse) > 0.50*(*p).length )
			{
				x_image_reverse = x_image_reverse - (*p).length;
			}
			
			while( (x_image_reverse - x_minus_reverse) < -0.50*(*p).length )
			{
				x_image_reverse = x_image_reverse + (*p).length;
			}
			*/
			
			
			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_image * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			double xi_reverse = last_bead.get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_image_reverse * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			
			
			
			
			// Sample with a gaussian around xi:
// 			double new_pos = xi + gaussian()*sigma_sample
			double new_pos = (*new_beads)[i].get_coord(iDim);
			
			// Adjust the new coordinate, e.g. for PBC in homogeneous systems
// 			new_pos = my_interaction.adjust_coordinate( new_pos );
			
			double sample_prob = 0.0;
// 			for(int n = -(*p).n_boxes;n<(*p).n_boxes+1;n++)
// 			{
				double delta_r = new_pos - xi;
				sample_prob += exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
				
// 				double delta_r_reverse = (*new_beads)[i].get_coord( iDim ) - xi_reverse + n*(*p).length; 
// 				sample_prob_reverse += exp( -delta_r_reverse * delta_r_reverse / ( 2.0 * sigma_sample*sigma_sample ) );
// 			}
			
			
			
			// Update the coordinate of the affected bead
// 			last_bead.set_coord( iDim, (*new_beads)[i].get_coord( iDim ) );
// 			(*new_beads)[i].set_coord( iDim, new_pos );
			
			
			
			
// 			sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );
// 			sample_prob_reverse = sample_prob_reverse
			
			/*
			std::cout << "x_image: " << x_image << "\t x_image_reverse: " << x_image_reverse << "\t pos: " << (*new_beads)[i].get_coord(iDim) << "\t new_pos: " << new_pos << "\n";
			std::cout << "i: " << i << "\t p_i: " << p_i << "\t sample_prob: " << sample_prob << "\t sample_prob_reverse: " << sample_prob_reverse << "\n";
			*/
			
			p_i = p_i  / sample_prob;
						
			
			
		} // end loop idim
		
// 		double new_diffusion = my_interaction.rho( &(*new_beads)[i-1], &(*new_beads)[i], 1.0 );
// 		(*new_beads)[i-1].set_diffusion_element( new_diffusion );
		double old_diffusion = (*new_beads)[i-1].get_diffusion_element();
		
		p_tot = p_tot * p_i * old_diffusion;

	} // end loop all new coords. 
	
	
	return p_tot*(*new_beads)[m].get_diffusion_element();
	
}



































// For the Wriggle update. Sample m new beads in between. 
// Also, compute the ration of new diffusion elements to the new sampling prob
// Finally, set the diffusion elements in the new_beads vector


// m is the total number of beads to be resampled, we do not have to start/end with main slice
template <class inter> double sampling_toolbox<inter>::wriggle_PBC_connect( std::vector<Bead>*new_beads, int m )
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[1+m].get_real_time();
 
	// index of the last (fixed) bead
	int m_index = m + 1;
	
	double p_tot = 1.0; // Initialize the prob. to sample the total path
	
	
	Bead last_bead;
	last_bead.copy( &(*new_beads)[ 0 ] );
	
 
	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		
		tau_i = (*new_beads)[i].get_real_time();
		tau_i_minus = (*new_beads)[i-1].get_real_time();
		
 		
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);
		
		double sigma_sample = sqrt(tau_sample);
		
		double p_i = 1.0; // Initialize the sampling prob. for point i
		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			
			
			double x_minus = (*new_beads)[i-1].get_coord(iDim);
			
			// First select the nearest image as the end point of the trajectory:
			double x_image = (*new_beads)[m_index].get_coord(iDim);
			
			while( (x_image - x_minus) > 0.50*(*p).length )
			{
				x_image = x_image - (*p).length;
			}
			
			while( (x_image - x_minus) < -0.50*(*p).length )
			{
				x_image = x_image + (*p).length;
			}

			
			// Also select the nearest image of the old coords
			double x_image_reverse = (*new_beads)[m_index].get_coord(iDim);
			double x_minus_reverse = last_bead.get_coord(iDim);
			
			while( (x_image_reverse - x_minus_reverse) > 0.50*(*p).length )
			{
				x_image_reverse = x_image_reverse - (*p).length;
			}
			
			while( (x_image_reverse - x_minus_reverse) < -0.50*(*p).length )
			{
				x_image_reverse = x_image_reverse + (*p).length;
			}
			
			
			
			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_image * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			double xi_reverse = last_bead.get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_image_reverse * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			
			
			
			
			// Sample with a gaussian around xi:
			double new_pos = xi + gaussian()*sigma_sample;
			
			// Adjust the new coordinate, e.g. for PBC in homogeneous systems
			new_pos = my_interaction.adjust_coordinate( new_pos );
			
			double sample_prob = 0.0;
			for(int n = -(*p).n_boxes;n<(*p).n_boxes+1;n++)
			{
				double delta_r = new_pos - xi + n*(*p).length;
				sample_prob += exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
				
			}
			
			
			
			// Update the coordinate of the affected bead
			last_bead.set_coord( iDim, (*new_beads)[i].get_coord( iDim ) );
			(*new_beads)[i].set_coord( iDim, new_pos );
			
			
			
			
 			sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );
// 			sample_prob_reverse = sample_prob_reverse
			
			/*
			std::cout << "x_image: " << x_image << "\t x_image_reverse: " << x_image_reverse << "\t pos: " << (*new_beads)[i].get_coord(iDim) << "\t new_pos: " << new_pos << "\n";
			std::cout << "i: " << i << "\t p_i: " << p_i << "\t sample_prob: " << sample_prob << "\t sample_prob_reverse: " << sample_prob_reverse << "\n";
			*/
			
			p_i = p_i  / sample_prob;
						
			
			
		} // end loop idim
		
		double new_diffusion = my_interaction.rho( &(*new_beads)[i-1], &(*new_beads)[i], 1.0 );
		(*new_beads)[i-1].set_diffusion_element( new_diffusion );
		
		p_tot = p_tot * p_i * new_diffusion;

	} // end loop all new coords. 
	
	
	double final_diffusion_element = my_interaction.rho( &(*new_beads)[m], &(*new_beads)[1+m], 1.0 );
	(*new_beads)[m].set_diffusion_element( final_diffusion_element );
	
	return p_tot*final_diffusion_element;
	
}










// m is the total number of beads to be resampled, we do not have to start/end with main slice
template <class inter> double sampling_toolbox<inter>::wriggle_trap_connect( std::vector<Bead>*new_beads, int m )
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[1+m].get_real_time();
 
	// index of the last (fixed) bead
	int m_index = m + 1;
	
	double p_tot = 1.0; // Initialize the prob. to sample the total path
	
	
	Bead last_bead;
	last_bead.copy( &(*new_beads)[ 0 ] );
	
 
	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		
		tau_i = (*new_beads)[i].get_real_time();
		tau_i_minus = (*new_beads)[i-1].get_real_time();
		
 		
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);
		
		double sigma_sample = sqrt(tau_sample);
		
		double p_i = 1.0; // Initialize the sampling prob. for point i
		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			
			
			double x_minus = (*new_beads)[i-1].get_coord(iDim);
			
			// First select the nearest image as the end point of the trajectory:
			double x_image = (*new_beads)[m_index].get_coord(iDim);
			
			/*
			while( (x_image - x_minus) > 0.50*(*p).length )
			{
				x_image = x_image - (*p).length;
			}
			
			while( (x_image - x_minus) < -0.50*(*p).length )
			{
				x_image = x_image + (*p).length;
			}
			*/

			
			// Also select the nearest image of the old coords
			double x_image_reverse = (*new_beads)[m_index].get_coord(iDim);
			double x_minus_reverse = last_bead.get_coord(iDim);
			
			/*
			while( (x_image_reverse - x_minus_reverse) > 0.50*(*p).length )
			{
				x_image_reverse = x_image_reverse - (*p).length;
			}
			
			while( (x_image_reverse - x_minus_reverse) < -0.50*(*p).length )
			{
				x_image_reverse = x_image_reverse + (*p).length;
			}
			*/
			
			
			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_image * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			double xi_reverse = last_bead.get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_image_reverse * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			
			
			
			
			// Sample with a gaussian around xi:
			double new_pos = xi + gaussian()*sigma_sample;
			
			// Adjust the new coordinate, e.g. for PBC in homogeneous systems
			new_pos = my_interaction.adjust_coordinate( new_pos );
			
			double sample_prob = 0.0;
// 			for(int n = -(*p).n_boxes;n<(*p).n_boxes+1;n++)
// 			{
			double delta_r = new_pos - xi;
			sample_prob += exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );

// 			}
			
			
			
			// Update the coordinate of the affected bead
			last_bead.set_coord( iDim, (*new_beads)[i].get_coord( iDim ) );
			(*new_beads)[i].set_coord( iDim, new_pos );
			
			
			
			
// 			sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );
// 			sample_prob_reverse = sample_prob_reverse
			
			/*
			std::cout << "x_image: " << x_image << "\t x_image_reverse: " << x_image_reverse << "\t pos: " << (*new_beads)[i].get_coord(iDim) << "\t new_pos: " << new_pos << "\n";
			std::cout << "i: " << i << "\t p_i: " << p_i << "\t sample_prob: " << sample_prob << "\t sample_prob_reverse: " << sample_prob_reverse << "\n";
			*/
			
			p_i = p_i  / sample_prob;
						
			
			
		} // end loop idim
		
		double new_diffusion = my_interaction.rho( &(*new_beads)[i-1], &(*new_beads)[i], 1.0 );
		(*new_beads)[i-1].set_diffusion_element( new_diffusion );
		
		p_tot = p_tot * p_i * new_diffusion;

	} // end loop all new coords. 
	double final_diffusion_element = my_interaction.rho( &(*new_beads)[m], &(*new_beads)[1+m], 1.0 );
	(*new_beads)[m].set_diffusion_element( final_diffusion_element );
	
	return p_tot*final_diffusion_element;
	
}

























// Return the reverse sampling prob. for an exisiting head-tail pair:
template <class inter> double sampling_toolbox<inter>::reverse_head_prob( Bead* tail, Bead* head )
{
	// obtain the difference in the imaginary time between the head and tail
	double tau_sample = (*head).get_real_time() - (*tail).get_real_time();
	if( tau_sample < 0 )
	{
		tau_sample += p->beta;
	}
	
	// compute the sigma of the gaussian
	double sigma_sample = sqrt( tau_sample );
	
	double prob = 1.0;
	
	for(int iDim=0;iDim<p->dim;iDim++) // sample all dimensions
	{
		
		// obtain the sampling prob for iDim
		double sample_prob = 0.0;
			
		
		if( p->system_type < p->n_traps) // prob. for trap-systems
		{
			double delta_x_iDim = (*tail).get_coord( iDim ) - (*head).get_coord( iDim );
			sample_prob = exp( - delta_x_iDim * delta_x_iDim / ( 2.0 * sigma_sample*sigma_sample ) );
		}
		else // prob. for PBC-systems
		{
			for(int n = -(*p).n_boxes;n<(*p).n_boxes+1;n++)
			{
				double delta_r = (*head).get_coord( iDim ) - (*tail).get_coord( iDim ) + n*(*p).length;
				sample_prob += exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
			}
		}
			
		sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );
		
		
		
		// update the total sampling prob
		prob *= sample_prob;
		
		/*
		double delta_x_iDim = (*tail).get_coord( iDim ) - (*head).get_coord( iDim );
		
		// obtain the sampling prob for iDim
		double sample_prob = exp( - delta_x_iDim * delta_x_iDim / ( 2.0 * sigma_sample*sigma_sample ) );
		sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );
			
		// update the total sampling prob
		prob *= sample_prob;
		*/
	}
	
	// Return the total sampling prob:
	return prob;

}




// Generate the coordinates for the head (standard PIMC update insert) around the tail and return the sampling prob:
template <class inter> double sampling_toolbox<inter>::sample_new_head( Bead* tail, Bead* head )
{
	// obtain the difference in the imaginary time between the head and tail
	double tau_sample = (*head).get_real_time() - (*tail).get_real_time();
	if( tau_sample < 0 )
	{
		tau_sample += p->beta;
	}
	
	// compute the sigma of the gaussian
	double sigma_sample = sqrt( tau_sample );
	
	double prob = 1.0;
	
	for(int iDim=0;iDim<p->dim;iDim++) // sample all dimensions
	{
		double delta_x_iDim = gaussian()*sigma_sample; // obtain the gaussian displacment

		double x_iDim = (*tail).get_coord( iDim ) + delta_x_iDim; // obtain the new coord of the head
		x_iDim = my_interaction.adjust_coordinate( x_iDim ); // system-specific adjustment, e.g. PBC
		
		// obtain the sampling prob for iDim
		double sample_prob = 0.0;
			
		
		if( p->system_type < p->n_traps) // prob. for trap-systems
		{
			sample_prob = exp( - delta_x_iDim * delta_x_iDim / ( 2.0 * sigma_sample*sigma_sample ) );
		}
		else // prob. for PBC-systems
		{
			for(int n = -(*p).n_boxes;n<(*p).n_boxes+1;n++)
			{
				double delta_r = x_iDim - (*tail).get_coord( iDim ) + n*(*p).length;
				sample_prob += exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
			}
		}
			
		sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );
		
		
		
		// update the total sampling prob
		prob *= sample_prob;
		
		// update the coord of the new head bead
		(*head).set_coord( iDim, x_iDim );
	}
	
	// Return the total sampling prob:
	return prob;
}




// m is the total number of beads to be resampled, we do not have to start/end with main slice
template <class inter> double sampling_toolbox<inter>::trap_backward_connect( std::vector<Bead>*new_beads, int m )
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[1+m].get_real_time();

	// index of the last (fixed) bead
	int m_index = m + 1;
	
	double p_tot = 1.0; // Initialize the prob. to sample the total path
 
	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		
		tau_i = (*new_beads)[i].get_real_time();
		tau_i_minus = (*new_beads)[i-1].get_real_time();
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);
		
		double sigma_sample = sqrt(tau_sample);
		
		double p_i = 1.0; // Initialize the sampling prob. for point i
		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			
			
			double x_minus = (*new_beads)[i-1].get_coord(iDim);
			
			// First obtain the end point of the trajectory:
			double x_end = (*new_beads)[ m_index ].get_coord(iDim);
			

			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_end * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			

			// for the backwards prob, the new position is the old one
			double new_pos = (*new_beads)[i].get_coord( iDim );


			double delta_r = new_pos - xi;
			double sample_prob = 0.0;
			
			if( p->system_type < p->n_traps) // prob. for trap-systems
			{
				sample_prob = exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
			}
			else // prob. for PBC-systems
			{
				for(int n = -(*p).n_boxes;n<(*p).n_boxes+1;n++)
				{
					double delta_r = new_pos - xi + n*(*p).length;
					sample_prob += exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
				}
			}
			
			sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );
			
			p_i = p_i * sample_prob;

			// for the backwards prob, nothing is updated!
			
		} // end loop idim
		
		p_tot = p_tot * p_i;

	} // end loop all new coords. 
	
	
	return p_tot;
	
}

























// m is the total number of beads to be resampled, we do not have to start/end with main slice
template <class inter> double sampling_toolbox<inter>::trap_backward_connect( std::vector<Bead>*new_beads, int m, std::vector<double> &probs )
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[1+m].get_real_time();

	// index of the last (fixed) bead
	int m_index = m + 1;
	
	double p_tot = 1.0; // Initialize the prob. to sample the total path
 
	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		
		tau_i = (*new_beads)[i].get_real_time();
		tau_i_minus = (*new_beads)[i-1].get_real_time();
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);
		
		double sigma_sample = sqrt(tau_sample);
		
		double p_i = 1.0; // Initialize the sampling prob. for point i
		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			
			
			double x_minus = (*new_beads)[i-1].get_coord(iDim);
			
			// First obtain the end point of the trajectory:
			double x_end = (*new_beads)[ m_index ].get_coord(iDim);
			

			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_end * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			

			// for the backwards prob, the new position is the old one
			double new_pos = (*new_beads)[i].get_coord( iDim );


			double delta_r = new_pos - xi;
			double sample_prob = 0.0;
			
			if( p->system_type < p->n_traps) // prob. for trap-systems
			{
				sample_prob = exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
			}
			else // prob. for PBC-systems
			{
				for(int n = -(*p).n_boxes;n<(*p).n_boxes+1;n++)
				{
					double delta_r = new_pos - xi + n*(*p).length;
					sample_prob += exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
				}
			}
			
			sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );
			
			p_i = p_i * sample_prob;
			
			

			// for the backwards prob, nothing is updated!
			
		} // end loop idim
		
		
		probs[i-1] = p_i;
		
		p_tot = p_tot * p_i;

	} // end loop all new coords. 
	
	
	return p_tot;
	
}

















// m is the total number of beads to be resampled, we do not have to start/end with main slice
template <class inter> double sampling_toolbox<inter>::PBC_backward_connect(std::vector<Bead>*new_beads, int m)
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[1+m].get_real_time();

	// index of the last (fixed) bead
	int m_index = m + 1;
	
	double p_tot = 1.0; // Initialize the prob. to sample the total path
 
	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		
		tau_i = (*new_beads)[i].get_real_time();
		tau_i_minus = (*new_beads)[i-1].get_real_time();
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);
		
		double sigma_sample = sqrt(tau_sample);
		
		double p_i = 1.0; // Initialize the sampling prob. for point i
		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			
			
			double x_minus = (*new_beads)[i-1].get_coord(iDim);
			
			// First select the nearest image as the end point of the trajectory:
			double x_image = (*new_beads)[m_index].get_coord(iDim);
			
			while( (x_image - x_minus) > 0.50*(*p).length )
			{
				x_image = x_image - (*p).length;
			}
			
			while( (x_image - x_minus) < -0.50*(*p).length )
			{
				x_image = x_image + (*p).length;
			}
			
			
			
			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_image * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			
// 			// Sample with a gaussian around xi:
// 			double new_pos = xi + gaussian()*sigma_sample;
			
// 			// Adjust the new coordinate, e.g. for PBC in homogeneous systems
// 			new_pos = my_interaction.adjust_coordinate( new_pos );
			
			// for the backwards prob, the new position is the old one
			double new_pos = (*new_beads)[i].get_coord(iDim);
			
			double sample_prob = 0.0;
			for(int n = -(*p).n_boxes;n<(*p).n_boxes+1;n++)
			{
				double delta_r = new_pos - xi + n*(*p).length;
				sample_prob += exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
			}
			
			sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );
			
			p_i = p_i * sample_prob;
			
			
			// Update the coordinate of the affected bead
// 			(*new_beads)[i].set_coord(iDim, new_pos);
			
			// for the backwards prob, nothing is updated!
			
		} // end loop idim
		
		p_tot = p_tot * p_i;

	} // end loop all new coords. 
	
	
	return p_tot;
	
}

















// m is the total number of beads to be resampled, we do not have to start/end with main slice
template <class inter> double sampling_toolbox<inter>::PBC_backward_connect( std::vector<Bead>*new_beads, int m, std::vector<double> &probs )
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[1+m].get_real_time();

	// index of the last (fixed) bead
	int m_index = m + 1;
	
	double p_tot = 1.0; // Initialize the prob. to sample the total path
 
	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		
		tau_i = (*new_beads)[i].get_real_time();
		tau_i_minus = (*new_beads)[i-1].get_real_time();
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);
		
		double sigma_sample = sqrt(tau_sample);
		
		double p_i = 1.0; // Initialize the sampling prob. for point i
		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			
			
			double x_minus = (*new_beads)[i-1].get_coord(iDim);
			
			// First select the nearest image as the end point of the trajectory:
			double x_image = (*new_beads)[m_index].get_coord(iDim);
			
			while( (x_image - x_minus) > 0.50*(*p).length )
			{
				x_image = x_image - (*p).length;
			}
			
			while( (x_image - x_minus) < -0.50*(*p).length )
			{
				x_image = x_image + (*p).length;
			}
			
			
			
			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_image * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			
			// for the backwards prob, the new position is the old one
			double new_pos = (*new_beads)[i].get_coord(iDim);
			
			double sample_prob = 0.0;
			for(int n = -(*p).n_boxes;n<(*p).n_boxes+1;n++)
			{
				double delta_r = new_pos - xi + n*(*p).length;
				sample_prob += exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
			}
			
			sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );
			
			p_i = p_i * sample_prob;
			
			
			
			// for the backwards prob, nothing is updated!
			
		} // end loop idim
		
		probs[i-1] = p_i;
		
		p_tot = p_tot * p_i;

	} // end loop all new coords. 
	
	
	return p_tot;
	
}
























template <class inter> double sampling_toolbox<inter>::debug_PBC_backward_connect( bool debug, std::vector<Bead>*new_beads, int m )
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[1+m].get_real_time();

	// index of the last (fixed) bead
	int m_index = m + 1;
	
	double p_tot = 1.0; // Initialize the prob. to sample the total path
 
	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		
		tau_i = (*new_beads)[i].get_real_time();
		tau_i_minus = (*new_beads)[i-1].get_real_time();
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);
		
		double sigma_sample = sqrt(tau_sample);
		
		double p_i = 1.0; // Initialize the sampling prob. for point i
		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			
			
			double x_minus = (*new_beads)[i-1].get_coord(iDim);
			
			// First select the nearest image as the end point of the trajectory:
			double x_image = (*new_beads)[m_index].get_coord(iDim);
			
			while( (x_image - x_minus) > 0.50*(*p).length )
			{
				x_image = x_image - (*p).length;
			}
			
			while( (x_image - x_minus) < -0.50*(*p).length )
			{
				x_image = x_image + (*p).length;
			}
			
			
			
			
			
			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_image * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			
// 			// Sample with a gaussian around xi:
// 			double new_pos = xi + gaussian()*sigma_sample;
			
// 			// Adjust the new coordinate, e.g. for PBC in homogeneous systems
// 			new_pos = my_interaction.adjust_coordinate( new_pos );
			
			// for the backwards prob, the new position is the old one
			double new_pos = (*new_beads)[i].get_coord(iDim);
			
			double sample_prob = 0.0;
			for(int n = -(*p).n_boxes;n<(*p).n_boxes+1;n++)
			{
				double delta_r = new_pos - xi + n*(*p).length;
				sample_prob += exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
			}
			
			sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );
			
			p_i = p_i * sample_prob;
			
			
			
			
			if(debug)
			{
				std::cout << "debug_PBC_backward_connect, i: " << i << "\t iDim: " << iDim << "\t x_minus: " << x_minus << "\t x_image: " << x_image << "\t xi: " << xi << "\t new_pos: " << new_pos << "\t sample_prob: " << sample_prob << "\n"; 
				
				
				
				
			}
			
			
			
			// Update the coordinate of the affected bead
// 			(*new_beads)[i].set_coord(iDim, new_pos);
			
			// for the backwards prob, nothing is updated!
			
		} // end loop idim
		
		p_tot = p_tot * p_i;

	} // end loop all new coords. 
	
	
	return p_tot;
	
}
















// m is the total number of beads to be resampled, we do not have to start/end with main slice
template <class inter> double sampling_toolbox<inter>::combined_trap_connect( std::vector<Bead>*new_beads, int m )
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[1+m].get_real_time();

	// index of the last (fixed) bead
	int m_index = m + 1;
	
	double p_tot = 1.0; // Initialize the prob. to sample the total path
	
	Bead last_bead;
	last_bead.copy( &(*new_beads)[0] );
 
	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		double mass_i = (*new_beads)[i].get_mass();
		tau_i = (*new_beads)[i].get_real_time();
		tau_i_minus = (*new_beads)[i-1].get_real_time();
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);

		// @TODO check for mass
		double sigma_sample = sqrt(tau_sample / mass_i);

		double p_i = 1.0; // Initialize the sampling prob. for point i
		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			
			
			double x_minus = (*new_beads)[i-1].get_coord(iDim);
			
			// First select the nearest image as the end point of the trajectory:
			double x_end = (*new_beads)[m_index].get_coord(iDim);

			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_end * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			double xi_reverse = last_bead.get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_end * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			
			
			// Sample with a gaussian around xi:
			double new_pos = xi + gaussian()*sigma_sample;
			
			// Adjust the new coordinate, e.g. for PBC in homogeneous systems
			new_pos = my_interaction.adjust_coordinate( new_pos );
			

			double delta_r = new_pos - xi;    
			double sample_prob = exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
			
			
			double delta_r_reverse = (*new_beads)[i].get_coord( iDim ) - xi_reverse;
			double sample_prob_reverse = exp( -delta_r_reverse*delta_r_reverse / ( 2.0 * sigma_sample*sigma_sample ) );
			
// 			sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );

			p_i = p_i * sample_prob_reverse / sample_prob;

			
			// Update the coordinate of the affected bead
			last_bead.set_coord( iDim, (*new_beads)[i].get_coord( iDim ) );
			(*new_beads)[i].set_coord( iDim, new_pos );
			
		} // end loop idim
		
		p_tot = p_tot * p_i;

	} // end loop all new coords. 
	
	
	return p_tot;
	
}















// m is the total number of beads to be resampled, we do not have to start/end with main slice
template <class inter> double sampling_toolbox<inter>::trap_connect( std::vector<Bead>*new_beads, int m )
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[ 1+m ].get_real_time();

	// index of the last (fixed) bead
	int m_index = m + 1;
	
	double p_tot = 1.0; // Initialize the prob. to sample the total path
 
	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		
		tau_i = (*new_beads)[i].get_real_time();
		tau_i_minus = (*new_beads)[i-1].get_real_time();
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);
		
		double sigma_sample = sqrt(tau_sample);

		double p_i = 1.0; // Initialize the sampling prob. for point i
		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			
			
			double x_minus = (*new_beads)[i-1].get_coord(iDim);
			
			// First select the nearest image as the end point of the trajectory:
			double x_end = (*new_beads)[m_index].get_coord(iDim);

			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_end * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			
			// Sample with a gaussian around xi:
			double new_pos = xi + gaussian()*sigma_sample;
			
			// Adjust the new coordinate, e.g. for PBC in homogeneous systems
			new_pos = my_interaction.adjust_coordinate( new_pos );
			

			double delta_r = new_pos - xi;    
			double sample_prob = exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
			
			
			sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );

			p_i = p_i * sample_prob;

			
			// Update the coordinate of the affected bead
			(*new_beads)[i].set_coord( iDim, new_pos );
			
		} // end loop idim
		
		p_tot = p_tot * p_i;

	} // end loop all new coords. 
	
	
	return p_tot;
	
}







// m is the total number of beads to be resampled, we do not have to start/end with main slice
template <class inter> double sampling_toolbox<inter>::trap_connect( std::vector<Bead>*new_beads, int m, std::vector<double> &probs )
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[ 1+m ].get_real_time();

	// index of the last (fixed) bead
	int m_index = m + 1;
	
	double p_tot = 1.0; // Initialize the prob. to sample the total path
 
	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		
		tau_i = (*new_beads)[i].get_real_time();
		tau_i_minus = (*new_beads)[i-1].get_real_time();
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);
		
		double sigma_sample = sqrt(tau_sample);

		double p_i = 1.0; // Initialize the sampling prob. for point i
		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			
			
			double x_minus = (*new_beads)[i-1].get_coord(iDim);
			
			// First select the nearest image as the end point of the trajectory:
			double x_end = (*new_beads)[m_index].get_coord(iDim);

			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_end * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			
			// Sample with a gaussian around xi:
			double new_pos = xi + gaussian()*sigma_sample;
			
			// Adjust the new coordinate, e.g. for PBC in homogeneous systems
			new_pos = my_interaction.adjust_coordinate( new_pos );
			

			double delta_r = new_pos - xi;    
			double sample_prob = exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
			
			
			sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );

			p_i = p_i * sample_prob;

			
			// Update the coordinate of the affected bead
			(*new_beads)[i].set_coord( iDim, new_pos );
			
		} // end loop idim
		
		probs[i-1] = p_i;
		
		p_tot = p_tot * p_i;

	} // end loop all new coords. 
	
	
	
	return p_tot;
	
}











// m is the total number of beads to be resampled, we do not have to start/end with main slice
template <class inter> double sampling_toolbox<inter>::combined_PBC_connect( std::vector<Bead>*new_beads, int m )
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[1+m].get_real_time();
 
	// index of the last (fixed) bead
	int m_index = m + 1;
	
	double p_tot = 1.0; // Initialize the prob. to sample the total path
	
	
	Bead last_bead;
	last_bead.copy( &(*new_beads)[ 0 ] );
	
 
	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		double mass_i = (*new_beads)[i].get_mass();
		tau_i = (*new_beads)[i].get_real_time();
		tau_i_minus = (*new_beads)[i-1].get_real_time();
		
 		
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);

		// @TODO Check mass
		double sigma_sample = sqrt(tau_sample / mass_i);
		
		double p_i = 1.0; // Initialize the sampling prob. for point i
		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			
			
			double x_minus = (*new_beads)[i-1].get_coord(iDim);
			
			// First select the nearest image as the end point of the trajectory:
			double x_image = (*new_beads)[m_index].get_coord(iDim);
			
			while( (x_image - x_minus) > 0.50*(*p).length )
			{
				x_image = x_image - (*p).length;
			}
			
			while( (x_image - x_minus) < -0.50*(*p).length )
			{
				x_image = x_image + (*p).length;
			}

			
			// Also select the neartest image of the old coords
			double x_image_reverse = (*new_beads)[m_index].get_coord(iDim);
			double x_minus_reverse = last_bead.get_coord(iDim);
			
			while( (x_image_reverse - x_minus_reverse) > 0.50*(*p).length )
			{
				x_image_reverse = x_image_reverse - (*p).length;
			}
			
			while( (x_image_reverse - x_minus_reverse) < -0.50*(*p).length )
			{
				x_image_reverse = x_image_reverse + (*p).length;
			}
			
			
			
			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_image * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			double xi_reverse = last_bead.get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_image_reverse * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			
			
			
			
			// Sample with a gaussian around xi:
			double new_pos = xi + gaussian()*sigma_sample;
			
			// Adjust the new coordinate, e.g. for PBC in homogeneous systems
			new_pos = my_interaction.adjust_coordinate( new_pos );
			
			double sample_prob = 0.0;
			double sample_prob_reverse = 0.0;
			for(int n = -(*p).n_boxes;n<(*p).n_boxes+1;n++)
			{
				double delta_r = new_pos - xi + n*(*p).length;
				sample_prob += exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
				
				double delta_r_reverse = (*new_beads)[i].get_coord( iDim ) - xi_reverse + n*(*p).length; 
				sample_prob_reverse += exp( -delta_r_reverse * delta_r_reverse / ( 2.0 * sigma_sample*sigma_sample ) );
			}
			
// 			sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );
// 			sample_prob_reverse = sample_prob_reverse
			
			/*
			std::cout << "x_image: " << x_image << "\t x_image_reverse: " << x_image_reverse << "\t pos: " << (*new_beads)[i].get_coord(iDim) << "\t new_pos: " << new_pos << "\n";
			std::cout << "i: " << i << "\t p_i: " << p_i << "\t sample_prob: " << sample_prob << "\t sample_prob_reverse: " << sample_prob_reverse << "\n";
			*/
			
			p_i = p_i * sample_prob_reverse / sample_prob;
						
			// Update the coordinate of the affected bead
			last_bead.set_coord( iDim, (*new_beads)[i].get_coord( iDim ) );
			(*new_beads)[i].set_coord( iDim, new_pos );
			
		} // end loop idim
		
		p_tot = p_tot * p_i;

	} // end loop all new coords. 
	
	
	return p_tot;
	
}











// m is the total number of beads to be resampled, we do not have to start/end with main slice
template <class inter> double sampling_toolbox<inter>::PBC_connect( std::vector<Bead>*new_beads, int m, std::vector<double> &probs )
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[1+m].get_real_time();
 
	// index of the last (fixed) bead
	int m_index = m + 1;
	
	double p_tot = 1.0; // Initialize the prob. to sample the total path
 
	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		
		tau_i = (*new_beads)[i].get_real_time();
		tau_i_minus = (*new_beads)[i-1].get_real_time();
		
 		
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);
		
		double sigma_sample = sqrt(tau_sample);
		
		double p_i = 1.0; // Initialize the sampling prob. for point i
		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			
			
			double x_minus = (*new_beads)[i-1].get_coord(iDim);
			
			// First select the nearest image as the end point of the trajectory:
			double x_image = (*new_beads)[m_index].get_coord(iDim);
			
			while( (x_image - x_minus) > 0.50*(*p).length )
			{
				x_image = x_image - (*p).length;
			}
			
			while( (x_image - x_minus) < -0.50*(*p).length )
			{
				x_image = x_image + (*p).length;
			}
			
			
			
			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_image * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			
			// Sample with a gaussian around xi:
			double new_pos = xi + gaussian()*sigma_sample;
			
			// Adjust the new coordinate, e.g. for PBC in homogeneous systems
			new_pos = my_interaction.adjust_coordinate( new_pos );
			
			double sample_prob = 0.0;
			for(int n = -(*p).n_boxes;n<(*p).n_boxes+1;n++)
			{
				double delta_r = new_pos - xi + n*(*p).length;
				sample_prob += exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
			}
			
			sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );
			
			
			
			p_i = p_i * sample_prob;
						
			// Update the coordinate of the affected bead
			(*new_beads)[i].set_coord(iDim, new_pos);
		} // end loop idim
		
		probs[i-1] = p_i;
		
		p_tot = p_tot * p_i;

	} // end loop all new coords. 
	
	
	return p_tot;
	
}













// m is the total number of beads to be resampled, we do not have to start/end with main slice
template <class inter> double sampling_toolbox<inter>::PBC_connect( std::vector<Bead>*new_beads, int m )
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[1+m].get_real_time();
 
	// index of the last (fixed) bead
	int m_index = m + 1;
	
	double p_tot = 1.0; // Initialize the prob. to sample the total path
 
	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		
		tau_i = (*new_beads)[i].get_real_time();
		tau_i_minus = (*new_beads)[i-1].get_real_time();
		
 		
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);
		
		double sigma_sample = sqrt(tau_sample);
		
		double p_i = 1.0; // Initialize the sampling prob. for point i
		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			
			
			double x_minus = (*new_beads)[i-1].get_coord(iDim);
			
			// First select the nearest image as the end point of the trajectory:
			double x_image = (*new_beads)[m_index].get_coord(iDim);
			
			while( (x_image - x_minus) > 0.50*(*p).length )
			{
				x_image = x_image - (*p).length;
			}
			
			while( (x_image - x_minus) < -0.50*(*p).length )
			{
				x_image = x_image + (*p).length;
			}
			
			
			
			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_image * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			
			// Sample with a gaussian around xi:
			double new_pos = xi + gaussian()*sigma_sample;
			
			// Adjust the new coordinate, e.g. for PBC in homogeneous systems
			new_pos = my_interaction.adjust_coordinate( new_pos );
			
			double sample_prob = 0.0;
			for(int n = -(*p).n_boxes;n<(*p).n_boxes+1;n++)
			{
				double delta_r = new_pos - xi + n*(*p).length;
				sample_prob += exp( - delta_r * delta_r / ( 2.0 * sigma_sample*sigma_sample ) );
			}
			
			sample_prob = sample_prob / ( sqrt(2.0*pi)*sigma_sample );
			
			
			
			p_i = p_i * sample_prob;
						
			// Update the coordinate of the affected bead
			(*new_beads)[i].set_coord(iDim, new_pos);
		} // end loop idim
		
		p_tot = p_tot * p_i;

	} // end loop all new coords. 
	
	
	return p_tot;
	
}









// m is the total number of beads to be resampled, we do not have to start/end with main slice
template <class inter> void sampling_toolbox<inter>::connect(std::vector<Bead>*new_beads, int m)
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[1+m].get_real_time();

	// index of the last (fixed) bead
	int m_index = m+1;	

	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		
		tau_i = (*new_beads)[i].get_real_time();
		tau_i_minus = (*new_beads)[i-1].get_real_time();
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);
		
		double sigma_sample = sqrt(tau_sample);
		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + (*new_beads)[m_index].get_coord(iDim) * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			
			// Sample with a gaussian around xi:
			double new_pos = xi + gaussian()*sigma_sample;
			
			// Adjust the new coordinate, e.g. for PBC in homogeneous systems
			new_pos = my_interaction.adjust_coordinate( new_pos );
			
			// Update the coordinate of the affected bead
			(*new_beads)[i].set_coord(iDim, new_pos);
		}
		

	}
	
	
	
	
}






















// ###################################################################################################
// #### New functions for standard PIMC implementation
// ###################################################################################################










// sample m new beads between new_beads[0] and new_beads[m+1], return P_sample / diffusion-ratio = 1.0 / rho( 0, m+1, tau )
template <class inter> double sampling_toolbox<inter>::standard_PIMC_trap_connect( std::vector<Bead>*new_beads, int m )
{
	// imaginary time of the fixed end-point
	double tau_m = (*new_beads)[ 1+m ].get_real_time();

	// index of the last (fixed) bead
	int m_index = m + 1;
 
	// loop over all beads to be sampled, i denotes the index of the bead to be changed in new_beads
	for(int i=1;i<m+1;i++)
	{
		double tau_i; // time of current beads
		double tau_i_minus; // time of the bead before
		
		tau_i = (*new_beads)[ i ].get_real_time();
		tau_i_minus = (*new_beads)[ i-1 ].get_real_time();
		
		// Calculate the imaginary time ratio from the sampling procedure, delta_time handles the beta-periodicity
		double tau_sample = delta_time(tau_m - tau_i,p)*delta_time(tau_i - tau_i_minus,p) / delta_time(tau_m - tau_i_minus,p);
		
		double sigma_sample = sqrt(tau_sample);


		
		// Loop over all dimensions:
		for(int iDim=0;iDim<(*p).dim;iDim++)
		{
			double x_minus = (*new_beads)[ i-1 ].get_coord( iDim );
			
			// First select the nearest image as the end point of the trajectory:
			double x_end = (*new_beads)[ m_index ].get_coord( iDim );

			// Calculate the intersection with the current time slice:
			double xi = (*new_beads)[i-1].get_coord(iDim) * delta_time(tau_m-tau_i,p) / delta_time(tau_m-tau_i_minus,p) + x_end * delta_time(tau_i-tau_i_minus,p) / delta_time(tau_m-tau_i_minus,p);
			
			// Sample with a gaussian around xi:
			double new_pos = xi + gaussian()*sigma_sample;
			
			// Adjust the new coordinate, e.g. for PBC in homogeneous systems
			new_pos = my_interaction.adjust_coordinate( new_pos );
			
			// Update the coordinate of the affected bead
			(*new_beads)[ i ].set_coord( iDim, new_pos );
			
		} // end loop idim
		


	} // end loop all new coords. 
	
	
	return 1.0 / my_interaction.rho( &(*new_beads)[ 0 ], &(*new_beads)[ m_index ], double( m+1 ) );
	
}










