#pragma once

#include "particles/pair_action_lookup_table.hpp"

/*
 * Contains the Monte Carlo class
 * 
 */



template <class inter> class Monte_Carlo
{
	
public:
    Monte_Carlo() {}

    void init( plot_tools my_plot, update_probs user_probs, Parameters p );
	
	void monte_carlo();
	
	void update_particle_histogram( int ctrl );
	
	void print_particle_histogram( std::string s );
	
	
private:
	
	// Metropolis configuration, i.e. beads, beadlists, matrices, etc.
	worm_configuration config;
	
	// Implementation for multiple species:
	// Save an entire ensemble of configurations
	config_ensemble ensemble;
	



	
	// PBPIMC updates
	update_PBPIMC< inter > update;
	
	// Observables 
	observable< inter > obs;
	
	int total_measurements;
	int status;
	
	
	int n_measure = 0;
	long int step = 0;
	

	int n_status_message = 0;
	
	


	
	// Create a histogram of particle-number configurations
	std::vector< std::vector <int> > all_N_histogram;
	std::vector< long int > all_N_count;
	
	
	plot_tools plot;

};



template <class inter> void Monte_Carlo<inter>::print_particle_histogram( std::string s )
{
	// Write the histogram to the disk:
	std::fstream f;
	f.open( s.c_str(), std::ios::out );
			
	for(int i=0;i<all_N_histogram.size();i++)
	{
		f << all_N_count[ i ] << "\t" << all_N_count[ i ] / double( n_measure );
		for(int k=0;k<ensemble.species_config.size();k++)
		{
			f << "\t" << all_N_histogram[ i ][ k ];
		}
		f << "\n";
	}
			
	f.close();
}
	
	
	

template <class inter> void Monte_Carlo<inter>::update_particle_histogram( int ctrl )
{
	if( ctrl > 0 ) // update the particle-number histogram
	{
		// obtain the particle number configuration of the current ensemble
		std::vector< int > tmp_N( ensemble.species_config.size(), 0 );
		for(int iSpecies=0;iSpecies<ensemble.species_config.size();iSpecies++)
		{
			tmp_N[ iSpecies ] = ensemble.species_config[ iSpecies ].beadlist[ 0 ].size();
		}
			
		// check, if this has been measured before 
		bool c = false;
			
		for(int i=0;i<all_N_histogram.size();i++)
		{
			int tmp_index;
			for(int k=0;k<ensemble.species_config.size();k++)
			{
				if( all_N_histogram[ i ][ k ] != tmp_N[ k ] )
				{
					break;
				}
				else 
				{
					if( k == ensemble.species_config.size()-1 )
					{ 
						c = true;
						tmp_index = i;
					}
				}
			}
			
			if( c )
			{
				all_N_count[ tmp_index ] += 1;
				break;
			}
		}
			
		if( !c ) // the current N config has not been added to the histogram yet
		{
			all_N_histogram.push_back( tmp_N );
			all_N_count.push_back( 1 );
		}
		
	}
}



template <class inter> void Monte_Carlo<inter>::monte_carlo()
{
	
	auto start_time = std::chrono::high_resolution_clock::now();
	
	double updateTime = 0;
	double measureTime = 0;
	double totalTime = 0;
	
	std::cout << "total_measurements: " << total_measurements << "\n";
	while( n_measure < total_measurements )
	{
		step++; // increase the step counter
		
		
		
		auto before_update_time = std::chrono::high_resolution_clock::now();
		update.update(); // perform a Monte Carlo update
		
		
		auto before_measure_time = std::chrono::high_resolution_clock::now();
		int ctrl = obs.measure(); // try to measure observables
	
		auto reference_time = std::chrono::high_resolution_clock::now();
		
		n_measure += ctrl; // update the number of reference measurements

		update_particle_histogram( ctrl ); // update the histogram of the particle numbers
		

	
		// Create a video for standard PIMC?
		if( plot.video_control && ( step > ensemble.params.n_equil ) && ( ensemble.params.simulation_type >= ensemble.params.n_multi ) )
		{
			std::cout << "standard_PIMC_video, step = " << step << "\n";
			plot.standard_PIMC_video( &ensemble );
		}


		
		
		
		
		
		// Give a status message to the terminal every status steps:
		if( step/status == double(step)/double(status) )
		{
			std::cout << "#########################################################\n";
			std::cout << "n_measure " << n_measure << " of " << total_measurements << " ( " << 100.0* double(n_measure) / double(total_measurements) << "% )\n";
			std::cout << "step: " << step << "\n";
			std::cout << "*****UpdateTime: " << double(updateTime)/double(totalTime)*100.0 << "\tmeasureTime: " << double(measureTime)/double(totalTime)*100 << "\ttotal (sec): " << double(totalTime) << "\n";
			update.make_check(); // Make a check of energies etc, and reset them
			std::cout << "#########################################################\n";
			update.status_message(); // prints acceptance ratios of the Monte Carlo updates to the terminal
			
			
			print_particle_histogram( "ensemble_hist.out" ); // print the particle histogram to the disk
			
			
			
		}
		
		
		// Keep track of the time:

		auto end_time = std::chrono::high_resolution_clock::now();
		
		std::chrono::duration<double> du = std::chrono::duration_cast<std::chrono::nanoseconds>(before_measure_time - before_update_time);
		std::chrono::duration<double> dt = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - before_update_time);
		std::chrono::duration<double> dm = std::chrono::duration_cast<std::chrono::nanoseconds>(reference_time - before_measure_time);
		
		double delta_total = dt.count();
		double delta_update = du.count();
		double delta_measure = dm.count();
		
		totalTime += delta_total;
		updateTime += delta_update;
		measureTime += delta_measure;
		
	}
	std::cout << "The Monte Carlo simulation has been successfully executed.\n";
}









template <class inter> void Monte_Carlo<inter>::init( plot_tools my_plot, update_probs user_probs, Parameters p )
{
	// Set the system parameters of the config:
	// This config is NOT a meaningful config for a single species, but rather all particles put into single species config
	config.params = p;
	
	ensemble.params = p;

	plot = my_plot;
	


	
	std::cout << "p.system_type::: " << p.system_type << "\n";
	

	
	if( ( p.system_type == p.n_traps ) || ( p.system_type == 9 ) || (  p.system_type == 11 ) || ( p.system_type == 14 ) || ( p.system_type >= 16 ) || ( p.system_type == 13 ) ) // Pre-calculate the Madelung const. etc. for the HEG
	{
		

		perturbed_ewald_HEG HEG;
		HEG.init( &config.params );
			
		double tol = 1e-10;
		int empty;
			
		config.params.madelung = HEG.get_madelung_constant( tol, &empty, config.params.kappa_int, 20 );
			
		std::cout << "MADELUNG: " << config.params.madelung << "\n";


	
	
	} // End of pre-caclulations for the HEG
	
	
	
	
	// Update the Madelung constant in Ensemble as well
	ensemble.params = config.params;

	
	total_measurements = p.total_measurements;
	status = p.status;
	




	// Initialize the updates and the config
	// Perform equilibration
 	update.init( user_probs, &config, &(config.params), &ensemble );

	// Initialize the observables:
 	obs.init( &(config.params), &ensemble );


	std::cout << "Monte Carlo init is finished.\n";



}
