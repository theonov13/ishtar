#include <chrono>
#include <thread>






class plot_tools
{
public:


	void standard_PIMC_3D_plot_config_povray( config_ensemble* ensemble, std::string file );
	void standard_PIMC_video( config_ensemble* ensemble );

	
	
	bool video_control;
	int frame_frequency;
	int total_frames;
	
	
	int n_frames = 0;
	int frame_cycle = 0;
	
};



int n_digits( int x )
{
	int r = 0;
	while( x >= pow( 10, r ) )
	{
		r++;
	}
	return r;
}






void plot_tools::standard_PIMC_video( config_ensemble* ensemble )
{
	frame_cycle++;
	if( n_frames == total_frames )
	{
		std::cout << "standard_PIMC_video: all " << total_frames << " have been generated, stopping program!\n";
		std::stringstream ss;
		ss << "ffmpeg -framerate 25 -i PIMC_video_%0" << n_digits( total_frames ) << "d_3D_plot.png output.mp4";
		int ctrl = system( ss.str().c_str() );
		exit(1);
	}
	if( frame_cycle == frame_frequency )
	{
		n_frames++;
		frame_cycle = 0;
		
		std::stringstream ss;
		ss << "PIMC_video_";
		int total_digits = n_digits( total_frames );
		int my_digits = n_digits( n_frames );
		while( my_digits < total_digits )
		{
			ss << "0";
			my_digits++;
		}
		ss << n_frames;
		
		std::cout << "--file: " << ss.str() << "\n";
		
		standard_PIMC_3D_plot_config_povray( ensemble, ss.str() );
	}
}





























void plot_tools::standard_PIMC_3D_plot_config_povray( config_ensemble* ensemble, std::string file )
{

	
	
	if( ensemble->params.dim != 3 ) return; // designed for 3D

	
	
	std::string cyl_col0 = "color rgb<209/250.0, 94/250.0, 59/250.0>";
	std::string cyl_col1 = "color rgb<210/250.0, 95/250.0, 60/250.0>";
	std::string cyl_col2 = "color rgb<211/250.0, 96/250.0, 61/250.0>";

// 	cyl_col = "color rgb<0.5,0.5,0.5>";
	
	
	std::vector<std::string> cyl_col_vector{ cyl_col0, cyl_col1, cyl_col2};
	
	
	
	
	
	std::string bead_col0 = "color rgb<66/250.0, 155/250.0, 244/250.0>";
	std::string bead_col1 = "color rgb<67/250.0, 156/250.0, 245/250.0>";
	std::string bead_col2 = "color rgb<68/250.0, 157/250.0, 246/250.0>";
// 	bead_col = "color rgb<117/250.0, 209/250.0, 56/250.0>";
	
	
	
	std::vector<std::string> bead_col_vector{ bead_col0, bead_col1, bead_col2};
	
	
	std::stringstream ss_file;
	ss_file << file << ".pov";
	
	std::fstream f; // file-handles to print bead coords for the three types of time-slices
	
	f.open( ss_file.str().c_str(), std::ios::out ); // file-handle for the gnuplot script
	
	

	
	f << "#include \"colors.inc\"" << std::endl;
	f << "background { color White }" << std::endl;
	f << "#declare LL = 0.680;" << std::endl;
	
	f << "#declare SR0 = " << ensemble->params.video_SR << ";" << std::endl;
	f << "#declare CR0 = " << ensemble->params.video_SR*0.015/0.025 << ";" << std::endl;
	
	f << "#declare SR1 = " << ensemble->params.video_SR << ";" << std::endl;
	f << "#declare CR1 = " << ensemble->params.video_SR*0.015/0.025 << ";" << std::endl;
	
	f << "#declare SR2 = " << ensemble->params.video_SR << ";" << std::endl;
	f << "#declare CR2 = " << ensemble->params.video_SR*0.015/0.025 << ";" << std::endl;
	
	f << "#declare L = 1;" << std::endl;
	f << "camera { location <2.0*LL, 2.5*LL, -1.7*LL> look_at <0.5*LL, 0.5*LL, 0.5*LL> }" << std::endl;

	if( ensemble->params.video_label )
	{
		f << "text { ttf \"crystal.ttf\" \"x\" 0.03, 0 pigment { Black } scale 0.1925 rotate<0,-45,0> translate 0.42*x-0.19*y-0.053*z}" << std::endl;
		f << "text { ttf \"crystal.ttf\" \"y\" 0.03, 0 pigment { Black } scale 0.1925 rotate<0,-45,0> translate x*1.04+0.45*z-0.03*y}" << std::endl;
		f << "text { ttf \"crystal.ttf\" \"z\" 0.03, 0 pigment { Black } scale 0.1925 rotate<0,-45,0> translate 0.45*y-0.14*x-0.04*z}" << std::endl;
	}
	
	
	
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
	{
	
		worm_configuration* c = &(*ensemble).species_config[ iSpecies ];
	
		// Loop over all beads and all connections (matrix-elements)
		for(int iSlice=0;iSlice<c->params.n_bead;iSlice++) // loop over all time slices
		{
			
			// loop over all particles on iSlice:
			for(int i=0;i<c->beadlist[ iSlice ].size();i++)
			{
				int id = c->beadlist[ iSlice ][ i ];		

				// Create spheres for the beads 
				f << "sphere{<" << c->beads[ id ].get_coord(0)/c->params.length << "," << c->beads[ id ].get_coord(1)/c->params.length << "," << c->beads[ id ].get_coord(2)/c->params.length << ">, SR" << iSpecies << " texture { pigment { " << bead_col_vector[iSpecies] << " transmit 0.467 } } }" << std::endl;

				
				if( c->head_id == id ) 
				{
					std::cout << "Encountered head_id = " << c->head_id << "\n";
					continue;
				}
				
				// First part of the cylinder-connection plot command
				std::stringstream cyl;
				cyl << "cylinder{<" << c->beads[ id ].get_coord(0)/c->params.length << "," << c->beads[ id ].get_coord(1)/c->params.length << "," << c->beads[ id ].get_coord(2)/c->params.length << ">,<";

				
				// find ID of next bead
				int next_id = c->beads[ id ].get_next_id();
				
				bool PBC = true;
				for(int iDim=0;iDim<c->params.dim;iDim++)
				{
					double next_coord = c->beads[ next_id ].get_coord( iDim );
					double coord = c->beads[ id ].get_coord( iDim );
					
					if( abs( next_coord - coord ) > 0.50*c->params.length ) PBC = false;
					
					cyl << next_coord/c->params.length;
					if( iDim != c->params.dim-1 )
						{
							cyl << ", ";
						}
						else 
						{
							cyl << ">, CR" << iSpecies << "*" << 0.78 << " pigment { " << cyl_col_vector[iSpecies] << " transmit 0.467 }}";
						}
				}

			
				if( PBC ) f << cyl.str() << "\n";
			
			} // end loop i
			
			
		} // end loop iSlice
		
	} // end loop over all particle species
		
	
	f << "cylinder{ <0,0,0>, <0,0,L>, 0.0031  pigment{color Black transmit 0}}\n";
	f << "cylinder{ <0,0,0>, <0,L,0>, 0.0031  pigment{color Black transmit 0}}\n";
	f << "cylinder{ <0,0,0>, <L,0,0>, 0.0031  pigment{color Black transmit 0}}\n";
	f << "cylinder{ <0,L,0>, <L,L,0>, 0.0031  pigment{color Black transmit 0}}\n";
	f << "cylinder{ <L,0,0>, <L,L,0>, 0.0031  pigment{color Black transmit 0}}\n";
	f << "cylinder{ <L,0,0>, <L,0,L>, 0.0031  pigment{color Black transmit 0}}\n";
	f << "cylinder{ <L,L,L>, <L,0,L>, 0.0031  pigment{color Black transmit 0}}\n";
	f << "cylinder{ <L,L,L>, <L,L,0>, 0.0031  pigment{color Black transmit 0}}\n";
	f << "cylinder{ <L,L,L>, <0,L,L>, 0.0031  pigment{color Black transmit 0}}\n";
	f << "cylinder{ <0,L,0>, <0,L,L>, 0.0031  pigment{color Black transmit 0}}\n";
	f << "cylinder{ <0,0,L>, <0,L,L>, 0.0031  pigment{color Black transmit 0}}\n";
	f << "cylinder{ <0,0,L>, <L,0,L>, 0.0031  pigment{color Black transmit 0}}\n";

	f << "light_source { <2.0*LL, 2.5*LL, -1.7*LL>color White}\n light_source { <2.0*LL, 2.5*LL, -1.7*LL>color White}\n";
	
	
	
	
	f.close();
	
	std::stringstream sys;
	sys << "povray " <<  ss_file.str() << " Height=" << ensemble->params.video_width*0.75 << " Width=" << ensemble->params.video_width << " Display=off Output_File_Name=" << file;
	std::cout << sys.str().c_str() << "\n";
	int ctrl = system( sys.str().c_str() );
	
	std::cout << "ss_file: " << ss_file.str() << "\t sys: " << sys.str() << "\n";
	
	
}













