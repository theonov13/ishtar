#pragma once

#include <random>
#include "parameters.hpp"


std::mt19937_64 mt(17229);
std::uniform_real_distribution<double> dist(0.0,1.0);
std::normal_distribution<double> poland(0.0,1.0);


// Uniform random number
double rdm()
{
	return dist(mt);
}

// gaussian random number
double gaussian()
{
 	return poland(mt);
}


const double pi = 3.1415926535897932384626433832795028841971693993751058;
const int digits = 12;
const int lw = 50;



// Print a line of length 'a' to the terminal
void line(int a)
{
	for(int q=0;q<a;q++)
	{
		std::cout << "~";
	}
	std::cout << "\n";
}


// Return sum of squared elements from a double vector
double vector_square(std::vector<double> v)
{
	double ans=0.0;
	for(auto c=v.begin();c!=v.end();c++)
	{
		ans += (*c)*(*c);
	}
	return ans;
}


// Adjust the time difference to beta periodicity
template<class T>
double delta_time(double t, T* p)
{
	double ans = t;
	if( ans <= 0.0 ) ans+=(*p).beta;
	return ans;
}

// Obtain the maximum element of a vector
double get_maximum( std::vector<double> *a )
{
	double ans = (*a)[0];
	
	for(auto c=(*a).begin();c!=(*a).end();c++)
	{
		if( (*c) > ans ) ans = (*c);
	}
	
	return ans;
}



template < class T > bool in_vector( T element, std::vector<T> vec )
{
	bool ans = false;
	for(auto c=vec.begin();c!=vec.end();c++)
	{
		if( (*c) == element )
		{
			ans = true;
			break;
		}
	}
	
	return ans;	
}


template<typename T>
auto get_vec_abs(std::vector<T>& v1) -> T
{
    T result = {0.0};

    for(auto&& i : v1)
    {
        result += i*i;
    }

    return sqrt(result);
}

template <class T>
auto get_diff_vec_abs(std::vector<T>& v1, std::vector<T>& v2) -> T
{

    T result = {0.0};

    auto range = v1.size();

    for(auto i = 0; i < range; i++)
    {
        result += (v1[i] - v2[i]) * (v1[i] - v2[i]);
    }

    result = std::sqrt(result);

    return result;
}