class error
{
public:
	error(std::vector<double> *new_sign_data);
	
	// Calculate the standard devation of data
	double get_sigma(std::vector<double> *data);
	
	// Calculate the mean value of data
	double get_mean(std::vector<double>* data);
	
	// Calculate the correlation between sign_data and data
	double get_correlation(std::vector<double>* data, double mean_data, double sigma_data);
	
	// Calculate the error of <data>/<sign>
	void get_result(std::vector<double>* data, double *result, double *error);
	
	
	
private:
	std::vector<double> *sign_data;
	double sigma_sign;
	double mean_sign;


};

error::error(std::vector<double> *new_sign_data)
{
	sign_data = new_sign_data;
	sigma_sign = get_sigma( sign_data );
	mean_sign = get_mean( sign_data );
}



// Return the fermionic exprectation value and the cross-correlated error
void error::get_result(std::vector<double>* data, double *result, double *error)
{
	double mean_data = get_mean( data );
	double sigma_data = get_sigma( data );

	double corr = get_correlation( data, mean_data, sigma_data );

	(*result) = mean_data / mean_sign;

	double r = (*result);

	double my_error_sq = r*r * ( sigma_data*sigma_data / ( mean_data*mean_data)  + sigma_sign*sigma_sign / (mean_sign*mean_sign) - 2.0*corr*sigma_sign*sigma_data / (mean_data*mean_sign) );

	(*error) = sqrt( my_error_sq ) / sqrt( double( (*data).size() ) );
	
}



double error::get_correlation(std::vector<double>* data, double mean_data, double sigma_data)
{
	int data_size = (*data).size();
	if( data_size != (*sign_data).size() )
	{
		std::cout << "Error, sign_size = " << (*sign_data).size() << " and data_size = " << data_size << " do not match!\n";
		return 0.0;
	}
	else
	{
		double ans = 0.0;
		
		for(int i=0;i<data_size;i++)
		{
			ans += ( (*sign_data)[i] - mean_sign ) * ( (*data)[i] - mean_data );
		}
		ans = ans / double( data_size );
		
		
		//std::cout << "correlation: " << ans / (sigma_data * sigma_sign) << std::endl;
		
		return ans / (sigma_data * sigma_sign);
	}
	
	
}





double error::get_mean(std::vector<double> *data)
{
	
	double ans = 0.0;
	for(auto c=(*data).begin();c!=(*data).end();c++)
	{
		ans += (*c);
	}
	ans = ans / double( (*data).size() );
	
	return ans;
	
	
}

double error::get_sigma(std::vector<double> *data)
{
	double ans = 0.0;
	double ans_sq = 0.0;
	
	for(auto c=(*data).begin();c!=(*data).end();c++)
	{
		ans += (*c);
		ans_sq += (*c)*(*c);
	}
	
	ans = ans / double( (*data).size() );
	ans_sq = ans_sq / double( (*data).size() );
	
	return sqrt( ans_sq - ans*ans );

}
