
/*
 * contains the class 'ensemble'
 * -> Stores configurations for each particle species as well as general info about the ensemble of all particles
 */


class config_ensemble
{
public:

	std::vector<worm_configuration> species_config; // contains one configuration for each species
	
	double get_sign(); // computes the sign of the current ensemble as the product of signs for all intial configs
	
	double total_energy; // energy of the current ensemble
	
	double total_force_sq;
	
	double total_diffusion_product;
	
	double total_exponent; // combined (mu)-exponent of all configurations
	
	
	// New book-keeping variables for the pair action
	double total_pair_action; // bookkeeping of all pair-actions in the system
	double total_pair_derivative; // bookkeeping of the derivative of the pair action
	
	
	double total_m_e_pair_derivative;
	double total_m_p_pair_derivative;
	
	
	// New book-keeping variables for the external potential action (i.e. snap-shot HTDM)
	double total_ext_pot_action;
	double total_ext_pot_action_derivative;
	
	
	
	
// 	double my_exp(); // exponential factors of the config. weights
	
	int diag; // Z- or G-sector config? (//TBD)
	int G_species; // ID of the species that currently is within G-sector

	int n_species; // total number of particle species
	int N_tot; // total number of particles of all species combined

	Parameters params; // contains all physical parameters like beta, mu and n_bead
			   // should access the number of particles from the individual configurations
			   
			   
	void print_standard_PIMC_ensemble_paths( std::string s ); // Print the paths of all worm_configs
	void print_standard_PIMC_ensemble_beadlist( std::string s ); // Print the beadlist of all worm_configs
	void print_standard_PIMC_ensemble_config( std::string s );
	
	double calculate_diffusion_product();
	
	
	

	

	
	
	
	
	

	
};












// Calculate the total diffusion product from the species_config
double config_ensemble::calculate_diffusion_product()
{
	double ans = 1.0;
	for(auto c=species_config.begin();c!=species_config.end();c++) // loop over all species
	{
		for(int iSlice=0;iSlice<c->params.n_bead;iSlice++) // loop over all propagators
		{
			for(auto b=c->beadlist[ iSlice ].begin();b!=c->beadlist[ iSlice ].end();b++) // loop over all particles on a single time-slice
			{
				if( (*b) != c->head_id )
				{
					ans *= c->beads[ (*b) ].get_diffusion_element();
				}
			}
		}
		
	}
	return ans;
}




// Print the beadlist of all worm_configs:
void config_ensemble::print_standard_PIMC_ensemble_beadlist( std::string s )
{
	for(int iSpecies=0;iSpecies<species_config.size();iSpecies++)
	{
		std::stringstream ss;
		ss << s << "_PIMC_ensemble_config_beadlist_" << iSpecies << ".info";
		species_config[ iSpecies ].standard_PIMC_print_beadlist( ss.str() );
	}
}





// Print the paths of all the worm_configs:
void config_ensemble::print_standard_PIMC_ensemble_paths( std::string s )
{
	for(int iSpecies=0;iSpecies<species_config.size();iSpecies++)
	{
		std::stringstream ss;
		ss << s << "_PIMC_ensemble_config_paths_" << iSpecies << ".info";
		species_config[ iSpecies ].standard_PIMC_print_paths( ss.str() );
	}
}




void config_ensemble::print_standard_PIMC_ensemble_config( std::string s )
{
	for(int iSpecies=0;iSpecies<species_config.size();iSpecies++)
	{
		std::stringstream ss;
		ss << s << "_PIMC_ensemble_config_config_" << iSpecies << ".info";
		species_config[ iSpecies ].standard_PIMC_print_config( ss.str() );
	}
}








// Calculate the total sign as the product from all species configs
double config_ensemble::get_sign()
{
	double tmp = 1.0;
	int Npp_total = 0;
	for(int sp = 0; sp < species_config.size(); sp++)
	{
		tmp = tmp*species_config[sp].my_sign;
	}
	
	return tmp;
}




