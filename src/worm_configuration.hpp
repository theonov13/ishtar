
/*
 * contains the class 'worm_configuration'
 * -> Stores all information about the Monte Carlo config, e.g. beads/coordinates, IDs of beads, matrices, determinants, ...
 */


class worm_configuration
{
public:

	double my_sign; // sign of the current worm_configuration
	double my_energy; // energy of the current config
	double my_force_sq; // sum over squared forces on all the beads, weighted by params.a1 factors
	double my_exp; // exponential factors of the config. weights
	
	int diag; // Z- or G-sector config?
	int head_id; // id of the worm's head
	int tail_id; // id of the worm's tail
	int head_kind, tail_kind; // slice type of head and tail
	
	std::vector<Bead> beads; // contains all existing beads
	std::vector<int> unused; // contains the ids of all beads in beads[] which are not currently in use (empty for PB-PIMC)

	Parameters params; // contains all physical parameters like beta, mu and n_bead
	

	std::vector<std::vector <int> >beadlist_A, beadlist_B, beadlist; // contains IDs of the beads in beads[] for all kinds of slices
	std::vector<int>next_id_list_A, next_id_list_B, next_id_list; // IDs of the beads which are not in use 

	std::vector<double>determinant_list_A, determinant_list_B, determinant_list; // contains the determinants of all diffusion matrices
	std::vector<double>sign_list_A, sign_list_B, sign_list; // contains the signs of all determinants
	
	std::vector<Matrix>matrix_list_A, matrix_list_B, matrix_list; // contains all diffusion matrices between all slices
	
	
	// Print data structure to a text file 's', only for debugging / illustration purposes
	void pbpimc_print_beads(std::string s);
	void pbpimc_print_beadlist(std::string s);
	void pbpimc_print_next(std::string s);
	
	
	void print_particles();
	void print_particles(std::string s);


	

	// Return the pointer to the beadlist of a specific slice type
	std::vector<std::vector <int> >* get_blist_pointer(int kind);
	
	// Return the pointer to the nlist of a specific slice type
	std::vector<int>* get_nlist_pointer(int kind);
	
	// Return the number of missing links in an open configuration:
	int missing_links();
	
	



	
	

	
	// ###############################################################################################################
	// ############################### Special functions for standard PIMC:
	// ###############################################################################################################
	
	
	// Return the ID in beads[...] of a new bead
	int get_new_bead();
	
	// Delete a bead from the beadlist[][], put the ID to the unused list
	void delete_bead( int id );

	
	// Print the beads (path pictures) of the standard PIMC
	void standard_PIMC_print_paths( std::string s );
	
	// Print the beadlist structure (including IDs, next_id and prev_id) to s
	void standard_PIMC_print_beadlist( std::string s );
	void standard_PIMC_print_config( std::string s );
	
	
	
	int Npp; // number of pair-permutations in the system
	int obtain_Npp(); // count the number of pair-exchanges in the system
	

	
};



























void worm_configuration::standard_PIMC_print_config(std::string s)
{
	std::fstream f;
	f.open( s.c_str(), std::ios::out );
	
	for(auto bl=beadlist.begin();bl!=beadlist.end();bl++)
	{
		int slice = bl - beadlist.begin();
		for(auto b=bl->begin();b!=bl->end();b++)
		{
			f << slice;
			for(int iDim=0;iDim<params.dim;iDim++)
			{
				f << "\t" << beads[ (*b) ].get_coord( iDim );
			}
			f << "\n";
		}
	}
	
	f.close();
}

// Calculate the number of pair-exchanges in the system, only meaningful for standard-PIMC
int worm_configuration::obtain_Npp()
{
	if( beadlist[0].size() == 0 ) return 0; 
	std::vector<int> counted_zero_beads;
	
	int start_slice = 0;
	if( !diag ) start_slice = beads[ head_id ].get_time();
	
	int new_Npp = 0;
	for(int i=0;i<beadlist[ start_slice ].size();i++)
	{
		int zero_id = beadlist[ start_slice ][ i ];
		if( !in_vector( zero_id, counted_zero_beads ) ) // check if the current zero_bead has already been encountered in a previous exchange cycle
		{
			counted_zero_beads.push_back( zero_id );

			int tmp_id = beads[ zero_id ].get_prev_id();
			
			while( tmp_id != zero_id )
			{
				if( tmp_id == tail_id ) break;
				
				int tmp_time = beads[ tmp_id ].get_time();
				
				if( tmp_time == start_slice )
				{
					new_Npp++;
					counted_zero_beads.push_back( tmp_id );
				}
				
				tmp_id = beads[ tmp_id ].get_prev_id();
			}
			
		} // end zero_bead condition
		
		
	} // end loop over all particles
	
	return new_Npp;
}

// Print the beadlist structure (including IDs, next_id and prev_id) to s
void worm_configuration::standard_PIMC_print_beadlist(std::string s)
{
	std::fstream f;
	f.open( s.c_str(), std::ios::out );
	
	for(int iSlice=0;iSlice<params.n_bead;iSlice++)
	{
		f << iSlice;
		for(auto c=beadlist[iSlice].begin();c!=beadlist[iSlice].end();c++)
		{
			f << "\t" << (*c) << "\t" << beads[(*c)].get_prev_id() << "\t" << beads[(*c)].get_next_id() << "\t" << beads[(*c)].get_time() << "\t" << beads[(*c)].get_real_time() << "\t" << beads[(*c)].get_mass() << "\t" << beads[(*c)].get_charge() << "\t |||";
		}
		f << "\n";
		
		
	} // end loop iSlice
	
	
	
	f.close();
}

// Print the beads (path pictures) of the standard PIMC
void worm_configuration::standard_PIMC_print_paths(std::string s)
{
	std::fstream f;
	f.open( s.c_str(), std::ios::out );
	
	for(int iSlice=0;iSlice<params.n_bead;iSlice++)
	{
		for(int i=0;i<beadlist[iSlice].size();i++)
		{
			int my_id = beadlist[iSlice][i];
			int prev_id = beads[my_id].get_prev_id();
			int next_id = beads[my_id].get_next_id();
		
			int time = beads[my_id].get_time();

			if( prev_id >= 0 )
			{
				f << beads[ prev_id ].get_time();
				for(int iDim=0;iDim<params.dim;iDim++)
				{
					double x_prev = beads[ prev_id ].get_coord( iDim );
					double x = beads[ my_id ].get_coord( iDim );
					
					if( params.system_type < params.n_traps )
					{
						f << "\t" << beads[ prev_id ].get_coord( iDim );
					}
					else 
					{
						double diff = x_prev - x;
						if( (diff >= -0.5*params.length) && (diff <= 0.5*params.length) )
						{
							f << "\t" << beads[ prev_id ].get_coord( iDim );
						}
					}
				}
				f << "\n";
			}
			f << time;
			for(int iDim=0;iDim<params.dim;iDim++)
			{
					f << "\t" << beads[ my_id ].get_coord( iDim );
			}
			f << "\n";
			if( next_id >= 0 )
			{
				f << beads[ next_id ].get_time();
				for(int iDim=0;iDim<params.dim;iDim++)
				{
					
					double x_next = beads[ next_id ].get_coord( iDim );
					double x = beads[ my_id ].get_coord( iDim );
					
					if( params.system_type < params.n_traps )
					{
						f << "\t" << beads[ next_id ].get_coord( iDim );
					}
					else 
					{
						double diff = x_next - x;
						if( (diff >= -0.5*params.length) && (diff <= 0.5*params.length) )
						{
							f << "\t" << beads[ next_id ].get_coord( iDim );
						}
					}
				}
				f << "\n";
			}
			
			
			f << "\n";
			f << "\n";

			
		
		} // end loop i
	} // end loop iSlice
	
	
	
	
	
	
	f.close();
	
}

// Return the ID of a new bead, create one if unused is empty
int worm_configuration::get_new_bead()
{
	int size = unused.size();
	if( size > 0 ) // if unused is not empty, return ID of the last bead in it
	{
		int id = unused[ size-1 ];
		unused.resize( size-1 );
		return id;
	}
	else // otherwise create a new bead and return its ID
	{
		int id = beads.size();
		beads.push_back( beads[0] );
		
		// the new bead has not prev. and next id until otherwise specified in the updates
		beads[id].set_prev_id( -1 );
		beads[id].set_next_id( -1 ); 
		
		return id;
	}
}

// Delete the bead with ID id from beadlist and add the id to the unused list
void worm_configuration::delete_bead(int id)
{
	// obtain the time slice of the bead to be deleted 
	int time = beads[ id ].get_time();
	int size = beadlist[ time ].size();
	
	// add the id to the list of unused beads
	unused.push_back( id );
	
	// find the id in the beadlist[time][...] list
	int slot = -1;

	for(int i=0;i<size;i++)
	{
		int it = beadlist[ time ][ i ];
		if( it == id )
		{
			slot = i;
			break;
		}
	}
	
	if( slot < 0 )
	{
		std::cout << "Error in delete_bead, ID: " << id << " has not been found in beadlist, time: " << time << "\n";
		exit(0);
	}
	
	// Swap slot and last element of beadlist[time] and reduce size by one:
	beadlist[ time ][ slot ] = beadlist[ time ][size-1];
	beadlist[ time ].resize( size-1 );

	
	
	
}










// Write the coordinates of each particle into a separate file and make a plot
void worm_configuration::print_particles(std::string s)
{
	for(int i=0;i<params.N;i++) // loop over all particles
	{
		std::stringstream ss;
		ss << s << "_particle_" << i << ".dat";
		
		std::fstream f;
		f.open( ss.str().c_str(), std::ios::out );
		
		for(int iSlice=0;iSlice<params.n_bead;iSlice++) // loop over all beads of each particle
		{
			int id = beadlist[iSlice][i];
			int id_A = beadlist_A[iSlice][i];
			int id_B = beadlist_B[iSlice][i];
			
			int next_id = next_id_list[iSlice];
			int next_id_A = next_id_list_A[iSlice];
			int next_id_B = next_id_list_B[iSlice];
			
			if( id != next_id )
			{
				f << iSlice;
				for(int iDim=0;iDim<params.dim;iDim++)
				{
					f << "\t" << beads[id].get_coord(iDim);
				}
				f << "\n";
			}
			
			if( id_A != next_id_A )
			{
				f << iSlice;
				for(int iDim=0;iDim<params.dim;iDim++)
				{
					f << "\t" << beads[id_A].get_coord(iDim);
				}
				f << "\n";
			}
			
			if( id_B != next_id_B )
			{
				f << iSlice;
				for(int iDim=0;iDim<params.dim;iDim++)
				{
					f << "\t" << beads[id_B].get_coord(iDim);
				}
				f << "\n";
			}
			

		} // end loop over all beads from a particle
		
		f.close();

	} // end loop over all particles
}

// Write the coordinates of each particle into a separate file and make a plot
void worm_configuration::print_particles()
{
	for(int i=0;i<params.N;i++) // loop over all particles
	{
		std::stringstream ss;
		ss << "particle_" << i << ".dat";
		
		std::fstream f;
		f.open( ss.str().c_str(), std::ios::out );
		
		for(int iSlice=0;iSlice<params.n_bead;iSlice++) // loop over all beads of each particle
		{
			int id = beadlist[iSlice][i];
			int id_A = beadlist_A[iSlice][i];
			int id_B = beadlist_B[iSlice][i];
			
			int next_id = next_id_list[iSlice];
			int next_id_A = next_id_list_A[iSlice];
			int next_id_B = next_id_list_B[iSlice];
			
			if( id != next_id )
			{
				f << iSlice;
				for(int iDim=0;iDim<params.dim;iDim++)
				{
					f << "\t" << beads[id].get_coord(iDim);
				}
				f << "\n";
			}
			
			if( id_A != next_id_A )
			{
				f << iSlice;
				for(int iDim=0;iDim<params.dim;iDim++)
				{
					f << "\t" << beads[id_A].get_coord(iDim);
				}
				f << "\n";
			}
			
			if( id_B != next_id_B )
			{
				f << iSlice;
				for(int iDim=0;iDim<params.dim;iDim++)
				{
					f << "\t" << beads[id_B].get_coord(iDim);
				}
				f << "\n";
			}
			

		} // end loop over all beads from a particle
		
		f.close();

	} // end loop over all particles
}

// Print the IDs of the missing beads to the textfile 's'
void worm_configuration::pbpimc_print_next(std::string s)
{
	std::fstream f;
	f.open(s.c_str(), std::ios::out);
	
	for(int i=0;i<params.n_bead;i++)
	{
		f << "i: " << i << "\t" << next_id_list[i] << "\n";
		f << "A: " << i << "\t" << next_id_list_A[i] << "\n";
		f << "B: " << i << "\t" << next_id_list_B[i] << "\n";
	}
	
	f.close();
}

// Print the IDs of all beadlists into the textfile 's'
void worm_configuration::pbpimc_print_beadlist(std::string s)
{
	std::fstream f;
	f.open(s.c_str(),std::ios::out);
	
	for(int i=0;i<params.n_bead;i++)
	{
		f << i << "\t";
		for(int k=0;k<params.N;k++)
		{
			int main_id = beadlist[i][k];
			int A_id = beadlist_A[i][k];
			int B_id = beadlist_B[i][k];
			
			f << "main: " << main_id << "\tA: " << A_id << "\tB: " <<  B_id << "\t";

		}
		
		f << "\n";
	}
	f.close();
}

// Print all coords of all beads into textfile 's'
void worm_configuration::pbpimc_print_beads(std::string s)
{
	std::fstream f;
	f.open(s.c_str(),std::ios::out);
	
	
	for(int n=0;n<params.N;n++)
	{
		
		for(int k=0;k<params.n_bead;k++)
		{

			int id=beadlist[k][n];
			if(id!=next_id_list[k])
			{
				f << k;
				for(int iDim=0;iDim<params.dim;iDim++)
				{
					f << "\t" << beads[id].get_coord(iDim);
				}
				f << "\n";
			}
			
			int id_A = beadlist_A[k][n];
			if(id_A!=next_id_list_A[k])
			{
				f << k;
				for(int iDim=0;iDim<params.dim;iDim++)
				{
					f << "\t" << beads[id_A].get_coord(iDim);
				}
				f << "\n";
			}
			
			int id_B = beadlist_B[k][n];
			if(id_B!=next_id_list_B[k])
			{
				f << k;
				for(int iDim=0;iDim<params.dim;iDim++
					
				){
					f << "\t" << beads[id_B].get_coord(iDim);
				}
				f << "\n";
			}
		}
		
		f << "\n";
		f << "\n";
		f << "\n";
	}
	
	return;
}

// Return a pointer to the beadlist of slice type 'kind'
std::vector<std::vector <int> >* worm_configuration::get_blist_pointer(int kind)
{
	if( kind == 1 )
	{
		// ancilla slice A
		return &beadlist_A;
	}
	else if( kind == 2 )
	{
		// ancilla slice B
		return &beadlist_B;
	}
	else
	{
		// main slice
		return &beadlist;
	}
}

// Return a pointer to the nlist of slice type 'kind'
std::vector<int>* worm_configuration::get_nlist_pointer(int kind)
{
	if( kind == 1 )
	{
		// ancilla slice A
		return &next_id_list_A;
	}
	else if( kind == 2 )
	{
		// ancilla slice B
		return &next_id_list_B;
	}
	else
	{
		// main slice
		return &next_id_list;
	}
}

// Return the number of missing links between head and tail, m
int worm_configuration::missing_links()
{
	int m = 0;
	int t_kind = head_kind; // initialize to kind of head (A,B,main)
	int t_time = beads[head_id].get_time(); // initialize to propagator number of the head
	
	while(true)
	{
		if( t_kind == 3 ) t_kind = 0; // reset the slice kind id
		t_kind++;
		m++;
		
		if( t_kind == 3 )
		{
			t_time++; // increase the time slice every time you encounter a main slice
			if(t_time == params.n_bead) t_time = 0;
		}
		
		if( t_time == beads[tail_id].get_time() )
		{
			if(t_kind == tail_kind)
			{
				break; // break up the loop once you encounter the slice + kind of the tail
			}
		}
		
		
		
	}

	return m;
}
