/*
 * Contains the class change_config
 * - update all forces from a const_store_the_force type structure
 */


class change_config{
public:
	
	
	// Updates the forces of all beads in blist, except next_id (missing bead) and skip_id (which has been changed during an update) 
	void update_forces(std::vector<int>* blist, int next_id, int skip_id, std::vector<std::vector <double> >* store);
	
	void init(worm_configuration* new_config);
	
	
	void ensemble_update_forces( config_ensemble* ensemble, int b_species, int b_time, int b_kind, std::vector<int>* blist, int next_id, int skip_id, std::vector<std::vector <double> >* store, std::vector<std::vector<std::vector <double> > >* changed_species);
	
	
	
	
	// Information about the config:
	worm_configuration* config;
	
	
};




void change_config::init(worm_configuration* new_config)
{
	config = new_config;
}
	
	
	
	
	
void change_config::ensemble_update_forces( config_ensemble* ensemble, int b_species, int b_time, int b_kind, std::vector<int>* blist, int next_id, int skip_id, std::vector<std::vector <double> >* store, std::vector<std::vector<std::vector <double> > >* changed_species)
{
	// Update the force on beads from the same species:
	for(int i=0;i<(*blist).size();i++) // loop over all beads in blist
	{
		int id = (*blist)[i];
		if( ( id != next_id ) && ( id != skip_id ) ) // only update existing beads or beads which have not been changed in the update
		{
			(*ensemble).species_config[b_species].beads[id].set_force( (*store)[i] );
		}
	}
	
	// Update the forces on beads from all other species:
	for(int iSpecies=0;iSpecies<(*ensemble).n_species;iSpecies++)
	{
		if( iSpecies != b_species ) // Exclude the changed species
		{
			// Loop over all particles from iSpecies
			for(int a=0;a<(*ensemble).species_config[iSpecies].params.N;a++) // loop over all particles of iSpecies
			{
				int a_id;
				
				if( b_kind == 1 ) // A
				{
					a_id = (*ensemble).species_config[iSpecies].beadlist_A[b_time][a];
				}
				else if ( b_kind == 2) // B
				{
					a_id = (*ensemble).species_config[iSpecies].beadlist_B[b_time][a];
				}
				else // main
				{
					a_id = (*ensemble).species_config[iSpecies].beadlist[b_time][a];
				}
				
				Bead* partner = &(*ensemble).species_config[iSpecies].beads[a_id];

				(*partner).set_force( (*changed_species)[iSpecies][a] );

			} // end loop over all particles, a
			
			
		}
		
		
	}
	
	
	
}
	
	
	
	
	
	
	

void change_config::update_forces(std::vector<int>* blist, int next_id, int skip_id, std::vector<std::vector <double> >* store)
{
	
	for(int i=0;i<(*blist).size();i++) // loop over all beads in blist
	{
		int id = (*blist)[i];
		
		if( ( id != next_id ) && ( id != skip_id ) ) // only update existing beads or beads which have not been changed in the update
		{
			(*config).beads[id].set_force( (*store)[i] );
		}
		
		
	}
	
	
	
}
