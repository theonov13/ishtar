/* List of some future issues:
 *
 * - 2) extensive and well documented tests of PA HEG, different "action infrastructure" interaction classes etc
 *
 *
 *
 */



#include <iostream>
#include <iterator>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <random>
#include <algorithm>
#include <chrono>

#include <omp.h>
#include <stdio.h>
#include <sched.h>


#include "hdf5_support.hpp" // Save measurements / estimators as hdf5 files

#include "stuff.hpp" // everything else, like random numbers, def. of PI, etc.
#include "matrix.hpp" // Class: Matrix
#include "parameters.hpp" // Class: Parameters
#include "worm_beads.hpp" // Class: Bead
#include "worm_configuration.hpp" // Class: worm_configuration
#include "config_ensemble.hpp" // Class: config_ensemble, for different particle species (e.g. unpolarized)


// GSL headers for determinant / inverse matrix
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_cblas.h>

// Read program parameters from config-file or via command line
#include <boost/program_options.hpp>


#include "estimator.hpp" // Classes: estimator_array and estimator_array_set
#include "force_toolbox.hpp" // Class: force_toolbox
#include "pair_action_toolbox.hpp" // Class: pair_action_toolbox




#include "observables/observable_density.hpp" // Class: observable_density
#include "observables/observable_density_improved.hpp" // Class: observable_density (improved from partition function)
#include "observables/observable_density_3D.hpp" // Actual 3D histogram
#include "observables/observable_force_on_ions.hpp" // Compute the force on all ions
#include "observables/observable_density_strip.hpp" // For "linear response" perturbed simulation
#include "observables/observable_density_induced.hpp" // For "linear response" perturbed simulation
#include "observables/observable_energy.hpp" // Class: observable_density
#include "observables/observable_bogoliubov.hpp"
#include "observables/observable_static_structure_factor.hpp" // Class: Static structure factor for standard PIMC and also PB-PIMC
#include "observables/observable_density_response_inter.hpp" // Class: observable_density_response_inter
#include "observables/observable_momentum.hpp" // Class: Direct Implementation of the momentum distribution from the off-diagonal simulation
#include "observables/observable_offdiagonal_density_matrix.hpp" // Class: observable_offdiagonal_density_matrix
#include "observables/observable_offdiagonal_sign.hpp" // CLass: observable_offdiagonal_sign 
#include "observables/observable_extended_information.hpp" // Class: observable_extended_information
#include "observables/observable_density_induced_full.hpp" // For "linear response" perturbed simulation, full q-dependence
#include "observables/observable_c2p.hpp" // Class: observable_c2p
#include "observables/observable_c2p_lsf.hpp" // Class: observable_c2p_lsf
#include "observables/observable_intra_cf.hpp" // Class observable_intra_cf
#include "observables/observable_inter_cf.hpp" // Class observable_intra_cf
#include "observables/observable_standard_PIMC_energy.hpp" // Class observable_standard_PIMC_energy
#include "observables/observable_standard_PIMC_trap_superfluidity.hpp" // Class observable_standard_PIMC_trap_superfluidity
#include "observables/observable_standard_PIMC_lsf.hpp" // Class observable_standard_PIMC_lsf
#include "observables/observable_standard_PIMC_energy_pair_approx.hpp"// Class for the pair-approximation  in standard_pimc
#include "observables/observable_standard_PIMC_permutations_correlation.hpp" // Class observable_standard_PIMC_permutations
#include "observables/observable_standard_PIMC_permutations.hpp" // Class observable_standard_PIMC_permutations
#include "observables/observable_standard_PIMC_winding.hpp" // Class observable_standard_PIMC_winding
#include "observables/observable_standard_PIMC_bogoliubov.hpp" // Class observable_standard_PIMC_bogoliubov
#include "observables/observable_monopole_cf.hpp" // Class: observable_monopole_cf (for estimation of the quantum breathing mode, traps)
#include "observables/observable_standard_PIMC_Kelbg_two_component.hpp" // Energy estimation etc for full two-component Kelbg implementation
#include "observables/observable_proton_electron_cf.hpp"
#include "observables/observable_proton_cf.hpp"



#include "selection_toolbox.hpp" // Class: selection_toolbox
#include "sampling_toolbox.hpp" // Class: sampling_toolbox
#include "interaction_toolbox.hpp" // Class: interaction_toolbox
#include "diffusion_toolbox.hpp" // Class: diffusion_toolbox



#include "interactions/coulomb_HO.hpp" //harmonic confinement with Coulomb interaction -> electrons in a quantum dot
// #include "interactions/dipole_HO.hpp" //  harmonic confinement with dipole interaction -> trapped ultracold atoms [implemented on-demand, off]
// #include "interactions/coulomb_HO_repulsive.hpp" // harmonic confinement with Coulomb interaction -> electrons in a quantum dot, Bogoliubov repulsion [implemented on-demand, off]
// EMPTY
// EMPTY
// #include "interactions/yakub_HEG.hpp" // homogeneous electro with Yakub/Ronchi interaction [implemented on-demand, off]
// #include "interactions/yakub_dipole_HEG.hpp" // homogeneous electron gas in PBC with Yakub/Ronchi interaction and dipole term [implemented on-demand, off]
// #include "interactions/yakub_MPC_HEG.hpp" // homogeneous electron gas in PBC with Yakub/Ronchi interaction [implemented on-demand, off]
// #include "interactions/coulomb_nn_HEG.hpp" // homogeneous electron gas in PBC with Coulomb-NN interaction [implemented on-demand, off]
#include "interactions/perturbed_ewald_HEG.hpp" // PBC with COULOMB EWALD interaction -> homogeneous electron gas
#include "interactions/perturbed_ideal_Fermi.hpp" // Perturbed ideal Fermi Gas
// #include "interactions/ewald_HEG_repulsive.hpp" // PBC with COULOMB EWALD interaction -> homogeneous electron gas [implemented on-demand, off]
// EMPTY
#include "interactions/ion_snapshot.hpp" // A PIMC Interaction inspired by the HEG with an ionic potential. Ions are fixed positions
// #include "interactions/ewald_HEG_pair_approx.hpp" // An implementation of the HEG pair approximation using the HTDM lookup table
// EMPTY
#include "interactions/Kelbg_ion_snapshot.hpp" // A PIMC Interaction inspired by the HEG with an ionic potential based on Kelbg. Ions are fixed positions
// #include "interactions/Kelbg_two_component.hpp" // A PIMC Interaction with spin-up/-down electrons, and also ions, with the ions being actual paths/beads [implemented on-demand, off]
// EMPTY
// #include "interactions/Kelbg_UEG.hpp" // UEG, but with Kelbg potential between the electrons. Benchmark purposes [implemented on-demand, off]
// #include "interactions/Kelbg_UEG_action.hpp" // UEG, but with Kelbg potential, and actually using the action infrastructure! Benchmark for pair density matrix! [implemented on-demand, off]
#include "interactions/Kelbg_two_component_action.hpp" // Two-component with Kelbg potential between all components, using the action infrastructure!
#include "interactions/Hydrogen_PA.hpp" // Two-component (hyrogen for now) with PA between e-p, otherwise Coulomb


#include "change_config.hpp" // Class: change_config




#include "wormupdates/update_pbpimc_swap.hpp" // Class: update_pbpimc_swap
#include "wormupdates/update_pbpimc_open.hpp" // Class: update_pbpimc_open
#include "wormupdates/update_pbpimc_close.hpp" // Class: update_pbpimc_close
#include "wormupdates/update_pbpimc_deform_universal.hpp" // Class: update_pbpimc_deform_universal

#include "wormupdates/update_standard_PIMC_offdiag_open.hpp" // Class: update_standard_PIMC_offdiag_open
#include "wormupdates/update_standard_PIMC_offdiag_close.hpp" // Class: update_standard_PIMC_offdiag_close
#include "wormupdates/update_standard_PIMC_wriggle.hpp" // Class: update_standard_PIMC_wriggle

#include "wormupdates/update_standard_PIMC_deform.hpp" // Class: update_standard_PIMC_deform
#include "wormupdates/update_standard_PIMC_insert.hpp" // Class: update_standard_PIMC_insert
#include "wormupdates/update_standard_PIMC_remove.hpp" // Class: update_standard_PIMC_insert
#include "wormupdates/update_standard_PIMC_advance.hpp" // Class: update_standard_PIMC_advance
#include "wormupdates/update_standard_PIMC_recede.hpp" // Class: update_standard_PIMC_recede
#include "wormupdates/update_standard_PIMC_close.hpp" // Class: update_standard_PIMC_close
#include "wormupdates/update_standard_PIMC_open.hpp" // Class: update_standard_PIMC_open
#include "wormupdates/update_standard_PIMC_swap.hpp" // Class: update_standard_PIMC_swap
#include "wormupdates/update_standard_PIMC_move.hpp" // Class: update_standard_PIMC_swap
#include "wormupdates/update_standard_PIMC_multi_move.hpp" // Class update_standard_PIMC_multi_move

#include "plot_tools.hpp" // Class: plot_tools


#include "wormupdates/update_pbpimc.hpp" // Class: update_pbpimc
#include "observables/observable.hpp" // Class: observable

#include "monte_carlo.hpp" // Class: Monte_Carlo


// Handling the IO for the snapshot interaction

#include "io/snapshot_handler.hpp"

#include "read_config.hpp"



const int n_traps = 3; // The number of possible system_types that are traps ( atm: coulomb_HO, dipole_HO, Bogoliubov HO )
const int n_multi = 3; // The number of simulation_types that are not related to standard PIMC [ 0 is PB-PIMC, 1/2 are empty at the moment ]








// Execution of the Monte Carlo run
void pelican( plot_tools plot, update_probs user_probs, Parameters p )
{

	Monte_Carlo< coulomb_HO > my_mc;                                                 // '0'
// 	Monte_Carlo< dipole_HO > my_mc_dipole;                                           // '1', on-demand
// 	Monte_Carlo< coulomb_HO_repulsive > my_mc_coulomb_repulsive;                     // '2', on-demand
//	EMPTY!
//	EMPTY!
// 	Monte_Carlo< yakub_HEG > my_mc_yakub;                                            // '5', on-demand
// 	Monte_Carlo< yakub_dipole_HEG > my_mc_yakub_dipole;                              // '6', on-demand
// 	Monte_Carlo< coulomb_nn_HEG > my_mc_coulomb_nn;                                  // '7', on-demand
// 	Monte_Carlo< yakub_MPC_HEG > my_mc_yakub_MPC;                                    // '8', on-demand
	Monte_Carlo< perturbed_ewald_HEG > my_mc_PERTURBED;                              // '9'
	Monte_Carlo< perturbed_ideal_Fermi > my_mc_ideal_PERTURBED;                      // '10'
// 	Monte_Carlo< ewald_HEG_repulsive > my_mc_ewald_repulsive;                        // '11', on-demand
//  EMPTY!
	Monte_Carlo< ion_snapshot > my_mc_ion_snapshot;                                  // '13'
// 	Monte_Carlo< ewald_HEG_pair_approx> my_mc_ewald_pair_approx;                     // '14', on-demand
//  EMPTY!
	Monte_Carlo< Kelbg_ion_snapshot > my_mc_Kelbg_ion_snapshot;                      // '16'
// 	Monte_Carlo< Kelbg_two_component > my_mc_Kelbg_two_component;                    // '17', on-demand
//  EMPTY!
// 	Monte_Carlo< Kelbg_UEG > my_mc_Kelbg_UEG;                                        // '19', on-demand
// 	Monte_Carlo< Kelbg_UEG_action > my_mc_Kelbg_UEG_action;                          // '20', on-demand
	Monte_Carlo< Kelbg_two_component_action > my_mc_Kelbg_two_component_action;      // '21'
	Monte_Carlo< Hydrogen_PA > my_mc_Hydrogen_PA;                                    // '22'


	switch(p.system_type)
	{
		case 0:
			std::cout << "System type is coulomb_HO\n";
			my_mc.init(  plot, user_probs, p  );
			my_mc.monte_carlo();
			break;
		case 1:
			std::cout << "System type is dipole_HO [implemented on-demand, off]\n";
// 			my_mc_dipole.init(  plot, user_probs, p   );
// 			my_mc_dipole.monte_carlo();
			exit(1);
			break;
		case 2:
			std::cout << "System type is coulomb HO with Bogoliubov!!! [implemented on-demand, off]\n";
//  		my_mc_coulomb_repulsive.init(  plot, user_probs, p   );
// 			my_mc_coulomb_repulsive.monte_carlo();
			exit(1);
			break;
		case 3:
			std::cout << "Empty system_type 3.\n";
			exit(1);
			break;
		case 4:
			std::cout << "Empty system_type 4.\n";
			exit(1);
			break;
		case 5:
			std::cout << "System type is Yakub HEG [implemented on-demand, off]\n";
// 			my_mc_yakub.init(  plot, user_probs, p  );
// 			my_mc_yakub.monte_carlo();
			exit(1);
			break;
		case 6:
			std::cout << "System type is Yakub HEG with dipole [implemented on-demand, off]\n";
// 			my_mc_yakub_dipole.init(  plot, user_probs, p  );
// 			my_mc_yakub_dipole.monte_carlo();
			exit(1);
			break;
		case 7:
			std::cout << "System type is HEG with Coulomb_nn [implemented on-demand, off]\n";
// 			my_mc_coulomb_nn.init(  plot, user_probs, p  );
// 			my_mc_coulomb_nn.monte_carlo();
			exit(1);
			break;
		case 8:
			std::cout << "System type is HEG with Yakub_MPC [implemented on-demand, off]\n";
// 			my_mc_yakub_MPC.init(  plot, user_probs, p  );
// 			my_mc_yakub_MPC.monte_carlo();
			exit(1);
			break;
		case 9:
			std::cout << "System type is HEG with EWALD, PERTURBED!!!!!!\n";
			my_mc_PERTURBED.init(  plot, user_probs, p  );
			my_mc_PERTURBED.monte_carlo();
			break;
		case 10:
			std::cout << "System type is ideal Fermi, PERTURBED!!!!!!\n";
			my_mc_ideal_PERTURBED.init(  plot, user_probs, p  );
			my_mc_ideal_PERTURBED.monte_carlo();
			break;
		case 11:
			std::cout << "System type is HEG with EWALD, but additional repulsion [implemented on-demand, off]\n";
// 			my_mc_ewald_repulsive.init(  plot, user_probs, p  );
// 			my_mc_ewald_repulsive.monte_carlo();
			exit(1);
			break;
	    case 12:
	        std::cout << "Empty system_type 12." << std::endl;
	        exit(1);
	        break;
	    case 13:
	        std::cout << "System type is the electron ion snapshot system" << std::endl;
	        my_mc_ion_snapshot.init( plot, user_probs, p );
            my_mc_ion_snapshot.monte_carlo();
            break;
        case 14:
            std::cout << "System type is the HEG using the pair approximation [implemented on-demand, off]" << std::endl;
//          my_mc_ewald_pair_approx.init(  plot, user_probs, p );
//          my_mc_ewald_pair_approx.monte_carlo();
			exit(1);
            break;
	    case 15:
	        std::cout << "Empty system_type 15." << std::endl;
            exit(1);
			break;
	    case 16:
	        std::cout << "System type is the electron ion snapshot system with Kelbg-e-p potential" << std::endl;
	        my_mc_Kelbg_ion_snapshot.init( plot, user_probs, p );
            my_mc_Kelbg_ion_snapshot.monte_carlo();
            break;
	    case 17:
	        std::cout << "System type is the full electron-ion two-component system with Kelbg-e-p potential [implemented on-demand, off]" << std::endl;
// 	        my_mc_Kelbg_two_component.init( plot, user_probs, p );
//          my_mc_Kelbg_two_component.monte_carlo();
			exit(1);
            break;
	    case 18:
	        std::cout << "Empty system_type 18." << std::endl;
	        exit(1);
            break;
		case 19:
	        std::cout << "System type is the usual UEG, but with Kelbg e-e interaction [implemented on-demand, off]" << std::endl;
// 	        my_mc_Kelbg_UEG.init( plot, user_probs, p );
//          my_mc_Kelbg_UEG.monte_carlo();
			exit(1);
            break;
		case 20:
	        std::cout << "System type is the usual UEG, but with Kelbg e-e interaction, and action infrastructure [implemented on-demand, off]" << std::endl;
// 	        my_mc_Kelbg_UEG_action.init( plot, user_probs, p );
//          my_mc_Kelbg_UEG_action.monte_carlo();
			exit(1);
            break;
		case 21:
	        std::cout << "System type is two-component, Kelbg interaction between all species, and action infrastructure" << std::endl;
	        my_mc_Kelbg_two_component_action.init( plot, user_probs, p );
            my_mc_Kelbg_two_component_action.monte_carlo();
            break;
		case 22:
	        std::cout << "System type is two-component Hydrogen with PA" << std::endl;
	        my_mc_Hydrogen_PA.init( plot, user_probs, p );
            my_mc_Hydrogen_PA.monte_carlo();
            break;
        default:
			std::cout << "Error: system_type = " << p.system_type << " is not defined!\n";
			exit(0);
	}



}









int main(int ac, char* av[])
{
	// First action item: get the cpu-id:
	int cpu_num = sched_getcpu();
	std::cout << "Which core am I? -- " << cpu_num << "\n";

	
	string file_name_int = "pelican.h5";
    H5File file = init_HDF5_file(file_name_int);

	std::cout.precision( 10 );


	read_config(ac, av);


	input_params.n_traps = n_traps;






	// For two-component systems, we shall activate the proton updates.
	if( ( input_params.system_type == 17 ) || ( input_params.system_type == 21 ) || ( input_params.system_type == 22 ) )
	{
		user_probs.P_standard_PIMC_deform_proton = user_probs.P_standard_PIMC_deform;
		user_probs.P_standard_PIMC_deform_offdiag_proton = user_probs.P_standard_PIMC_deform_offdiag;
		
		user_probs.P_standard_PIMC_move_proton = user_probs.P_standard_PIMC_move;
		user_probs.P_standard_PIMC_move_offdiag_proton = user_probs.P_standard_PIMC_move_offdiag;

		user_probs.P_standard_PIMC_offdiag_close_proton = user_probs.P_standard_PIMC_offdiag_close;
		user_probs.P_standard_PIMC_offdiag_open_proton = user_probs.P_standard_PIMC_offdiag_open;
		user_probs.P_standard_PIMC_wriggle_proton = user_probs.P_standard_PIMC_wriggle;

	}
	else
	{
		user_probs.P_standard_PIMC_deform_proton = 0.0;
		user_probs.P_standard_PIMC_deform_offdiag_proton = 0.0;
		user_probs.P_standard_PIMC_move_proton = 0.0;
		user_probs.P_standard_PIMC_move_offdiag_proton = 0.0;
		user_probs.P_standard_PIMC_multi_move = 0.0;
		user_probs.P_standard_PIMC_multi_move_offdiag = 0.0;
	}
	
	








	input_params.n_species = 1;
	if( ( input_params.N_up > 0 ) && ( input_params.N_down > 0 ) ) input_params.n_species = 2; // unpolarized simulations
	if( (input_params.system_type == 17 ) || (input_params.system_type == 21) || (input_params.system_type == 22) ) input_params.n_species += 1; // Add Boltzmannons to the species mix















	// Tell the user about the simulation_type


	input_params.n_multi = n_multi;
    if( input_params.simulation_type < n_multi )
	{
		switch( input_params.simulation_type )
		{
			case 0:
				std::cout << "~~~~~~~~~simulation_type: " << input_params.simulation_type << " ( PB-PIMC )\n";
				break;
			case 1:
				std::cout << "Error; empty\n";
				exit(1);

				break;
			case 2: std::cout << "Error; empty(2)\n";
				exit(1);

				break;
			default:
				std::cout << "~~~~~~~~~simulation_type: " << input_params.simulation_type << " DOES NOT EXIST, ERROR!\n";
				exit(1);
		}
	}
	else // and for the standard PIMC simulation_types
	{


		switch( input_params.simulation_type-n_multi )
		{

			case 0:
				std::cout << "~~~~~~~~~simulation_type: " << input_params.simulation_type << " ( standard PIMC for boltzmannons (canonical) )\n";
				break;
			case 1:
				std::cout << "~~~~~~~~~simulation_type: " << input_params.simulation_type << " ( WA-PIMC )\n";
				break;
			case 2:
				std::cout << "~~~~~~~~~simulation_type: " << input_params.simulation_type << " ( offdiag-WA-PIMC )\n";
				break;
			default:
				std::cout << "~~~~~~~~~simulation_type: " << input_params.simulation_type << " DOES NOT EXIST, ERROR!\n";
				exit(0);
		}
	}







	// #################################################################################################################################
	// ########### Set some simulation parameters automatically
	// #################################################################################################################################



	input_params.N = input_params.N_up + input_params.N_down;
	input_params.N_tot = input_params.N_up + input_params.N_down;


	// Calculate the rest of the CHIN approximation parameters:
	input_params.u0 = ( 1.0 - 1.0/(1.0-2.0*input_params.t0) + 1.0/(6.0*pow(1.0-2.0*input_params.t0,3.0)) ) / 12.0;
	input_params.v1 = 1.0/(6.0*pow(1.0-2.0*input_params.t0,2.0));
	input_params.v2 = 1.0-2.0*input_params.v1;
	input_params.t1 = 0.50-input_params.t0;

	// Calculate the propagator imaginary time step:
	input_params.epsilon = input_params.beta / double( input_params.n_bead );


	// for the trap, the volume is a box (square) with twice 'length' as a side length!
	input_params.volume = pow( 2.0*input_params.length, input_params.dim );



	// Calculate box length and beta for the HEG/ ideal_Fermi:
	if( input_params.system_type >= n_traps )
	{
		input_params.lambda = 1.0; // lambda is unity and not used at all!
		input_params.length = pow( 4.0*pi*input_params.N/3.0, 1.0/3.0 ) * input_params.rs; // length (and rs) is calculated from the total number of (spin-up and spin-down) electrons

		input_params.dim = 3; // The HEG is 3D
		input_params.volume = pow( input_params.length, input_params.dim );

		if( input_params.N_down > 0 )
		{ // This is unpolarized
			if( input_params.N_down > input_params.N_up )
			{
				int n_tmp = input_params.N_down;
				input_params.N_down = input_params.N_up;
				input_params.N_up = n_tmp;
			}
			input_params.beta = 2.0 * input_params.length * input_params.length * pow( 6.0*input_params.N_up*pi*pi, -2.0/3.0 ) / input_params.theta;
		}
		else
		{ // This is polarized
			input_params.beta = 2.0 * input_params.length * input_params.length * pow( 6.0*input_params.N*pi*pi, -2.0/3.0 ) / input_params.theta;
		}

		input_params.epsilon = input_params.beta / double( input_params.n_bead );

		// Pre-calculate the exponential functions from the Ewald summation:
		input_params.fill_Ewald_exp_store();

		std::cout << "///// fill_Ewald_exp_store ///// \n";
	}

	// Load the ion positions
	if( (input_params.system_type == 13) || (input_params.system_type == 16) )
    {
        snapshot_handler handler(input_params.length);
        ion_container cont = handler.read_input_file(input_params.snap_path);
        input_params.set_ion_container(cont);
	}
	

	// For the HEG with the pair-approximation the pair_action lookup table has to be initialized from a file first, same for ion-snapshot class
	if( (input_params.system_type == 14) || (input_params.system_type == 13) )
    {
        // Initialize the lookup table
        input_params.init_lookup_table();
    }

	// In the case of the two-component system, one has to initialize the lookup table for the fermionic species as well
	// as the number of particles of the bolzmannonic second species
	if( (input_params.system_type == 21) || (input_params.system_type == 22) )
    {


		if(input_params.system_type == 22) input_params.init_lookup_table();



		input_params.N_spec_1 = input_params.N_up + input_params.N_down;


	    double total_charge = (double) input_params.N_spec_1 * (double) input_params.charge_spec_1 + (double)input_params.N_spec_2 * (double) input_params.charge_spec_2;

	    // Update the total number of particles
	    input_params.N_tot = input_params.N_spec_1 + input_params.N_spec_2;

	    if(total_charge != 0.0)
        {
	        std::cout << "Error! Specified system is not charge neutral but has a total charge of " << total_charge << "\n";
	        std::cout << "EWALD sum requires charge-neutrality. Aborting simulation \n";
	        exit(2);
        }


    }

	// Print some simulation parameters to the screen:
	input_params.print_to_screen();
	std::cout << "N_up: " << input_params.N_up << "\tN_down: " << input_params.N_down << "\n";
    if( (input_params.system_type == 15) || (input_params.system_type == 21) || (input_params.system_type == 22) )
    {
        std::cout << "N_Boltzmann: " << input_params.N_spec_2 << "\tN_tot: " << input_params.N_tot << std::endl;
    }


	// Write some of the used parameters to the disk
	std::fstream info;
	info.open("info.info",std::ios::out);

	info << input_params.length << "\n";
	info << input_params.beta << "\n";
	info << input_params.dim << "\n";
	info << input_params.lambda << "\n";
	info << input_params.n_bead << "\n";
	info << input_params.mu << "\n";
	info << input_params.N << "\n";

	info.close();



	// Set seed of the random number engine and print information to the terminal
	std::cout << "Seed: " << input_params.seed << "\n";
	mt.seed( input_params.seed );
	std::cout << "rdm: " << rdm() << "\n";



	// #################################################################################################################################


	
	omp_set_num_threads(input_params.omp_threads);
	
	
	
	// Do the Monte Carlo simulation:
	pelican(  my_plot, user_probs, input_params );





	return 1;



}
