#pragma once


// ###############################################################################################
// ############## Perturbed ideal Fermi gas ######################################################
// ###############################################################################################

class perturbed_ideal_Fermi
{
public:

    // Return the "artificial" repulsive pair potential for the Bogoliubov inequality
    double Repulsion(Bead* a, Bead* b){ return 0.0; };
    void Repulsive_force(Bead* a, Bead* b, std::vector<double>* force) { };

    // Set pointer to the set of system parameters (beta, epsilon, etc.)
    void init( Parameters* new_p );

    // Returns the value of the external potential on bead a
    double ext_pot(Bead* a);

    // Returns value of Coulomb interaction between beads a and b
    double pair_interaction(Bead* a, Bead* b);

    // Returns distance between beads a and b
    double distance(Bead* a, Bead* b);

    // Returns distance_sq between beads a and b
    double distance_sq(Bead* a, Bead *b);

    // Writes the force of bead b on bead a into vector force
    void pair_force_on_a(Bead* a, Bead* b, std::vector<double>* force);

    // Writes the ext. force on bead a into vector force
    void ext_force_on_a(Bead* a, std::vector<double>* force);

    // Returns the free particle density matrix between Beads a and b
    double rho(Bead* a, Bead* b, double scale);

    // Return an adjusted coordinate, e.g. for PBC. -> identity for HO system
    double adjust_coordinate(double coord);

    // Return the spatial difference between two positions within the simulation cell
    double get_diff(double a, double b);
	
	// Return the pair-action compotent of the total action. Zero in this case
	std::vector<double> pair_action( Bead* A_0, Bead* A_1, Bead* B_0, Bead* B_1 ){ std::vector result{0.0,0.0,0.0,0.0}; return result; }
	
	// Return the "pair-action" of two beads of the same particle with the external potential
	std::vector<double> ext_pot_action( Bead* A_0, Bead* A_1 ){ std::vector result{0.0,0.0}; return result; }



private:

    // Pointer to the Parameters
    Parameters* p;


};








void perturbed_ideal_Fermi::init( Parameters* new_p )
{
    p = new_p;
}



// Handle the PBC
double perturbed_ideal_Fermi::adjust_coordinate(double coord)
{
    while( coord < 0.0 )
    {
        coord += (*p).length;
    }
    while( coord > (*p).length )
    {
        coord -= (*p).length;
    }
    return coord;
}


double perturbed_ideal_Fermi::rho(Bead* a, Bead* b, double scale)
{


    double delta_tau = (*p).epsilon * scale; // Obtain the imaginary time step between beads a and b

    double lambda_sq =  2.0 * pi * delta_tau; // thermal wavelength squared from timestep delta_tau;

// 	double norm = pow( sqrt(lambda_sq), (*p).dim ); // Calculate the normalization of rho(...)
    double norm = 1.0;

    double ans = 0.0; // init the sum over all images

    Bead image_bead;

    // Loops over 2*n_boxes+1 images in each dimension
    for(int iX=-(*p).n_boxes;iX<(*p).n_boxes+1;iX++)
    {
        for(int iY=-(*p).n_boxes;iY<(*p).n_boxes+1;iY++)
        {
            for(int iZ=-(*p).n_boxes;iZ<(*p).n_boxes+1;iZ++)
            {

                double my_x = (*b).get_coord(0) + iX*(*p).length;
                double my_y = (*b).get_coord(1) + iY*(*p).length;
                double my_z = (*b).get_coord(2) + iZ*(*p).length;

                double delta_x = (*a).get_coord(0) - my_x;
                double delta_y = (*a).get_coord(1) - my_y;
                double delta_z = (*a).get_coord(2) - my_z;

                double delta_r_sq = delta_x*delta_x + delta_y*delta_y + delta_z*delta_z;

                ans += exp( -pi * delta_r_sq / lambda_sq );
            }
        }
    }

    return ans / norm;

}











// External potential is solely due to perturbation
double perturbed_ideal_Fermi::ext_pot(Bead* a)
{
    double x_a = (*a).get_coord( 0 );
    double y_a = (*a).get_coord( 1 );
    double z_a = (*a).get_coord( 2 );

    double cos_arg = ( 2.0*pi/(*p).length * ( (*p).pq_x*x_a + (*p).pq_y*y_a + (*p).pq_z*z_a ) );
    double cos_arg2 = ( 2.0*pi/p->length * ( p->pq_x2*x_a + p->pq_y2*y_a + p->pq_z2*z_a ) );
    double cos_arg3 = ( 2.0*pi/p->length * ( p->pq_x3*x_a + p->pq_y3*y_a + p->pq_z3*z_a ) );


    return 2.0*( cos( cos_arg )*(*p).vq + cos( cos_arg2 )*p->vq2 + cos( cos_arg3 )*p->vq3 );
}


// Ext force due to the perturbation
void perturbed_ideal_Fermi::ext_force_on_a(Bead* a, std::vector<double>* force)
{

    double x_a = (*a).get_coord( 0 );
    double y_a = (*a).get_coord( 1 );
    double z_a = (*a).get_coord( 2 );

    double pre_factor = 2.0*(*p).vq;
    double tpl = 2.0*pi/(*p).length;

    double pre_factor2 = 2.0*p->vq2;
    double pre_factor3 = 2.0*p->vq3;



    (*force)[0] = pre_factor * tpl * (*p).pq_x * sin( tpl * (*p).pq_x * x_a );
    (*force)[1] = pre_factor * tpl * (*p).pq_y * sin( tpl * (*p).pq_y * y_a );
    (*force)[2] = pre_factor * tpl * (*p).pq_z * sin( tpl * (*p).pq_z * z_a );

    (*force)[0] += pre_factor2 * tpl * p->pq_x2 * sin( tpl * p->pq_x2 * x_a );
    (*force)[1] += pre_factor2 * tpl * p->pq_y2 * sin( tpl * p->pq_y2 * y_a );
    (*force)[2] += pre_factor2 * tpl * p->pq_z2 * sin( tpl * p->pq_z2 * z_a );

    (*force)[0] += pre_factor3 * tpl * p->pq_x3 * sin( tpl * p->pq_x3 * x_a );
    (*force)[1] += pre_factor3 * tpl * p->pq_y3 * sin( tpl * p->pq_y3 * y_a );
    (*force)[2] += pre_factor3 * tpl * p->pq_z3 * sin( tpl * p->pq_z3 * z_a );

}







// We measure the direct distance, not the closest to the next image.
double perturbed_ideal_Fermi::get_diff(double a, double b)
{
    double tmp = a - b;

    return tmp;
}




double perturbed_ideal_Fermi::distance_sq(Bead* a, Bead* b)
{
    double ans = 0.0;
    for(int i=0;i<(*p).dim;i++)
    {
// 		double tmp = get_diff( (*a).get_coord(i), (*b).get_coord(i) );
        double tmp = (*a).get_coord(i) - (*b).get_coord(i);
        while( tmp > (*p).length*0.5 )
        {
            tmp = tmp - (*p).length;
        }
        while( tmp < -(*p).length*0.5 )
        {
            tmp = tmp + (*p).length;
        }


        ans += tmp * tmp;
    }
    return ans;
}



double perturbed_ideal_Fermi::distance(Bead* a, Bead* b)
{
    return sqrt( distance_sq(a, b) );
}









// No pair force

void perturbed_ideal_Fermi::pair_force_on_a(Bead* a, Bead* b, std::vector<double>* force)
{

    (*force)[0] = 0.0;
    (*force)[1] = 0.0;
    (*force)[2] = 0.0;
}



// No pair interaction
double perturbed_ideal_Fermi::pair_interaction(Bead* a, Bead* b)
{
    return 0.0;
}

































