#pragma once

#include <memory>

#include "../particles/pair_action_lookup_table.hpp"
// ###################################################################################################
// ############# PIMC for electrons with an ion-background potential in BO approximation #############
// ###################################################################################################


// TBD: Implement forces, for PB-PIMC and some such
class Kelbg_two_component_action
{

public:

    // Return the "artificial" repulsive pair potential for the Bogoliubov inequality
    double Repulsion(Bead* a, Bead* b){ return 0.0; };
    void Repulsive_force(Bead* a, Bead* b, std::vector<double>* force) { };

    // Set pointer to the set of system parameters (beta, epsilon, etc.)
    void init( Parameters* new_p );

    // Returns the value of the external potential on bead a
    double ext_pot(Bead* a);

    // Returns value of Coulomb interaction between beads a and b
    double pair_interaction(Bead* a, Bead* b);

    // Returns distance between beads a and b
    double distance(Bead* a, Bead* b);

    // Returns distance_sq between beads a and b
    double distance_sq(Bead* a, Bead *b);

    // Writes the force of bead b on bead a into vector force
    void pair_force_on_a(Bead* a, Bead* b, std::vector<double>* force);

    // Writes the ext. force on bead a into vector force
    void ext_force_on_a(Bead* a, std::vector<double>* force);

    // Returns the free particle density matrix between Beads a and b
    double rho(Bead* a, Bead* b, double scale);

    // Return an adjusted coordinate, e.g. for PBC. -> identity for HO system
    double adjust_coordinate(double coord);

    // Return the spatial difference between two positions within the simulation cell
    double get_diff(double a, double b);



	// Special Kelbg potential function
	double Kelbg(double r, double lambda);
	

	
	

    // Calculate the Madelung constant
    double get_madelung_constant(double tol, int* max_it, double kappa, int max_index);





    // Calculate the interaction energy between two beads to find optimum kappa value
    double Ewald_pair(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index);

    // modified Ewald pair interaction with a specified r_index:
    double Ewald_pair_index(Bead* a, Bead* b, int cnt);

    // Calculate the Ewald force between two beads to find the optimum kappa value
    double Ewald_force(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index);
    double Ewald_force_cnt(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index);
	
	
	
	
	double PBC_distance( double x );
	



    // Dummy Methods in order to get the pair-action toolbox requirements satisfied
    std::vector<double> pair_action(Bead* a0, Bead* a1, Bead* b0, Bead* b1);
	
	// Return the "pair-action" of two beads of the same particle with the external potential
	std::vector<double> ext_pot_action( Bead* A_0, Bead* A_1 ){ std::vector result{0.0,0.0}; return result; }

private:

    std::shared_ptr<Parameters> p;

};










/*

double Kelbg_two_component_action::Kelbg_derivative( Bead* a, Bead* b )
{
	double ans = 0.0;
	auto a_coords = a->get_all_coords();
	auto b_coords = b->get_all_coords();
	
	
	double length = p->length;
	

		
	std::vector<double> diff_vec = {a_coords[0] - b_coords[0], a_coords[1] - b_coords[1], a_coords[2] - b_coords[2]};
	
	double delta_r_sq = 0.0;
	
	// Compute the nearest image pair between bead and ion
	for (int dim = 0; dim < p->dim; dim++)
	{
		if (diff_vec[dim] > 0.5 * length)
		{
			diff_vec[dim] -= length;
		}

		if (diff_vec[dim] < -0.5 * length)
		{
			diff_vec[dim] += length;
		}
            
		delta_r_sq += diff_vec[dim]*diff_vec[dim];
	}
        
	// Compute the thermal wave length parameter of the Kelbg potential: 
	double Kelbg_lambda = sqrt(0.5*p->epsilon*( 1.0/a->get_mass() + 1.0/b->get_mass() ));
		
	double x = sqrt(delta_r_sq)/Kelbg_lambda;
		
	ans += ( erf(x)-1.0 )/Kelbg_lambda;
		

	
	return  sqrt(M_PI) * 0.50 * a->get_charge() * b->get_charge() * ans / p->beta; 
}
*/




void Kelbg_two_component_action::init(Parameters* new_p)
{
    p = std::make_shared<Parameters>(*new_p);
}



double Kelbg_two_component_action::Kelbg(double r, double lambda)
{
	double x = r/lambda;
	return ( 1.0 - exp(-x*x) + sqrt(M_PI)*x*( 1.0 - erf(x) ) ) / r;
}




double Kelbg_two_component_action::PBC_distance( double x )
{
	if( x < -0.5*p->length ) return x+p->length;
	if( x > 0.5*p->length ) return x-p->length;
	return x;
}










double Kelbg_two_component_action::ext_pot(Bead* a)
{
    /**
     * This method calculated the ewald sum for fixed ion positions, which are contained in the ion-container.
     */
/*
    double ans = 0.0;
    double last_ans = 10000.0;

    int cnt = 0;
    int max_index = 20;
    double length = p->length;
    double inv_length = 1.0 / p->length;
    double tol = 1e-10;
    double G_fac = 2.0 * M_PI / p->length;
    double kappa = p->kappa_int;

	

    int n_ions = p->ions.get_num_ions();
    auto bead_coords = a->get_all_coords();
*/


    double Madelung_contribution = 0.50*(*p).madelung;
    


    return  Madelung_contribution;
}







double Kelbg_two_component_action::pair_interaction(Bead* a, Bead* b)
{
    double G_fac = 2.0*pi/p->length;

    double ans_r = 0.0;
    double ans_k = 0.0;

    double kappa = p->kappa_int;

    double dx = a->get_coord(0) - b->get_coord(0);
    double dy = a->get_coord(1) - b->get_coord(1);
    double dz = a->get_coord(2) - b->get_coord(2);

	// Compute the thermal wave length parameter of the Kelbg potential: 
	double Kelbg_lambda = sqrt(0.5*p->epsilon*(1.0/a->get_mass() + 1.0/b->get_mass()));

	
	
	
	
    // Pre-compute the cosine-factors for the reciprocal part:
    std::vector<double> GX,GY,GZ;
    for(int iK=0;iK<p->k_index+1;iK++)
    {
        GX.push_back( cos(iK*G_fac*dx) );
        GY.push_back( cos(iK*G_fac*dy) );
        GZ.push_back( cos(iK*G_fac*dz) );
    }


    double ans_k_new = 0.0;
    // Evaluate the sum in reciprocal space:
    for(int iX=-p->k_index;iX<p->k_index+1;iX++)
    {
        int x_index = abs( iX );
        for(int iY=-p->k_index;iY<p->k_index+1;iY++)
        {
            int y_index = abs( iY );
            for(int iZ=-p->k_index;iZ<p->k_index+1;iZ++)
            {
                int z_index = abs( iZ );

//  				double G_sq = ( iX*iX + iY*iY + iZ*iZ ) / ( p->length * p->length );

                double cos_term = GX[ x_index ]*GY[ y_index ]*GZ[ z_index ];
// 				double my_exp = exp( -pi*pi*G_sq/(kappa*kappa) );
                double my_exp = p->get_Ewald_exp(iX, iY, iZ);

                if( ( iX != 0 ) || ( iY != 0 ) || ( iZ != 0 ) )
                {
                    ans_k_new +=  cos_term * my_exp / pi;
                }

            }
        }
    }

    int max_r = 1+int( p->r_index/4 );
    double cutoff = p->r_index * 0.250 * p->length;

    // Evaluate the sum in real space:
    for(int iX=-max_r;iX<max_r+1;iX++)
    {
        double x = dx + p->length*iX;
        for(int iY=-max_r;iY<max_r+1;iY++)
        {
            double y = dy + p->length*iY;
            for(int iZ=-max_r;iZ<max_r+1;iZ++)
            {
                double z = dz + p->length*iZ;
				
				
				double r_abs = sqrt( x*x + y*y + z*z );
				
				if( (fabs(x) < 0.5*p->length) && (fabs(y) < 0.5*p->length) && (fabs(z) < 0.5*p->length)) // For the nearest image, use the Kelb interaction potential to avoid path collapse
				{
					// erfc() = 1 - erf(), 1->Kelbg
// 					if( r_abs <= cutoff ) ans_r +=  ( Kelbg(r_abs,Kelbg_lambda) - erf( r_abs*kappa ) / r_abs ); 
					
					
					if( a->get_charge() * b->get_charge() < 0 )
					{
						if( r_abs <= cutoff ) ans_r +=  ( - erf( r_abs*kappa ) / r_abs );
					}
					else
					{
						if( r_abs <= cutoff ) ans_r += erfc( r_abs*kappa ) / r_abs;
					}
				}
				else // Otherwise, use the usual Ewald expression
				{
					if( r_abs <= cutoff ) ans_r += erfc( r_abs*kappa ) / r_abs;
					
				}
				
					
					
            }
        }
    }

    // 		double ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;
    double ans = ans_k_new/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;


    return ans * a->get_charge() * b->get_charge();
	
}




double Kelbg_two_component_action::distance(Bead* a, Bead* b)
{
    return sqrt( distance_sq(a, b) );
}



double Kelbg_two_component_action::distance_sq(Bead* a, Bead* b)
{
    double ans = 0.0;
    for(int i=0;i<(*p).dim;i++)
    {
        double tmp = (*a).get_coord(i) - (*b).get_coord(i);
        while( tmp > (*p).length*0.5 )
        {
            tmp = tmp - (*p).length;
        }
        while( tmp < -(*p).length*0.5 )
        {
            tmp = tmp + (*p).length;
        }


        ans += tmp * tmp;
    }
    return ans;
}





double Kelbg_two_component_action::rho(Bead* a, Bead* b, double scale)
{


    double delta_tau = (*p).epsilon * scale; // Obtain the imaginary time step between beads a and b

    double lambda_sq =  2.0 * pi * delta_tau / a->get_mass(); // thermal wavelength squared from timestep delta_tau;

    double norm = pow( sqrt(lambda_sq), (*p).dim ); // Calculate the normalization of rho(...)
//	double norm = 1.0;

    double ans = 0.0; // init the sum over all images

    Bead image_bead;

    // Loops over 2*n_boxes+1 images in each dimension
    for(int iX=-(*p).n_boxes;iX<(*p).n_boxes+1;iX++)
    {
        for(int iY=-(*p).n_boxes;iY<(*p).n_boxes+1;iY++)
        {
            for(int iZ=-(*p).n_boxes;iZ<(*p).n_boxes+1;iZ++)
            {

                double my_x = (*b).get_coord(0) + iX*(*p).length;
                double my_y = (*b).get_coord(1) + iY*(*p).length;
                double my_z = (*b).get_coord(2) + iZ*(*p).length;

                double delta_x = (*a).get_coord(0) - my_x;
                double delta_y = (*a).get_coord(1) - my_y;
                double delta_z = (*a).get_coord(2) - my_z;

                double delta_r_sq = delta_x*delta_x + delta_y*delta_y + delta_z*delta_z;

                ans += exp( -pi * delta_r_sq / lambda_sq );
            }
        }
    }

    return ans / norm;

}

double Kelbg_two_component_action::adjust_coordinate(double coord)
{
    while( coord < 0.0 )
    {
        coord += (*p).length;
    }
    while( coord > (*p).length )
    {
        coord -= (*p).length;
    }
    return coord;
}


double Kelbg_two_component_action::get_diff(double a, double b)
{
    double tmp = a - b;

    return tmp;
}






double Kelbg_two_component_action::get_madelung_constant(double tol, int* max_it, double kappa, int max_index)
{
    double ans = 0.0;
    double last_ans = 1000.0;

    int cnt = 0; // number of iterations

    double inverse_length = 1.0 / (*p).length;
    double kappa_sq = kappa*kappa;

    while( fabs( (ans-last_ans)/last_ans ) > tol ) // increase number of boxes in real space until changes become smaller than tol
    {
        cnt++;
        last_ans = ans;
        ans = 0.0;

        if( cnt == max_index )
        {
            (*max_it) = max_index;
            return last_ans;
        }

        double ans_r = 0.0;
        double ans_k = 0.0;

        for(int nx=-cnt;nx<cnt+1;nx++)
        {
            double G_x = nx * inverse_length;
            double G_x_sq = G_x * G_x;
            double R_x = nx * (*p).length;
            double R_x_sq = R_x * R_x;
            for(int ny=-cnt;ny<cnt+1;ny++)
            {
                double G_y = ny * inverse_length;
                double G_y_sq = G_y * G_y;
                double R_y = ny * (*p).length;
                double R_y_sq = R_y * R_y;
                for(int nz=-cnt;nz<cnt+1;nz++)
                {
                    double G_z = nz * inverse_length;
                    double G_z_sq = G_z * G_z;
                    double R_z = nz * (*p).length;
                    double R_z_sq = R_z * R_z;


                    // Do not include the zero G-vector
                    if( !( ( nx == 0 ) && ( ny == 0 ) && ( nz == 0 ) ) )
                    {

                        double G_sq = G_x_sq + G_y_sq + G_z_sq;
                        double r_abs = sqrt( R_x_sq + R_y_sq + R_z_sq );

                        ans_k += exp( -pi*pi*G_sq/kappa_sq ) / (pi * G_sq);
                        ans_r += erfc( kappa * r_abs ) / r_abs;

                    } // end zero vector cond.


                } // end loop nz
            }
        } // end loop nx

// 		std::cout << "k_cnt: " << cnt << "\t ans: " << ans << "\n";

        ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r - 2.0*kappa/sqrt(pi);




    } // end of convergence condition


    // Set the maximum index number
    (*max_it) = cnt-1;

    // Return reciprocal part of Madelung const:
    return ans;

}




double Kelbg_two_component_action::Ewald_pair(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index)
{
	std::cout << "Warning. Not adjusted for Kelbg hydrogen. This is not for production Monte Carlo steps!!!\n";
	
	
    double ans = 0.0;
    double last_ans = 1000.0;

    int cnt = 0; // number of iterations

    double inverse_length = 1.0 / (*p).length;
    double kappa_sq = kappa*kappa;

    while( fabs( (ans-last_ans)/last_ans ) > tol ) // increase number of boxes in real space until changes become smaller than tol
    {
        cnt++;
        last_ans = ans;
        ans = 0.0;

        if( cnt == max_index )
        {
            (*max_it) = max_index;
            return last_ans;
        }

        double ans_r = 0.0;
        double ans_k = 0.0;

        for(int nx=-cnt;nx<cnt+1;nx++)
        {
            double G_x = nx * inverse_length;
            double G_x_sq = G_x * G_x;
            double R_x = nx * (*p).length;

            for(int ny=-cnt;ny<cnt+1;ny++)
            {
                double G_y = ny * inverse_length;
                double G_y_sq = G_y * G_y;
                double R_y = ny * (*p).length;

                for(int nz=-cnt;nz<cnt+1;nz++)
                {
                    double G_z = nz * inverse_length;
                    double G_z_sq = G_z * G_z;
                    double R_z = nz * (*p).length;

                    double x_diff = get_diff( (*a).get_coord(0), (*b).get_coord(0) );
                    double y_diff = get_diff( (*a).get_coord(1), (*b).get_coord(1) );
                    double z_diff = get_diff( (*a).get_coord(2), (*b).get_coord(2) );

                    // Do not include the zero G-vector
                    if( !( (nx == 0) && (ny == 0) && (nz == 0) ) )
                    {
                        double G_sq = G_x_sq + G_y_sq + G_z_sq;

                        double x = G_x * x_diff;
                        double y = G_y * y_diff;
                        double z = G_z * z_diff;

                        double cos_arg = 2.0*pi*( x + y + z );

                        ans_k += ( cos(cos_arg) * exp( -pi*pi*G_sq/kappa_sq ) ) / (pi * G_sq);

                    } // end zero vector cond.


                    double x = R_x + x_diff;
                    double y = R_y + y_diff;
                    double z = R_z + z_diff;

                    double my_abs = sqrt( x*x + y*y + z*z );


                    ans_r += erfc( kappa * my_abs ) / my_abs;



                } // end loop nz
            }
        } // end loop nx

        ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;


    } // end of convergence condition


    // Set the maximum index number
    (*max_it) = cnt - 1;

    // Return the Ewald pair interaction
    return ans;


}





double Kelbg_two_component_action::Ewald_pair_index(Bead* a, Bead* b, int cnt)
{
		std::cout << "Warning2. Not adjusted for Kelbg hydrogen. This is not for production Monte Carlo steps!!!\n";
	
	
	
    double ans;

    double ans_r = 0.0;
    double ans_k = 0.0;

// 	int cnt = (*p).r_index;
    double inverse_length = 1.0 / (*p).length;
    double kappa = (*p).kappa_int;
    double kappa_sq = kappa*kappa;

    for(int nx=-cnt;nx<cnt+1;nx++)
    {
        double G_x = nx * inverse_length;
        double G_x_sq = G_x * G_x;
        double R_x = nx * (*p).length;

        for(int ny=-cnt;ny<cnt+1;ny++)
        {
            double G_y = ny * inverse_length;
            double G_y_sq = G_y * G_y;
            double R_y = ny * (*p).length;

            for(int nz=-cnt;nz<cnt+1;nz++)
            {
                double G_z = nz * inverse_length;
                double G_z_sq = G_z * G_z;
                double R_z = nz * (*p).length;

                double x_diff = get_diff( (*a).get_coord(0), (*b).get_coord(0) );
                double y_diff = get_diff( (*a).get_coord(1), (*b).get_coord(1) );
                double z_diff = get_diff( (*a).get_coord(2), (*b).get_coord(2) );


                // Do not include the zero G-vector
                if( !( (nx == 0) && (ny == 0) && (nz == 0) ) )
                {
                    double G_sq = G_x_sq + G_y_sq + G_z_sq;

                    double x = G_x * x_diff;
                    double y = G_y * y_diff;
                    double z = G_z * z_diff;

                    double cos_arg = 2.0*pi*( x + y + z );

// 					double my_exp = (*p).get_Ewald_exp(nx, ny, nz);
// 					ans_k += ( cos(cos_arg) * my_exp ) / (pi * G_sq);
                    ans_k += ( cos(cos_arg) * exp( -pi*pi*G_sq/kappa_sq ) ) / (pi * G_sq);

                } // end zero vector cond.


                double x = R_x + x_diff;
                double y = R_y + y_diff;
                double z = R_z + z_diff;

                double my_abs = sqrt( x*x + y*y + z*z );


                ans_r += erfc( kappa * my_abs ) / my_abs;




            } // end loop nz
        }
    } // end loop nx

    ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;

    return ans;

}





double Kelbg_two_component_action::Ewald_force(Bead *a, Bead *b, double tol, int *max_it, double kappa, int max_index)
{
    return 0;
}

double Kelbg_two_component_action::Ewald_force_cnt(Bead *a, Bead *b, double tol, int *max_it, double kappa, int max_index)
{
    return 0;
}

void Kelbg_two_component_action::ext_force_on_a(Bead *a, std::vector<double> *force)
{

}

void Kelbg_two_component_action::pair_force_on_a(Bead *a, Bead *b, std::vector<double> *force)
{
	    double inverse_length = 1.0 / (*p).length;
    double g_fac = 2.0 / (*p).volume;

    double kappa = (*p).kappa_int;
    double kappa_sq = kappa * kappa;

    int cnt = (*p).r_index;

    (*force)[0] = 0.0;
    (*force)[1] = 0.0;
    (*force)[2] = 0.0;

    double r_x = (*a).get_coord(0) - (*b).get_coord(0);
    double r_y = (*a).get_coord(1) - (*b).get_coord(1);
    double r_z = (*a).get_coord(2) - (*b).get_coord(2);

    // The first loop is for the k-space:

    for(int nx=-cnt;nx<1+cnt;nx++)
    {
        double G_x = nx * inverse_length;
        double G_x_sq = G_x * G_x;

        for(int ny=-cnt;ny<1+cnt;ny++)
        {
            double G_y = ny * inverse_length;
            double G_y_sq = G_y * G_y;


            for(int nz=-cnt;nz<1+cnt;nz++)
            {
                double G_z = nz * inverse_length;
                double G_z_sq = G_z * G_z;


                double G_sq = G_x_sq + G_y_sq + G_z_sq;

                double product = r_x * G_x + r_y * G_y + r_z * G_z;

                double my_exp = (*p).get_Ewald_exp(nx, ny, nz);
                double tmp = g_fac * sin( product * 2.0*pi ) * my_exp;// / G_sq;
// 				double tmp = g_fac * sin( product * 2.0*pi ) * exp( - pi*pi * G_sq / kappa_sq ) / G_sq;

                if( ! ( ( nx == 0) && ( ny == 0 ) && ( nz == 0 ) ) ) // Do not include zero g vector
                {
                    (*force)[0] += tmp * G_x;
                    (*force)[1] += tmp * G_y;
                    (*force)[2] += tmp * G_z;
                }



            } // end nz
        } // end ny
    } // end nx;



    // The second loops are for the real-space part:
    cnt = 1+int( p->r_index/4 );
    double cutoff = p->r_index * 0.250 * p->length;

    for(int nx=-cnt;nx<1+cnt;nx++)
    {
        double R_x = nx * (*p).length;

        for(int ny=-cnt;ny<1+cnt;ny++)
        {
            double R_y = ny * (*p).length;

            for(int nz=-cnt;nz<1+cnt;nz++)
            {
                double R_z = nz * (*p).length;


                double r_x_priam = r_x + R_x;
                double r_y_priam = r_y + R_y;
                double r_z_priam = r_z + R_z;

                double R_priam_sq = r_x_priam*r_x_priam + r_y_priam*r_y_priam + r_z_priam*r_z_priam;
                double R_priam = sqrt( R_priam_sq );

                if( R_priam <= cutoff )
                {

                    double erfc_part = erfc( kappa * R_priam );
                    double exp_part = 2.0 * kappa * R_priam * exp( - kappa_sq * R_priam_sq ) / sqrt( pi );

                    double tmp2 = ( erfc_part + exp_part ) / ( R_priam_sq * R_priam );

                    (*force)[0] += r_x_priam * tmp2;
                    (*force)[1] += r_y_priam * tmp2;
                    (*force)[2] += r_z_priam * tmp2;
                }


            } // end nz
        } // end ny
    } // end nx;

	(*force)[0] = (*force)[0]*a->get_charge()*b->get_charge();
	(*force)[1] = (*force)[1]*a->get_charge()*b->get_charge();
	(*force)[2] = (*force)[2]*a->get_charge()*b->get_charge();

}









// TBD TBD TBD Open question: the derivative should actually be subtracted from the rest, no?
// Handles both the action and its derivative!
std::vector<double> Kelbg_two_component_action::pair_action(Bead *a0, Bead *a1, Bead *b0, Bead *b1)
{
    std::vector<double> result;
	if( a0->get_charge()*b0->get_charge() > 0 )
	{
		result.push_back(0.0);
		result.push_back(0.0);
		result.push_back(0.0);
		result.push_back(0.0);
		return result;
	}
	
	double Kelbg_lambda = sqrt(0.5*p->epsilon*(1.0/a0->get_mass() + 1.0/b0->get_mass()));
	double Mu = 1.0 / ( 1.0/a0->get_mass() + 1.0/b0->get_mass() );
	
	double ans0 = 0.0;
	double ans1 = 0.0;
	
	for(int iDim=0;iDim<p->dim;iDim++)
	{
		double diff0 = PBC_distance( a0->get_coord(iDim) - b0->get_coord(iDim) );
		double diff1 = PBC_distance( a1->get_coord(iDim) - b1->get_coord(iDim) );
		
		
		ans0 += diff0*diff0;
		ans1 += diff1*diff1;
	}
	
	double r0 = sqrt(ans0);
	double r1 = sqrt(ans1);
	
	double pot0 = Kelbg( r0, Kelbg_lambda );
	double pot1 = Kelbg( r1, Kelbg_lambda );
	
	double action = 0.5 * ( pot0 + pot1 ) * a0->get_charge() * b0->get_charge();
	result.push_back( action );
	
	
	
	
	
	
	// Compute derivative of Kelbg action with respect to the inverse temperature
	
	double x0 = r0/Kelbg_lambda;
	double x1 = r1/Kelbg_lambda;
		
	
	double der0 = ( erf(x0)-1.0 )/Kelbg_lambda;
	double der1 = ( erf(x1)-1.0 )/Kelbg_lambda;

	double derivative = - 1.0/double(p->n_bead) * action    -    1.0/double(p->n_bead) * 0.5 * ( der0 + der1 ) * a0->get_charge() * b0->get_charge() * sqrt(M_PI) * 0.5;
	
	
	result.push_back( derivative );
	
	
	
	
	
	
	// Compute the derivative of Kelbg action with respect to electron and proton mass
	
	
	double curly_bracket0 = 0.5*exp(-x0) + sqrt(pi)*0.50 * (1.0-erf(x0)) - exp(-x0*x0);
	double curly_bracket1 = 0.5*exp(-x1) + sqrt(pi)*0.50 * (1.0-erf(x1)) - exp(-x1*x1);
	
	
	double m_proton = a0->get_mass();
	if( m_proton < 1.1 ) m_proton = b0->get_mass();
	
	double m_electron = 1.0;
	
	double m_e_derivative = - p->epsilon * a0->get_charge() * b0->get_charge() / Kelbg_lambda * Mu / ( m_electron*m_electron ) * 0.5 * ( curly_bracket0 + curly_bracket1 );
	double m_p_derivative = - p->epsilon * a0->get_charge() * b0->get_charge() / Kelbg_lambda * Mu / ( m_proton*m_proton ) * 0.5 * ( curly_bracket0 + curly_bracket1 );
	
	
	

	result.push_back( m_e_derivative );
	result.push_back( m_p_derivative );
	
	
	
	
	
    return result;
}
























