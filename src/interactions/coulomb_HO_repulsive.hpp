#pragma once


// ###########################################################################################################################################
// ##### harmonic confinement with Coulomb interaction -> electrons in a quantum dot ############# BUT NOW WITH REPULSION AND BOGOLIUBOV :) ##
// ###########################################################################################################################################

class coulomb_HO_repulsive
{
public:
    // Return the "artificial" repulsive pair potential for the Bogoliubov inequality
    double Repulsion(Bead* a, Bead* b);

    void Repulsive_force(Bead* a, Bead* b, std::vector<double>* force);


    // Set pointer to the set of system parameters (beta, epsilon, etc.)
    void init( Parameters* new_p );

    // Returns the value of the external potential on bead a
    double ext_pot(Bead* a);

    // Returns value of Coulomb interaction between beads a and b
    double pair_interaction(Bead* a, Bead* b);

    // Returns distance between beads a and b
    double distance(Bead* a, Bead* b);

    // Returns distance_sq between beads a and b
    double distance_sq(Bead* a, Bead *b);

    // Writes the force of bead b on bead a into vector force
    void pair_force_on_a(Bead* a, Bead* b, std::vector<double>* force);

    // Writes the ext. force on bead a into vector force
    void ext_force_on_a(Bead* a, std::vector<double>* force);

    // Returns the free particle density matrix between Beads a and b
    double rho(Bead* a, Bead* b, double scale);

    // Return an adjusted coordinate, e.g. for PBC. -> identity for HO system
    double adjust_coordinate(double coord);
	
	// Return the pair-action compotent of the total action. Zero in this case
	std::vector<double> pair_action( Bead* A_0, Bead* A_1, Bead* B_0, Bead* B_1 ){ std::vector result{0.0,0.0,0.0,0.0}; return result; }
	
	// Return the "pair-action" of two beads of the same particle with the external potential
	std::vector<double> ext_pot_action( Bead* A_0, Bead* A_1 ){ std::vector result{0.0,0.0}; return result; }


private:


    // Pointer to the Parameters
    Parameters* p;


};

void coulomb_HO_repulsive::init( Parameters* new_p )
{
    p = new_p;

}




double coulomb_HO_repulsive::adjust_coordinate(double coord)
{
    return coord;
}



double coulomb_HO_repulsive::rho(Bead* a, Bead* b, double scale)
{
    double delta_r_sq = distance_sq(a, b); // Obtain the spatial distance squared between a and b

    double delta_tau = (*p).epsilon * scale; // Obtain the imaginary time step between beads a and b

    double lambda_sq =  2.0 * pi * delta_tau; // thermal wavelength squared from timestep delta_tau;

    double norm = pow( sqrt(lambda_sq), (*p).dim ); // Calculate the normalization of rho(...)

    return exp( -pi * delta_r_sq / lambda_sq ) / norm;
}



double coulomb_HO_repulsive::ext_pot(Bead* a)
{
    double ans = 0.0;
    for(int i=0;i<(*p).dim;i++)
    {
        ans += (*a).get_coord(i) * (*a).get_coord(i);
    }
    return 0.5*ans;
}


void coulomb_HO_repulsive::ext_force_on_a(Bead* a, std::vector<double>* force)
{
    for(int i=0;i<(*p).dim;i++)
    {
        (*force)[i] = -1.0*(*a).get_coord(i);
    }
}


void coulomb_HO_repulsive::pair_force_on_a(Bead* a, Bead* b, std::vector<double>* force)
{
    double r = distance(a, b);

    std::vector<double> RepForce( p->dim, 0.0 );
    Repulsive_force( a, b, &RepForce );

    for(int i=0;i<(*p).dim;i++)
    {
        double diff = (*a).get_coord(i) - (*b).get_coord(i);
        (*force)[i] = (*p).lambda * diff / (r*r*r);
        (*force)[i] += RepForce[i];
    }
}



void coulomb_HO_repulsive::Repulsive_force(Bead* a, Bead* b, std::vector<double>* force)
{
    double r = distance(a, b);
    for(int i=0;i<p->dim;i++)
    {
        if( p->repulsive_choice == 1 )
        {
            double triple = p->repulsive_scale*p->repulsive_scale*p->repulsive_scale;
            double diff = a->get_coord(i) - b->get_coord(i);
            (*force)[i] = diff * p->repulsive_eta * 3.0 / (r*r*r*r*r*triple);
        }
        else
        {
            (*force)[i] = 0.0;
        }
    }
}





double coulomb_HO_repulsive::distance(Bead* a, Bead* b)
{
    double ans = 0.0;
    for(int i=0;i<(*p).dim;i++)
    {
        double tmp = (*a).get_coord(i) - (*b).get_coord(i);
        ans += tmp * tmp;
    }
    return sqrt(ans);
}



double coulomb_HO_repulsive::distance_sq(Bead* a, Bead* b)
{
    double ans = 0.0;
    for(int i=0;i<(*p).dim;i++)
    {
        double tmp = (*a).get_coord(i) - (*b).get_coord(i);
        ans += tmp * tmp;
    }
    return ans;
}



double coulomb_HO_repulsive::pair_interaction(Bead* a, Bead* b)
{
    if( distance(a, b)<1e-6)
    {
        std::cout << "incr. small distance.\n";
    }
    return (*p).lambda / distance(a, b) + Repulsion(a,b); // return: lambda / r
}


double coulomb_HO_repulsive::Repulsion(Bead* a, Bead* b)
{
    double ans = 0.0;
    double delta_r_sq = distance_sq(a,b);

    if( p->repulsive_choice == 0 )
    {
        double lambda_sq = 2.0*pi*p->beta;
        double lambda = sqrt(lambda_sq);
        double repulsive_factor = - 1.0 / ( 2.0 * lambda_sq * p->repulsive_c * p->repulsive_c );
        ans += exp( repulsive_factor * delta_r_sq ) / ( sqrt(2.0*pi) * p->repulsive_c * lambda );
    }
    else if( p->repulsive_choice == 1 )
    {
        double myR = p->repulsive_scale * sqrt( delta_r_sq );
        ans += 1.0 / pow( myR, p->repulsive_c );
    }
    else
    {
        double Rho_same = rho(a, a, double(p->n_bead)*p->repulsive_c);
        double Rho_diff = rho(a, b, double(p->n_bead)*p->repulsive_c);

        ans += - log( Rho_same*Rho_same - Rho_diff*Rho_diff ) / p->epsilon;
    }



    return p->repulsive_eta * ans;
}



















