#pragma once

#include "../particles/pair_action_lookup_table.hpp"
#include "../stuff.hpp"
#include <memory>

class Hydrogen_PA
{
public:

    // Return the "artificial" repulsive pair potential for the Bogoliubov inequality
    static double Repulsion(Bead* a, Bead* b){ return 0.0; };

    void Repulsive_force(Bead* a, Bead* b, std::vector<double>* force) { };

    // Set pointer to the set of system parameters (beta, epsilon, etc.)
    void init( Parameters* new_p );

    // Returns the value of the external potential on bead a
    double ext_pot(Bead* a);

    // Returns value of Coulomb interaction between beads a and b
    double pair_interaction(Bead* a, Bead* b);

    // Returns distance between beads a and b
    double distance(Bead* a, Bead* b);

    // Returns distance_sq between beads a and b
    double distance_sq(Bead* a, Bead *b);

    // Writes the force of bead b on bead a into vector force
    void pair_force_on_a(Bead* a, Bead* b, std::vector<double>* force) {};

    // Writes the ext. force on bead a into vector force
    void ext_force_on_a(Bead* a, std::vector<double>* force) {};

    // Returns the free particle density matrix between Beads a and b
    double rho(Bead* a, Bead* b, double scale);

    // Return an adjusted coordinate, e.g. for PBC. -> identity for HO system
    double adjust_coordinate(double coord);

    // Return the spatial difference between two positions within the simulation cell
    static double get_diff(double a, double b) ;

    // Calculate the Madelung constant
    double get_madelung_constant(double tol, int* max_it, double kappa, int max_index);

    // Calculate the interaction energy between two beads to find optimum kappa value
    double Ewald_pair(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index);

    // modified Ewald pair interaction with a specified r_index:
    double Ewald_pair_index(Bead* a, Bead* b, int cnt);

    // Calculate the Ewald force between two beads to find the optimum kappa value
    double Ewald_force(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index) { return 0.0;};
    double Ewald_force_cnt(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index) { return 0.0;};
	
	
	// Return the pair-action compotent of the total action. 
	std::vector<double> pair_action( Bead* A_0, Bead* A_1, Bead* B_0, Bead* B_1 );
	
	// Return the "pair-action" of two beads of the same particle with the external potential
	std::vector<double> ext_pot_action( Bead* A_0, Bead* A_1 ){ std::vector result{0.0,0.0}; return result; }


    ~Hydrogen_PA();

private:

    // Pointer to the Parameters
    Parameters* p;
    gsl_integration_workspace* ws;
    std::shared_ptr<pair_action_lookup_table> table_ptr;

};


std::vector<double> Hydrogen_PA::pair_action(Bead *A_0, Bead *A_1, Bead *B_0, Bead *B_1)
{
	
	if( A_0->get_charge() * B_0->get_charge() > 0 ) // We only have a non-zero pair action for e-p case
	{
		std::vector<double> result{ 0.0, 0.0, 0.0, 0.0 };

		return result;
	}
	
	
	
	
	
    auto vec_a0 = A_0->get_all_coords();
    auto vec_a1 = A_1->get_all_coords();
    auto vec_b0 = B_0->get_all_coords();
    auto vec_b1 = B_1->get_all_coords();


//     double a = 0.0;
//     double b = 0.0;


    std::vector<double> vec_diff_0;// =  {vec_a0[0] - vec_b0[0], vec_a0[1] - vec_b0[1], vec_a0[2] - vec_b0[2]};
    std::vector<double> vec_diff_1;// =  {vec_a1[0] - vec_b1[0], vec_a1[1] - vec_b1[1], vec_a1[2] - vec_b1[2]};

    for(int iDim=0;iDim<p->dim;iDim++)
    {
        double dr0 = A_0->get_coord(iDim) - B_0->get_coord(iDim);
        if( dr0 > 0.5 * p->length ) dr0 = dr0 - p->length;
        if( dr0 < -0.5 * p->length ) dr0 = dr0 + p->length;

        double dr1 = A_1->get_coord(iDim) - B_1->get_coord(iDim);
        if( dr1 > 0.5 * p->length ) dr1 = dr1 - p->length;
        if( dr1 < -0.5 * p->length ) dr1 = dr1 + p->length;

        vec_diff_0.push_back(dr0);
        vec_diff_1.push_back(dr1);

//         a += dr0*dr0;
//         b += dr1*dr1;
    }


//     double kappa = 1./(2.*p->red_mass);
//     double z = p->charge_spec_1 * p->charge_spec_2 / kappa;
    auto u1 = table_ptr->get_pair_action(vec_diff_0, vec_diff_1);


/*
    double r1 = 0.0;
    double r2 = 0.0;

    for(int iDim=0;iDim<p->dim;iDim++)
    {
        double d1 = fabs(A_0->get_coord(iDim) - B_0->get_coord(iDim));
        if( d1 > 0.5*p->length ) d1 = d1 - p->length;


        double d2 = fabs(A_1->get_coord(iDim) - B_1->get_coord(iDim));
        if( d2 > 0.5*p->length ) d2 = d2 - p->length;


        r1 += d1*d1;
        r2 += d2*d2;
    }
*/
	// TBD TBD TBD Implement the mass derivatives for the kinetic energy!
	std::vector<double> result{ u1[0], -u1[1], 0.0, 0.0 };

    return result;

}

void Hydrogen_PA::init(Parameters *new_p)
{
    p = new_p;
//     ws = gsl_integration_workspace_alloc(1500);

    // Get the smart pointer reference to the paramters table_ptr
    table_ptr = p->table_ptr;
}







// Calculate the entire Madelung constant and set number of required iterations:
double Hydrogen_PA::get_madelung_constant(double tol, int* max_it, double kappa, int max_index)
{
    double ans = 0.0;
    double last_ans = 1000.0;

    int cnt = 0; // number of iterations

    double inverse_length = 1.0 / (*p).length;
    double kappa_sq = kappa*kappa;

    while( fabs( (ans-last_ans)/last_ans ) > tol ) // increase number of boxes in real space until changes become smaller than tol
    {
        cnt++;
        last_ans = ans;
        ans = 0.0;

        if( cnt == max_index )
        {
            (*max_it) = max_index;
            return last_ans;
        }

        double ans_r = 0.0;
        double ans_k = 0.0;

        for(int nx=-cnt;nx<cnt+1;nx++)
        {
            double G_x = nx * inverse_length;
            double G_x_sq = G_x * G_x;
            double R_x = nx * (*p).length;
            double R_x_sq = R_x * R_x;
            for(int ny=-cnt;ny<cnt+1;ny++)
            {
                double G_y = ny * inverse_length;
                double G_y_sq = G_y * G_y;
                double R_y = ny * (*p).length;
                double R_y_sq = R_y * R_y;
                for(int nz=-cnt;nz<cnt+1;nz++)
                {
                    double G_z = nz * inverse_length;
                    double G_z_sq = G_z * G_z;
                    double R_z = nz * (*p).length;
                    double R_z_sq = R_z * R_z;


                    // Do not include the zero G-vector
                    if( !( ( nx == 0 ) && ( ny == 0 ) && ( nz == 0 ) ) )
                    {

                        double G_sq = G_x_sq + G_y_sq + G_z_sq;
                        double r_abs = sqrt( R_x_sq + R_y_sq + R_z_sq );

                        ans_k += exp( -pi*pi*G_sq/kappa_sq ) / (pi * G_sq);
                        ans_r += erfc( kappa * r_abs ) / r_abs;

                    } // end zero vector cond.


                } // end loop nz
            }
        } // end loop nx

// 		std::cout << "k_cnt: " << cnt << "\t ans: " << ans << "\n";

        ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r - 2.0*kappa/sqrt(pi);




    } // end of convergence condition


    // Set the maximum index number
    (*max_it) = cnt-1;

    // Return reciprocal part of Madelung const:
    return ans;

}


// External potential is Madelung contribution
double Hydrogen_PA::ext_pot(Bead* a)
{
	double harmonic = 0.0;
	
	if( a->get_charge() < 0 ) // only electrons are supposed to be harmonically perturbed here!
	{
		double x_a = (*a).get_coord( 0 );
		double y_a = (*a).get_coord( 1 );
		double z_a = (*a).get_coord( 2 );

		double cos_arg = ( 2.0*pi/(*p).length * ( (*p).pq_x*x_a + (*p).pq_y*y_a + (*p).pq_z*z_a ) );
		double cos_arg2 = ( 2.0*pi/p->length * ( p->pq_x2*x_a + p->pq_y2*y_a + p->pq_z2*z_a ) );
		double cos_arg3 = ( 2.0*pi/p->length * ( p->pq_x3*x_a + p->pq_y3*y_a + p->pq_z3*z_a ) );
		
		harmonic = 2.0*( cos( cos_arg )*(*p).vq + cos( cos_arg2 )*p->vq2 + cos( cos_arg3 )*p->vq3 );
	}
	
	
    return harmonic + 0.50*(*p).madelung;
}


// We measure the direct distance, not the closest to the next image.
double Hydrogen_PA::get_diff(double a, double b)
{
    double tmp = a - b;

    return tmp;
}


double Hydrogen_PA::distance_sq(Bead* a, Bead* b)
{
    double ans = 0.0;
    for(int i=0;i<(*p).dim;i++)
    {
// 		double tmp = get_diff( (*a).get_coord(i), (*b).get_coord(i) );
        double tmp = (*a).get_coord(i) - (*b).get_coord(i);
        while( tmp > (*p).length*0.5 )
        {
            tmp = tmp - (*p).length;
        }
        while( tmp < -(*p).length*0.5 )
        {
            tmp = tmp + (*p).length;
        }


        ans += tmp * tmp;
    }
    return ans;
}


double Hydrogen_PA::distance(Bead* a, Bead* b)
{
    return sqrt(distance_sq(a, b));
}


double Hydrogen_PA::Ewald_pair_index(Bead* a, Bead* b, int cnt)
{
    double ans;

    double ans_r = 0.0;
    double ans_k = 0.0;

// 	int cnt = (*p).r_index;
    double inverse_length = 1.0 / (*p).length;
    double kappa = (*p).kappa_int;
    double kappa_sq = kappa*kappa;

    for(int nx=-cnt;nx<cnt+1;nx++)
    {
        double G_x = nx * inverse_length;
        double G_x_sq = G_x * G_x;
        double R_x = nx * (*p).length;

        for(int ny=-cnt;ny<cnt+1;ny++)
        {
            double G_y = ny * inverse_length;
            double G_y_sq = G_y * G_y;
            double R_y = ny * (*p).length;

            for(int nz=-cnt;nz<cnt+1;nz++)
            {
                double G_z = nz * inverse_length;
                double G_z_sq = G_z * G_z;
                double R_z = nz * (*p).length;

                double x_diff = get_diff( (*a).get_coord(0), (*b).get_coord(0) );
                double y_diff = get_diff( (*a).get_coord(1), (*b).get_coord(1) );
                double z_diff = get_diff( (*a).get_coord(2), (*b).get_coord(2) );


                // Do not include the zero G-vector
                if( !( (nx == 0) && (ny == 0) && (nz == 0) ) )
                {
                    double G_sq = G_x_sq + G_y_sq + G_z_sq;

                    double x = G_x * x_diff;
                    double y = G_y * y_diff;
                    double z = G_z * z_diff;

                    double cos_arg = 2.0*pi*( x + y + z );

// 					double my_exp = (*p).get_Ewald_exp(nx, ny, nz);
// 					ans_k += ( cos(cos_arg) * my_exp ) / (pi * G_sq);
                    ans_k += ( cos(cos_arg) * exp( -pi*pi*G_sq/kappa_sq ) ) / (pi * G_sq);

                } // end zero vector cond.


                double x = R_x + x_diff;
                double y = R_y + y_diff;
                double z = R_z + z_diff;

                double my_abs = sqrt( x*x + y*y + z*z );


                ans_r += erfc( kappa * my_abs ) / my_abs;




            } // end loop nz
        }
    } // end loop nx

    ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;

    return ans;

}


double Hydrogen_PA::Ewald_pair(Bead* a, Bead* b, double tol, int* max_it, double kappa, int max_index)
{
    double ans = 0.0;
    double last_ans = 1000.0;

    int cnt = 0; // number of iterations

    double inverse_length = 1.0 / (*p).length;
    double kappa_sq = kappa*kappa;

    while( fabs( (ans-last_ans)/last_ans ) > tol ) // increase number of boxes in real space until changes become smaller than tol
    {
        cnt++;
        last_ans = ans;
        ans = 0.0;

        if( cnt == max_index )
        {
            (*max_it) = max_index;
            return last_ans;
        }

        double ans_r = 0.0;
        double ans_k = 0.0;

        for(int nx=-cnt;nx<cnt+1;nx++)
        {
            double G_x = nx * inverse_length;
            double G_x_sq = G_x * G_x;
            double R_x = nx * (*p).length;

            for(int ny=-cnt;ny<cnt+1;ny++)
            {
                double G_y = ny * inverse_length;
                double G_y_sq = G_y * G_y;
                double R_y = ny * (*p).length;

                for(int nz=-cnt;nz<cnt+1;nz++)
                {
                    double G_z = nz * inverse_length;
                    double G_z_sq = G_z * G_z;
                    double R_z = nz * (*p).length;

                    double x_diff = get_diff( (*a).get_coord(0), (*b).get_coord(0) );
                    double y_diff = get_diff( (*a).get_coord(1), (*b).get_coord(1) );
                    double z_diff = get_diff( (*a).get_coord(2), (*b).get_coord(2) );

                    // Do not include the zero G-vector
                    if( !( (nx == 0) && (ny == 0) && (nz == 0) ) )
                    {
                        double G_sq = G_x_sq + G_y_sq + G_z_sq;

                        double x = G_x * x_diff;
                        double y = G_y * y_diff;
                        double z = G_z * z_diff;

                        double cos_arg = 2.0*pi*( x + y + z );

                        ans_k += ( cos(cos_arg) * exp( -pi*pi*G_sq/kappa_sq ) ) / (pi * G_sq);

                    } // end zero vector cond.


                    double x = R_x + x_diff;
                    double y = R_y + y_diff;
                    double z = R_z + z_diff;

                    double my_abs = sqrt( x*x + y*y + z*z );


                    ans_r += erfc( kappa * my_abs ) / my_abs;



                } // end loop nz
            }
        } // end loop nx

        ans = ans_k/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;


    } // end of convergence condition


    // Set the maximum index number
    (*max_it) = cnt - 1;

    // Return the Ewald pair interaction
    return ans;


}


double Hydrogen_PA::adjust_coordinate(double coord)
{
    while( coord < 0.0 )
    {
        coord += (*p).length;
    }
    while( coord > (*p).length )
    {
        coord -= (*p).length;
    }
    return coord;
}

double Hydrogen_PA::rho(Bead* a, Bead* b, double scale)
{


    double delta_tau = (*p).epsilon * scale; // Obtain the imaginary time step between beads a and b

    double lambda_sq =  2.0 * pi * delta_tau / a->get_mass(); // thermal wavelength squared from timestep delta_tau;

    double norm = pow( sqrt(lambda_sq), (*p).dim ); // Calculate the normalization of rho(...)
//	double norm = 1.0;

    double ans = 0.0; // init the sum over all images

    Bead image_bead;

    // Loops over 2*n_boxes+1 images in each dimension
    for(int iX=-(*p).n_boxes;iX<(*p).n_boxes+1;iX++)
    {
        for(int iY=-(*p).n_boxes;iY<(*p).n_boxes+1;iY++)
        {
            for(int iZ=-(*p).n_boxes;iZ<(*p).n_boxes+1;iZ++)
            {

                double my_x = (*b).get_coord(0) + iX*(*p).length;
                double my_y = (*b).get_coord(1) + iY*(*p).length;
                double my_z = (*b).get_coord(2) + iZ*(*p).length;

                double delta_x = (*a).get_coord(0) - my_x;
                double delta_y = (*a).get_coord(1) - my_y;
                double delta_z = (*a).get_coord(2) - my_z;

                double delta_r_sq = delta_x*delta_x + delta_y*delta_y + delta_z*delta_z;

                ans += exp( -pi * delta_r_sq / lambda_sq );
            }
        }
    }

    return ans / norm;

}

double Hydrogen_PA::pair_interaction(Bead* a, Bead* b)
{
    double G_fac = 2.0*pi/p->length;

    double ans_r = 0.0;
    double ans_k = 0.0;

    double kappa = p->kappa_int;

    double dx = a->get_coord(0) - b->get_coord(0);
    double dy = a->get_coord(1) - b->get_coord(1);
    double dz = a->get_coord(2) - b->get_coord(2);



    // Pre-compute the cosine-factors for the reciprocal part:
    std::vector<double> GX,GY,GZ;
    for(int iK=0;iK<p->k_index+1;iK++)
    {
        GX.push_back( cos(iK*G_fac*dx) );
        GY.push_back( cos(iK*G_fac*dy) );
        GZ.push_back( cos(iK*G_fac*dz) );
    }


    double ans_k_new = 0.0;
    // Evaluate the sum in reciprocal space:
    for(int iX=-p->k_index;iX<p->k_index+1;iX++)
    {
        int x_index = abs( iX );
        for(int iY=-p->k_index;iY<p->k_index+1;iY++)
        {
            int y_index = abs( iY );
            for(int iZ=-p->k_index;iZ<p->k_index+1;iZ++)
            {
                int z_index = abs( iZ );

//  				double G_sq = ( iX*iX + iY*iY + iZ*iZ ) / ( p->length * p->length );

                double cos_term = GX[ x_index ]*GY[ y_index ]*GZ[ z_index ];
// 				double my_exp = exp( -pi*pi*G_sq/(kappa*kappa) );
                double my_exp = p->get_Ewald_exp(iX, iY, iZ);

                if( ( iX != 0 ) || ( iY != 0 ) || ( iZ != 0 ) )
                {
                    ans_k_new +=  cos_term * my_exp / pi;
                }

            }
        }
    }

    int max_r = 1+int( p->r_index/4 );
    double cutoff = p->r_index * 0.250 * p->length;

    // Evaluate the sum in real space:
    for(int iX=-max_r;iX<max_r+1;iX++)
    {
        double x = dx + p->length*iX;
        for(int iY=-max_r;iY<max_r+1;iY++)
        {
            double y = dy + p->length*iY;
            for(int iZ=-max_r;iZ<max_r+1;iZ++)
            {
                double z = dz + p->length*iZ;

                double r_abs = sqrt( x*x + y*y + z*z );

               
				if( r_abs <= cutoff )
				{
					
 					if( ( fabs(x) < p->length*0.5 ) && ( fabs(y) < p->length*0.5 ) && ( fabs(z) < p->length*0.5 ) )
 					{
						if( a->get_charge()*b->get_charge() < 0 )
						{
							ans_r -= erf(r_abs * kappa) / r_abs;
						}
						else
						{
							ans_r += erfc( r_abs*kappa ) / r_abs;
						}
 					}
 					else
 					{
						ans_r += erfc( r_abs*kappa ) / r_abs;
 					}
				}
				
				
				
				
            }
        }


    }






    



    
    double ans = ans_k_new/(*p).volume - pi/( kappa*kappa*(*p).volume ) + ans_r;

    return ans * a->get_charge() * b->get_charge();

}















Hydrogen_PA::~Hydrogen_PA()
{
}
