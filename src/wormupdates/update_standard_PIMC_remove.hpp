/*
 *  Contains the standard PIMC update Remove (G-sector)
 */



template <class inter> class update_standard_PIMC_remove
{
public:
	
	void init( bool new_debug, config_ensemble* new_ensemble );
	int execute( int m_max, double c_bar, double p_remove, double p_insert ); // execute the update
	
	
private:
	
	// Variables of the update:
	bool debug;
	
	config_ensemble* ensemble; // Pointer to ensemble of particle species
	

	// all information about what kind of system we have:
	inter my_interaction;
	
	// Utilize the appropriate toolboxes:
	interaction_toolbox<inter> inter_tools;
	sampling_toolbox<inter> sampling_tools;	
};



// init
template <class inter> void update_standard_PIMC_remove<inter>::init( bool new_debug, config_ensemble* new_ensemble )
{
	std::cout << "initialize the standard-PIMC update REMOVE (ensemble)\n";
	
	ensemble = new_ensemble;
	debug = new_debug;
	
	my_interaction.init( &(*ensemble).params );

	sampling_tools.init( &(*ensemble).params );
	
	std::cout << "initialization successful!\n";
}




// Execute the Monte Carlo update:
template <class inter> int update_standard_PIMC_remove<inter>::execute( int m_max, double c_bar, double p_remove, double p_insert )
{
	
	// obtain the configuration (species) that is in the G-sector:
	int my_species = ensemble->G_species;
	
	// obtain pointer to G-species-config:
	worm_configuration* config = &(*ensemble).species_config[ my_species ];
	
	// initialize the interaction toolbox:
	inter_tools.init( config );
	
	// obtain the number of beads between tail and head (forward in imag. time):
	int m = 0;
	int s_id = config->tail_id;
	
	while( config->beads[ s_id ].get_next_id() != config->head_id )
	{
		s_id = config->beads[ s_id ].get_next_id();
		m++;
		if( m > m_max + 3 )
		{
			break;
		}
	}
	
	if( m == 0 ) // empty trajectories are not allowed!
	{
		std::cout << "Error in update_standard_PIMC_remove, m=0. head_id: " << config->head_id << "\t tail_id: " << config->tail_id << "\t head_time: " << config->beads[ config->head_id ].get_time() << "\t tail_time: " << config->beads[ config->tail_id ].get_time() << "\n";
		exit(0);
	}
	
	if(debug) // verbose:
	{
		std::cout << "~~~~~~~~~~~~~~~~ update_standard_PIMC_remove, m_max: " << m_max << "\t m: " << m << "\n"; 
		std::cout << "my_species: " << my_species << std::endl;
	}
	
	if( m > m_max ) // The worm-trajectory is too long, no counter-insert (detailed balance), therefore reject!
	{
		return 0;
	}

	
	// Check, if the tail is within the INSERT-volume, otherwise there is no reverse move (detailed balance!)
	// Is only relevant for the traps, for PBC, insert is possible in the entire box (volume)
	if( config->params.system_type < config->params.n_traps )
	{
		for(int iDim=0;iDim<config->params.dim;iDim++)
		{
			double x = config->beads[ config->tail_id ].get_coord( iDim );
			if( ( x < -config->params.length ) || ( x > config->params.length ) ) // Insert-volume of the traps
			{
				if(debug) std::cout << "Rejected due to coord_out_of_box, x: " << x << "\t iDim: " << iDim << "\tL: " << config->params.length << "\n";
				return 0;
			}
		}
	}


	// obtain the change in the interaction and the diffusion_element_product:
	double delta_u = 0.0; // OLD-NEW
	double diffusion_element_product = config->beads[ config->tail_id ].get_diffusion_element();
	
	double sampling_diffusion_ratio = 1.0 / diffusion_element_product;
	
	
	// treat interaction from head and tail separately:
	int tail_slice = config->beads[ config->tail_id ].get_time();
	int head_slice = config->beads[ config->head_id ].get_time();
	
	// obtain interaction due to the same species (and ext_pot):
	double inter_tail = inter_tools.old_interaction( &(*config).beadlist[ tail_slice ], config->tail_id, -1 );
	double inter_head = inter_tools.old_interaction( &(*config).beadlist[ head_slice ], config->head_id, -1 );
	
	// obtain interaction of tail and head with all the other species:
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
	{
		if( iSpecies != my_species )
		{
			inter_tail += inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &(*config).beads[ config->tail_id ] );
			inter_head += inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &(*config).beads[ config->head_id ] );
		}
	}
	
	delta_u += 0.50 * ( inter_head + inter_tail ); // head and tail do only count half
	
	std::vector<Bead>new_beads( 1, config->beads[ config->tail_id ] );
	
	int i_id = config->tail_id;
	std::vector<double> diffusion_vector( m, 0.0 ); // vector for the diffusion elements to be removed (bead-by-bead)
	
	for(int i=0;i<m;i++)
	{
		i_id = config->beads[ i_id ].get_next_id();
		int i_time = config->beads[ i_id ].get_time();
		
		// interaction of i_id due to same species (and ext_pot):
		delta_u += inter_tools.old_interaction( &(*config).beadlist[ i_time ], i_id, -1 );
		
		// interaction due to all the other species:
		for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
		{
			if( iSpecies != my_species )
			{
				delta_u += inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &(*config).beads[ i_id ] );
			}
		}

		diffusion_element_product *= config->beads[ i_id ].get_diffusion_element(); // compute product on-the-fly, for debug purposes only (overflow!)
		diffusion_vector[i] = config->beads[ i_id ].get_diffusion_element();
		
		new_beads.push_back( config->beads[ i_id ] );
		
	} // end loop i to m
	
	new_beads.push_back( config->beads[ config->head_id ] );
	
	// obtain the change in the mu-exponent (chem. pot.):
	double delta_exponent = -config->params.mu * config->params.epsilon * (1.0+m);
	
	
	// obtain all the reverse sampling probs:
	// Reverse prob. to generate the head starting from the tail:
	double prob_head = sampling_tools.reverse_head_prob( &(*config).beads[ config->tail_id ], &(*config).beads[ config->head_id ] );
	std::vector<double> probs_connect( m, 0.0 ); // vector for the sampling probs of the individual beads (bead-by-bead)

	// Reverse prob to sample the m beads between head and tail:
	double prob_connect;
	if( config->params.system_type < config->params.n_traps )
	{
		prob_connect = sampling_tools.trap_backward_connect( &new_beads, m, probs_connect ); // For the trap system
	}
	else 
	{
		prob_connect = sampling_tools.PBC_backward_connect( &new_beads, m, probs_connect ); // For the PBC system 
	}
	
	
	// obtain a random number to decide acceptance of the update:
	double choice = rdm();
	
	// obtain the acceptance ratio:
	double ratio = exp( delta_exponent ) * exp( config->params.epsilon * delta_u ) / ( config->params.n_bead * config->params.volume * ensemble->species_config.size() * m_max * c_bar );

	if( config->params.PBC_detailed_balance ) // If rho is a sum over Gaussians, diffusion and sampling terms do not exactly cancel!
	{
		for(int i=0;i<m;i++)
		{
			sampling_diffusion_ratio *= ( probs_connect[i] / diffusion_vector[i] );
		}
		ratio = ratio * sampling_diffusion_ratio * prob_head;
	}
	
	if( debug ) // verbose, and check for overflow in case of !PBC_detailed_balance
	{
		double ratio2 = exp( delta_exponent ) * exp( config->params.epsilon * delta_u ) * prob_connect * prob_head / ( diffusion_element_product * config->params.n_bead * config->params.volume * ensemble->species_config.size() * m_max * c_bar );

		if( fabs( ratio/ratio2-1.0) > 1e-13 )
		{
			std::cout << "Error in std_PIMC Remove, ratio = " << ratio << "\t ratio2= " << ratio2 << "\n";
			std::cout << "diff: " << fabs(ratio/ratio2-1) << "\n";
			int cin;
			std::cin >> cin;
		}
	}
	

	// Artificial potential to favor a given target N:
	// -> Use carefully!
	if( config->params.N_potential_control )
	{
		double old_N_diff =  config->params.N - config->my_exp / ( config->params.mu * config->params.beta );
		double new_N_diff = config->params.N - ( config->my_exp + delta_exponent ) / ( config->params.mu * config->params.beta );
		double W_N_scale = -0.50 / ( config->params.sigma_target * config->params.sigma_target );
		double W_N_new = exp( W_N_scale * new_N_diff * new_N_diff );
		double W_N_old = exp( W_N_scale * old_N_diff * old_N_diff );

		if(debug) std::cout << "W_N_new = " << W_N_new << "\t W_N_old = " << W_N_old << "\t W_N_scale = " << W_N_scale << "\n";
		ratio = ratio * W_N_new / W_N_old;
	}


	// correction due to different update frequencies:
	ratio = ratio * p_insert / p_remove;
	
	if( debug ) // verbose:
	{
		std::cout << "ratio: " << ratio << "\t choice: " << choice << "\t c_bar: " << c_bar << "\t m_max: " << m_max << "\n";
		std::cout << "delta_exponent: " << delta_exponent << "\t diffusion_element_product: " << diffusion_element_product << "\t prob_connect: " << prob_connect << "\t prob_head: " << prob_head << "\n";
	
		std::cout << "p_remove: " << p_remove << "\t p_insert: " << p_insert << "\n";
	}


	if( choice <= ratio ) // The update is accepted:
	{
		// Product of diffusion elements, for debug purposes only!
		ensemble->total_diffusion_product = ensemble->total_diffusion_product / diffusion_element_product;
		
		ensemble->diag = 1; // The entire ensemble of configurations is again in the diagonal sector.
		ensemble->G_species = -1; // no species in the G-sector
		config->diag = 1; // this species is again diagonal
		
		ensemble->total_energy -= delta_u; // update total energy of ensemble
		ensemble->total_exponent += delta_exponent; // update the (mu)-exponent of the entire ensemble

		config->my_exp += delta_exponent; // update mu-exponent of the selected species

		// Now delete the "worm" from this configuration:
		int i_id = config->tail_id;
		for(int i=0;i<2+m;i++)
		{
			int tmp = config->beads[ i_id ].get_next_id();
			(*config).delete_bead( i_id );
			i_id = tmp;
		}
		
		// Reset head- and tail-id:
		config->head_id = -1;
		config->tail_id = -1;
		
		// update the total number of particles in the system:
		int new_N_tot = 0;
		for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
		{
			new_N_tot += ensemble->species_config[ iSpecies ].beadlist[ 0 ].size();
		}
		ensemble->N_tot = new_N_tot;
		
		if( debug ) // Debug option: Write particle coordinates and bead structure to the disk
		{
			config->standard_PIMC_print_paths( "remove_paths.dat" );
			config->standard_PIMC_print_beadlist( "remove_beadlist.dat" );
			ensemble->print_standard_PIMC_ensemble_paths( "remove" ); // Print the paths of all worm_configs
			ensemble->print_standard_PIMC_ensemble_beadlist( "remove" ); // Print the beadlist of all worm_configs
		}
		
		return 1;
	}
	else // The update is rejected
	{
		return 0;
	}
}




























