/*
 *  Contains the standard PIMC update Move
 */


template <class inter> class update_standard_PIMC_move
{
public:
	
	void init( bool new_debug, config_ensemble* new_config_ensemble );
	int execute( double delta_scale, bool proton ); // execute the update
	
private:
	
	// Variables of the update:
	bool debug; // Execute Update in Debug mode?
	
	config_ensemble* ensemble; // pointer to the ensemble of particle species
	

	// all information about what kind of system we have:
	inter my_interaction;
	
	// Utilize the appropriate toolboxes:
	interaction_toolbox<inter> inter_tools;
	sampling_toolbox<inter> sampling_tools;
	pair_action_toolbox<inter> pair_tools;
};



// init
template <class inter> void update_standard_PIMC_move<inter>::init( bool new_debug, config_ensemble* new_config_ensemble )
{
	std::cout << "initialize the standard-PIMC update MOVE (ensemble)\n";
	
	ensemble = new_config_ensemble;
	debug = new_debug;
	
	my_interaction.init( &(*ensemble).params );

	sampling_tools.init( &(*ensemble).params );
	
	pair_tools.init( &(*ensemble).params );

	std::cout << "initialization successful!\n";
}



// Execute the Monte Carlo update:
template <class inter> int update_standard_PIMC_move<inter>::execute( double delta_scale, bool proton )
{
	
	if( debug ) std::cout << "****** std-PIMC (ensemble) update MOVE \n"; 
	
	// Randomly select the species to be updated:
	int quant = ensemble->species_config.size();
	if( quant > 2 ) quant = 2;
	int my_species = quant*rdm();
	
	
	
	
	if( proton ) my_species = 2;
	
	
	if( debug ) std::cout << "my_species: " << my_species << "\n";
		
	// obtain a pointer to the worm_config to be updated:
	worm_configuration* config = &(*ensemble).species_config[ my_species ];
	
	// Initialize the interaction toolbox with the selected particle speciec config:
	inter_tools.init( &(*ensemble).species_config[ my_species ] );
	
	// Check, if there exist particles in the selected species:
	if( config->beadlist[ 0 ].size() == 0 ) return 0;
	
	// Obtain random bead at slice 0:
	int my_rdm_id = rdm()*config->beadlist[ 0 ].size();
	my_rdm_id = config->beadlist[ 0 ][ my_rdm_id ];
	if( debug ) std::cout << "my_rdm_id: " << my_rdm_id << "\n";
	
	// Check, if this bead belongs to a single, closed particle:
	int tmp_id = my_rdm_id;
	for(int i=0;i<config->params.n_bead;i++)
	{
		tmp_id = config->beads[ tmp_id ].get_next_id();
		if( tmp_id == -1 ) return 0;
		if( tmp_id == config->head_id ) return 0; // if we encounter the head, or a missing link, the update is rejected
	}
	
	if( tmp_id != my_rdm_id )
	{
		if( debug ) std::cout << "Bead is part of exchange_cycle, tmp_id: " << tmp_id << "\n";
		return 0; // Reject the update, if the selected bead is part of an exchange cycle!
	}
	
	// Randomly select the displacement of the particle:
	std::vector<double>delta( config->params.dim, 0.0 );
	for(int iDim=0;iDim<config->params.dim;iDim++)
	{
		delta[ iDim ] = delta_scale * ( 2.0*rdm() - 1.0 ) * config->params.rs;
		if( debug ) std::cout << "iDim: " << iDim << "\t delta: " << delta[ iDim ] << "\n";
	}
	
	
	// Create P copies of all affected beads and dispace them by delta, simultaneously obtain the change in the interaction:
	double delta_u = 0.0; // OLD - NEW
	
	double delta_pair = 0.0;
	double delta_pair_derivative = 0.0;
	
	double delta_m_e_pair_derivative = 0.0;
	double delta_m_p_pair_derivative = 0.0;
	

	double delta_ext_pot_action = 0.0;
	double delta_ext_pot_action_derivative = 0.0;
	
	
	
	
	
	
	
	// ##########################################################################################################################
	// ### Start::OpenMP parallelization part
	
	
	
	std::vector<double> OpenMP_delta_u( config->params.n_bead, 0.0 );
	std::vector<double> OpenMP_delta_pair( config->params.n_bead, 0.0 );
	std::vector<double> OpenMP_delta_pair_derivative( config->params.n_bead, 0.0 );
	std::vector<double> OpenMP_delta_m_e_pair_derivative( config->params.n_bead, 0.0 );
	std::vector<double> OpenMP_delta_m_p_pair_derivative( config->params.n_bead, 0.0 );
	std::vector<double> OpenMP_delta_ext_pot_action( config->params.n_bead, 0.0 );
	std::vector<double> OpenMP_delta_ext_pot_action_derivative( config->params.n_bead, 0.0 );
	
	

	
	std::vector<Bead>new_beads;
	tmp_id = my_rdm_id;
	for(int i=0;i<config->params.n_bead;i++)
	{
		new_beads.push_back( config->beads[ tmp_id ] );
		for(int iDim=0;iDim<config->params.dim;iDim++)
		{
			new_beads[ i ].set_coord( iDim, my_interaction.adjust_coordinate( new_beads[ i ].get_coord( iDim ) + delta[ iDim ] ) );
		}
		
		// Change in the potential energy within my_species, old - new:
		// ### OpenMP:: delta_u += inter_tools.change_interaction( &(*config).beadlist[ new_beads[ i ].get_time() ], &new_beads[ i ], tmp_id , -1 );
		

		tmp_id = config->beads[ tmp_id ].get_next_id();
	}
	
	
	
	
	if( debug ) std::cout << "INtermediate-u:" << delta_u << "\n";
	
	
	#pragma omp parallel 
	{
		#pragma omp for
		for(int i=0;i<config->params.n_bead;i++)
		{
			int i_id_1 = new_beads[i].get_next_id();
			int i_id = config->beads[ i_id_1 ].get_prev_id();
			
			OpenMP_delta_u[ i ] += inter_tools.change_interaction( &(*config).beadlist[ new_beads[ i ].get_time() ], &new_beads[ i ], i_id , -1 );
			
			
			// Change in the potential energy due to all other species:
			for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
			{
				if( iSpecies != my_species )
				{
					double old_sp = inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &(*config).beads[ i_id ] );
					double new_sp = inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ i ] );
					
					// ### OpenMP:: delta_u += ( old_sp - new_sp );
					OpenMP_delta_u[ i ] += ( old_sp - new_sp );
				}
			}
			
			
			int next_index = 1+i;
			if( i == config->params.n_bead-1 ) next_index = 0;
			
			std::vector<double> dA = pair_tools.change_pair_action( i_id, i_id_1, &new_beads[i], &new_beads[ next_index ], my_species, ensemble ); // is actually the change in ext_pot_action(), historic name :roll_eyes:
			
			// ### OpenMP:: delta_pair += dA[0];
			// ### OpenMP:: delta_pair_derivative += dA[1];
			
			// ### OpenMP:: delta_m_e_pair_derivative += dA[2];
			// ### OpenMP:: delta_m_p_pair_derivative += dA[3];
			
			OpenMP_delta_pair[ i ] += dA[0];
			OpenMP_delta_pair_derivative[ i ] += dA[1];
			
			OpenMP_delta_m_e_pair_derivative[ i ] += dA[2];
			OpenMP_delta_m_p_pair_derivative[ i ] += dA[3];
			
			
			
			
			std::vector<double> old_ext_pot_action, new_ext_pot_action;
			old_ext_pot_action = my_interaction.ext_pot_action( &(*config).beads[ i_id ], &(*config).beads[ i_id_1 ] );
			int i_plus_one = 1+i;
			if( i_plus_one >= config->params.n_bead ) i_plus_one -= config->params.n_bead;
			new_ext_pot_action = my_interaction.ext_pot_action( &new_beads[ i ], &new_beads[ i_plus_one ] );
			
			
			
			
			// ### OpenMP:: delta_ext_pot_action += ( old_ext_pot_action[0] - new_ext_pot_action[0] );
			// ### OpenMP:: delta_ext_pot_action_derivative += ( old_ext_pot_action[1] - new_ext_pot_action[1] );
			
			OpenMP_delta_ext_pot_action[ i ] += ( old_ext_pot_action[0] - new_ext_pot_action[0] );
			OpenMP_delta_ext_pot_action_derivative[ i ] += ( old_ext_pot_action[1] - new_ext_pot_action[1] );
			
			
		}
	
	}
	
	// After OMP post-processing loop!
	for(int i=0;i<config->params.n_bead;i++)
	{		
		delta_u += OpenMP_delta_u[ i ];
		delta_pair += OpenMP_delta_pair[ i ];
		delta_pair_derivative += OpenMP_delta_pair_derivative[ i ];
		delta_m_e_pair_derivative += OpenMP_delta_m_e_pair_derivative[ i ];
		delta_m_p_pair_derivative += OpenMP_delta_m_p_pair_derivative[ i ];
		delta_ext_pot_action += OpenMP_delta_ext_pot_action[ i ];
		delta_ext_pot_action_derivative += OpenMP_delta_ext_pot_action_derivative[ i ];
	}
	

		
	// ### End::OpenMP parallelization part
	// ##########################################################################################################################

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Obtain a random number to decide acceptance:
	double choice = rdm();
	
	// Calculate the acceptance prob:
	double ratio = exp( config->params.epsilon * delta_u ) * exp( config->params.epsilon * delta_pair ) * exp( config->params.epsilon * delta_ext_pot_action );
	
	// verbose:
	if( debug )
	{
		std::cout << "ratio: " << ratio << "\t choice: " << choice << "\t delta_u: " << delta_u << "\t delta_pair: " << delta_pair << "\n";
		std::cout << "delta_pair_derivative: " << delta_pair_derivative << "\n";
		std::cout << "delta_m_e_pair_derivative: " << delta_m_e_pair_derivative << "\t delta_m_p_pair_derivative: " << delta_m_p_pair_derivative << "\n";
		std::cout << "delta_ext_pot_action: " << delta_ext_pot_action << "\t delta_ext_pot_action_derivative: " << delta_ext_pot_action_derivative << "\n";
	}
	

	if( choice <= ratio ) // The update has been accepted:
	{

		// book keeping of the total potential energy:
		ensemble->total_energy -= delta_u;
		
		// book keeping of the total pair action in the system:
		ensemble->total_pair_action -= delta_pair;
		ensemble->total_pair_derivative -= delta_pair_derivative;
		
		// book keeping of the ext_pot_action:
		ensemble->total_ext_pot_action -= delta_ext_pot_action;
		ensemble->total_ext_pot_action_derivative -= delta_ext_pot_action_derivative;
		
		ensemble->total_m_e_pair_derivative -= delta_m_e_pair_derivative;
		ensemble->total_m_p_pair_derivative -= delta_m_p_pair_derivative;
		
		
		// Replace the coords of the dispaced single partice:
		tmp_id = my_rdm_id;
		for(int i=0;i<config->params.n_bead;i++)
		{
			for(int iDim=0;iDim<config->params.dim;iDim++)
			{
				config->beads[ tmp_id ].set_coord( iDim, new_beads[ i ].get_coord( iDim ) );
			}
			tmp_id = config->beads[ tmp_id ].get_next_id();
		}
		
		
		if( debug ) // Debug option: Write particle coordinates and bead structure to the disk
		{
			std::cout << "-+my_species: " << my_species << "\n";
			config->standard_PIMC_print_paths( "move_paths.dat" );
			config->standard_PIMC_print_beadlist( "move_beadlist.dat" );
		}

		return 1;
	}
	else // The update has been rejected
	{
		return 0;
	}
}




























