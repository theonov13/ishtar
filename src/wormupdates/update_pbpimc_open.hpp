/*
 *  Contains the PB-PIMC update open
 * -> m_max is the maximum number of LINKS to be deleted -> delete max. m_max-1 beads
 *  ### Update has been debugged and benchmarked. Yet, use in PB-PIMC simulations is NOT recommended!
 *  ### Worm algorithm is unnecessarily complicated. Use Deform update, instead!
 *  ### No implementation of bead-by-bead for sampling probs, etc. However, overflow is checked and does not happen in practice!
 */

template <class inter> class update_pbpimc_open
{
public:
	
	void init( bool new_debug, config_ensemble* new_ensemble );
	int execute( int m_max, double factor ); // execute the update
	
private:
	
	// Variables of the update:
	config_ensemble* ensemble; // Pointer to ensemble of particle species
	bool debug; // Execute Update in Debug mode?
	
	
	// Constant structures to prevent dynamic memory allocation:
	
	std::vector< selection_toolbox<inter> >selection_tools;
	std::vector< sampling_toolbox<inter> >sampling_tools;
	std::vector< force_toolbox<inter> >force_tools;
	std::vector< interaction_toolbox<inter> >inter_tools;
	std::vector< diffusion_toolbox<inter> >diffusion_tools;
	
	std::vector< inter > my_interaction;

	std::vector< change_config >update;
	
	std::vector<std::vector <std::vector <double> > > const_store_the_force;
	std::vector<std::vector <std::vector <std::vector <double> > > > const_store_the_species;
	
	std::vector<Matrix> const_new_matrix_list;
	std::vector<double> const_new_determinant_list;
	std::vector<double> const_new_sign_list;
};




// init
template <class inter> void update_pbpimc_open<inter>::init( bool new_debug, config_ensemble* new_ensemble )
{
	
	std::cout << "Universal(PB-PIMC)_open_init\n";
	
	ensemble = new_ensemble;
	debug = new_debug;
	
	int n_species = (*ensemble).n_species;

	
	for(int iSpecies=0;iSpecies<n_species;iSpecies++) // initialize a toolbox for each species:
	{
		worm_configuration* cfg = &(*new_ensemble).species_config[ iSpecies ];

		// Update the config pointer from the selection_toolbox
		selection_toolbox< inter > tmp_selection;
		tmp_selection.init( cfg );
		selection_tools.push_back( tmp_selection );
		

		// Update the parameters pointer from the sampling_toolbox
		sampling_toolbox< inter > tmp_sampling;
		tmp_sampling.init( &(*cfg).params );
		sampling_tools.push_back( tmp_sampling );
		

		// Update the config pointer from the inter_tools
		interaction_toolbox< inter > tmp_interaction;
		tmp_interaction.init( cfg );
		inter_tools.push_back( tmp_interaction );
		

		// Update the config pointer from the change_config
		change_config tmp_update;
		tmp_update.init( cfg );
		update.push_back( tmp_update );
		

		// Update the config pointer from the diffusion_toolbox
		diffusion_toolbox< inter > tmp_diffusion;
		tmp_diffusion.init( cfg );
		diffusion_tools.push_back( tmp_diffusion );
		

		// Update the parameters pointer from the interaction class:
		inter tmp_inter;
		tmp_inter.init( &(*cfg).params );
		my_interaction.push_back( tmp_inter );
		

		// Update the config pointer from the force_toolbox
		force_toolbox< inter > tmp_force;
		tmp_force.init( cfg );
		force_tools.push_back( tmp_force );
	
	} // end loop iSpecies
	
	
	
	// Create a constant store_the_force structure with 3*P slots for N particles and dim dimensions
	std::vector<double>dims( (*ensemble).params.dim, 0.0 );
	
	std::vector<std::vector <double> >slice_force( (*ensemble).N_tot, dims );
	const_store_the_force.assign( (*ensemble).params.n_bead*3, slice_force );
	
	
	// Create constant structures for the diffusion parts
	const_new_matrix_list.assign( (*ensemble).params.n_bead*3, (*ensemble).species_config[0].matrix_list[0] );
	const_new_determinant_list.assign( (*ensemble).params.n_bead*3, 1.0 );
	const_new_sign_list.assign( (*ensemble).params.n_bead*3, 1.0 );
	

	std::vector<std::vector<std::vector <double> > > slice_part;
	for(int iSpecies=0;iSpecies<(*ensemble).n_species;iSpecies++)
	{
		std::vector<std::vector <double> > species_part( (*ensemble).species_config[iSpecies].params.N, dims );
		slice_part.push_back( species_part );
	}
		
	const_store_the_species.assign( (*ensemble).params.n_bead*3, slice_part );
}






// Execute the Monte Carlo update:
template <class inter> int update_pbpimc_open<inter>::execute( int m_max, double factor )
{
	
	// First randomly select a particle species:
	int my_species = ensemble->n_species * rdm();
	
	// Create a pointer to the config to be opened:
	worm_configuration* config = &(*ensemble).species_config[ my_species ];
	
	if(debug) std::cout << "--PB-PIMC Open\n";
	
	// obtain the imaginary time step:
	double epsilon = config->params.epsilon;
	
	// Select the timeslice of new head:
	int start_time = rdm()*config->params.n_bead;

	// Select particle number of new head:
	int start_number = rdm()*config->params.N;
	
	if(debug) std::cout << "start_time: " << start_time << "\t start_number: " << start_number << "\n";
	
	// Define pointers to blist and nlist of the new head:
	std::vector<std::vector <int> >* new_head_blist;
	std::vector<int>* new_head_nlist;
	
	// Randomly select the kind of slice for the new head:
	int new_head_kind = selection_tools[ my_species ].random_slice_type( &new_head_blist, &new_head_nlist );
	
	// Obtain particle id of new head:
	int start_id = (*new_head_blist)[start_time][start_number];
	
	int new_tail_kind;
	
	// Select number of total links to be erased: (at least erase one bead, i.e., two links)
	int m = rdm()*m_max+2;

	
	if(debug) // verbose:
	{
		std::cout << "Species to be opened: " << my_species << "\n";
		std::cout << "OPEN, m: " << m << "\tnew_head_kind: " << new_head_kind << "\n";
	}

	
	// ########################################################################################
	// ########### Select m beads #############################################################
	// ########################################################################################
	// ->  m-1 of them are deleted and the last one is the new tail
	
	std::vector<Bead>new_beads( 1, config->beads[start_id] );
	std::vector<int>id_list( 1, start_id );
	std::vector<int>number_list( 1, start_number );

	
	double selection_prob = 1.0; // the product of all the normalizations, for the acceptance ratio
	
	int target_time = start_time; // initialize time counter
	int cnt = new_head_kind; // initialize slice kind counter
	
	for(int i=0;i<m;i++) // loop over the new artificial trajectory
	{
		if( cnt == 3 ) cnt = 0;
		cnt++;

		std::vector<int>* blist_target; // pointer to target beadlist
		Matrix *m; // pointer to the corresponding diffusion matrix
		int next;
		
		double scale;
		if(cnt == 1)
		{ // target is ancilla slice A
			blist_target = &(*config).beadlist_A[target_time];
			scale = config->params.t1;
			new_tail_kind = 1;
			
			m = &(*config).matrix_list[target_time];
			next = config->next_id_list_A[target_time];
		}
		else if(cnt == 2)
		{ // target is ancilla slice B
			blist_target = &(*config).beadlist_B[target_time];
			scale = config->params.t1;
			new_tail_kind = 2;
			
			m = &(*config).matrix_list_A[target_time];
			next = config->next_id_list_B[target_time];
		}
		else
		{ // target is main slice
			m = &(*config).matrix_list_B[target_time];
			
			
			target_time++;
			if( target_time == config->params.n_bead ) target_time = 0;
			
			blist_target = &(*config).beadlist[target_time];
			scale = 2.0*config->params.t0;
			new_tail_kind = 3;

			next = config->next_id_list[target_time];
		}
		
		// Select a new bead, according to the diffusion matrix elements:
		double p_i; // temporary normalization
		int select_number;
		int select_id = selection_tools[ my_species ].PBC_select_bead_from_blist( &new_beads[i], blist_target, next, scale, &p_i, &select_number, -1 ); // setting the last argument "special" should not matter because we do not yet have head or tail in the system
		selection_prob *= p_i;
		
		
		new_beads.push_back( config->beads[select_id] );
		number_list.push_back( select_number );
		id_list.push_back( select_id );
	}
	

	// length of the artificial trajectory (in imaginary time):
	double tau_diff =  new_beads[m].get_real_time() - new_beads[0].get_real_time();
	if(tau_diff < 0.0) tau_diff += config->params.beta;
	

	// Obtain the reverse sampling probability:
	double backward_sampling_prob; // compute the product of sampling probs on-the-fly. bead-by-bead atm not implemented, but overflow is checked at the end!
	
	if( ensemble->params.system_type < ensemble->params.n_traps ) // sampling/ prob for a trap
	{
		backward_sampling_prob = sampling_tools[ my_species ].trap_backward_connect( &new_beads, m-1 );
	}
	else // sampling/ prob for PBC
	{
		backward_sampling_prob = sampling_tools[ my_species ].PBC_backward_connect( &new_beads, m-1 );
	}
	
	if(debug) // verbose:
	{
		std::cout << "selection_prob: " << selection_prob << "\t backward_sampling_prob: " << backward_sampling_prob << "\n";
		std::cout << "tau_diff: " << tau_diff << "\n";
	}
	
	
	// ########################################################################################
	// ########### Calculate the change in the energy and the forces ##########################
	// ########################################################################################
	
	double delta_f_sq = 0.0;
	double old_energy = 0.0;
	
	cnt = new_head_kind; // initialize the slice kind counter
	
	for(int i=0;i<m-1;i++) // loop over all changed slices:
	{
		if(cnt==3) cnt = 0;
		cnt++;
		
		if( cnt == 1 )
		{ // ancilla slice A
			double ftmp = force_tools[ my_species ].ensemble_handle_old( ensemble, my_species, 1, &(*config).beadlist_A, id_list[i+1], &const_store_the_force[i], &(*config).next_id_list_A, &const_store_the_species[i] );
			delta_f_sq += (1.0 - 2.0*config->params.a1)*ftmp;

			old_energy += config->params.v2 * inter_tools[ my_species ].ensemble_old_interaction( ensemble, id_list[i+1], my_species, 1, &(*config).beadlist_A[new_beads[i+1].get_time()], config->next_id_list_A[new_beads[i+1].get_time()] );
		}
		else if( cnt == 2 )
		{ // ancilla slice B
			double ftmp = force_tools[ my_species ].ensemble_handle_old( ensemble, my_species, 2, &(*config).beadlist_B, id_list[i+1], &const_store_the_force[i], &(*config).next_id_list_B, &const_store_the_species[i] );
			delta_f_sq += config->params.a1*ftmp;

			old_energy += config->params.v1 * inter_tools[ my_species ].ensemble_old_interaction( ensemble, id_list[i+1], my_species, 2, &(*config).beadlist_B[new_beads[i+1].get_time()], config->next_id_list_B[new_beads[i+1].get_time()] );
		}
		else
		{ // main slice
			double ftmp = force_tools[ my_species ].ensemble_handle_old( ensemble, my_species, 3, &(*config).beadlist, id_list[i+1], &const_store_the_force[i], &(*config).next_id_list, &const_store_the_species[i] );
			delta_f_sq += config->params.a1*ftmp;

			old_energy += config->params.v1 * inter_tools[ my_species ].ensemble_old_interaction( ensemble, id_list[i+1], my_species, 3, &(*config).beadlist[new_beads[i+1].get_time()], config->next_id_list[new_beads[i+1].get_time()] );
		}
	} // end loop over all affected slices
	
	
	// slices of new head and tail require special attention:
	// obtain pointers to the blists, and obtain the scale- and next-information:
	std::vector<int> *head_beadlist, *tail_beadlist;
	int head_next, tail_next;
	double head_scale, tail_scale;
	
	int head_time = new_beads[0].get_time();
	int tail_time = new_beads[m].get_time();
	
	if(new_head_kind==1)
	{ // ancilla slice A
		head_beadlist = &(*config).beadlist_A[head_time];
		head_next = config->next_id_list_A[head_time];
		head_scale = config->params.v2;
	}
	else if(new_head_kind==2)
	{ // ancilla slice B
		head_beadlist = &(*config).beadlist_B[head_time];
		head_next = config->next_id_list_B[head_time];
		head_scale = config->params.v1;
	}
	else
	{ // main slice 
		head_beadlist = &(*config).beadlist[head_time];
		head_next = config->next_id_list[head_time];
		head_scale = config->params.v1;
	}
	
	
	if(new_tail_kind==1)
	{ // ancilla slice A
		tail_beadlist = &(*config).beadlist_A[tail_time];
		tail_next = config->next_id_list_A[tail_time];
		tail_scale = config->params.v2;
	}
	else if(new_tail_kind==2)
	{ // ancilla slice B
		tail_beadlist = &(*config).beadlist_B[tail_time];
		tail_next = config->next_id_list_B[tail_time];
		tail_scale = config->params.v1;
	}
	else
	{ // main slice
		tail_beadlist = &(*config).beadlist[tail_time];
		tail_next = config->next_id_list[tail_time];
		tail_scale = config->params.v1;
	}
	
	
	// energy change due to new head:
	double half_head = 0.50 * head_scale * inter_tools[ my_species ].ensemble_old_interaction( ensemble, id_list[0], my_species, new_head_kind, head_beadlist, head_next );
	old_energy +=  half_head;
	
	// energy change due to new tail
	double half_tail = 0.50 * tail_scale * inter_tools[ my_species ].ensemble_old_interaction( ensemble, id_list[m], my_species, new_tail_kind, tail_beadlist, tail_next );
	old_energy +=  half_tail;
	
	if(debug) std::cout << "-----half_head: " << half_head << "\t half_tail: " << half_tail << "\t sum: " << half_head+half_tail << "\n";

	
	// ########################################################################################
	// ########### Calculate the change in the diffusion matrices, etc. #######################
	// ########################################################################################
	
	double new_sign = config->my_sign; // initialize the sign of the new config.
	double determinant_ratio = 1.0; 
	
	cnt = new_head_kind; // initialize the slice kind counter
	for(int i=0;i<m;i++)
	{
		int ktime = config->beads[id_list[i]].get_time(); // time slice of the transition
		Matrix *mp; // pointer to the diffusion matrix
		
		if( cnt == 1 )
		{ // starting from ancilla slice A
			mp = &(*config).matrix_list_A[ktime];
		}
		else if( cnt == 2 )
		{ // starting from ancilla slice B
			mp = &(*config).matrix_list_B[ktime];
		}
		else
		{ // starting from main slice
			mp = &(*config).matrix_list[ktime];
		}
	
		
		if(i != m-1) // both a start and an end bead have been removed:
		{
			diffusion_tools[ my_species ].delete_matrix( number_list[i], number_list[1+i], mp, &const_new_matrix_list[i] );
		}
		else
		{ // only for the last transition is the target slice unaffected, because the tail can be end bead
			diffusion_tools[ my_species ].delete_matrix( number_list[i], -1, mp, &const_new_matrix_list[i] );
		}
		
		// Update the sign and determinants:
		const_new_sign_list[i] = 1.0;
		const_new_determinant_list[i] = const_new_matrix_list[i].determinant();
		if(const_new_determinant_list[i] < 0.0)
		{
			const_new_determinant_list[i] = -1.0*const_new_determinant_list[i];
			const_new_sign_list[i] = -1.0;
		}
		
		
		if( cnt == 1 )
		{ // start from ancilla slice A
			new_sign = new_sign*config->sign_list_A[ktime]*const_new_sign_list[i];
			determinant_ratio = determinant_ratio*const_new_determinant_list[i]/config->determinant_list_A[ktime];
		}
		else if( cnt == 2 )
		{ // start from ancilla slice B
			new_sign = new_sign*config->sign_list_B[ktime]*const_new_sign_list[i];
			determinant_ratio = determinant_ratio*const_new_determinant_list[i]/config->determinant_list_B[ktime];
		}
		else
		{ // start from main slice
			cnt = 0;
			new_sign = new_sign*config->sign_list[ktime]*const_new_sign_list[i];
			determinant_ratio = determinant_ratio*const_new_determinant_list[i]/config->determinant_list[ktime];
		}
		
		cnt++;
	} // end loop over all affected slices
	
	
	// Obtain random number to decide acceptance: 
	double choice = rdm();

	// Obtain new exp. factor:
	double new_exp = exp( epsilon* ( old_energy + epsilon*epsilon*config->params.u0*delta_f_sq ) - tau_diff*config->params.mu );
	
	// Detailed balance factor from the selection of start bead and length of 'worm'
	double add_fac = 3.0*double( config->params.N*m_max*config->params.n_bead );
	
	
	if( determinant_ratio < 0.0 ) // Determinants stored in the configuration must not be negative:
	{
		std::cout << "Encountered negative determinant_ratio = " << determinant_ratio << ", stopping program!\n";
		exit(1);
	}
	
	// Calculate the acceptance ratio:
	double ratio = backward_sampling_prob * determinant_ratio * new_exp * add_fac * double( ensemble->n_species ) * factor / selection_prob;
	
	
	if( debug ) // verbose:
	{
		std::cout << "new_exp: " << new_exp << "\t add_fac: " << add_fac << "\t determinant_ratio: " << determinant_ratio << "\t factor: " << factor << "\n";
		std::cout << "old_energy: " << old_energy << "\t delta_f_sq: " << delta_f_sq << "\n";
		std::cout << "ratio: " << ratio << "\t choice: " << choice << "\n";
	}
	
	// Check for possible overflows, because bead-by-bead is NOT implemented, atm. However, of no consequence for any practical purposes!
	if( isnana(determinant_ratio) || isnana(backward_sampling_prob) || isnana(selection_prob) || isnana(factor) )
	{
		std::cout << "INFin Open; determinant_ratio: " << determinant_ratio << "\t selection_prob: " << selection_prob << "\t backward_sampling_prob: " << backward_sampling_prob << "\t factor: " << factor << "\n";
		int cin;
		std::cin >> cin;
	}
	

	if( choice <= ratio ) // The update is accepted:
	{
		
		ensemble->diag = false; // we are now in the G_sector
		ensemble->G_species = my_species; // update the G_species
		
		
		// Updated forces, interaction, etc:
		config->my_sign = new_sign;
		ensemble->total_force_sq = ensemble->total_force_sq - delta_f_sq;
		ensemble->total_energy = ensemble->total_energy - old_energy;
		
		
		// Update the m kinetic matrices etc:
		int cnt = new_head_kind; // initialize the slice kind counter
		for(int i=0;i<m;i++)
		{
			int ktime = config->beads[ id_list[i] ].get_time();
			
			if( cnt == 1 )
			{ // start from ancilla slice A
				config->matrix_list_A[ktime].copy(&const_new_matrix_list[i]);
				config->determinant_list_A[ktime] = const_new_determinant_list[i];
				config->sign_list_A[ktime] = const_new_sign_list[i];
			}
			else if( cnt == 2 )
			{ // start from ancilla slice B
				config->matrix_list_B[ktime].copy(&const_new_matrix_list[i]);
				config->determinant_list_B[ktime] = const_new_determinant_list[i];
				config->sign_list_B[ktime] = const_new_sign_list[i];
			}
			else
			{ // start from main slice
				config->matrix_list[ktime].copy(&const_new_matrix_list[i]);
				config->determinant_list[ktime] = const_new_determinant_list[i];
				config->sign_list[ktime] = const_new_sign_list[i];
				cnt = 0;
			}
				
			cnt++; // update the slice-kind counter
		} // end loop over m slices
		
		
		
		
		// Update data structure on m-1 slices, where beads have been removed:
		cnt = new_head_kind; // initialize the slice kind counter
		for(int i=0;i<m-1;i++)
		{
			if(cnt == 3) cnt = 0;
			cnt++;
			
			int ktime = config->beads[ id_list[i+1] ].get_time(); // Current time slice
			int update_id = id_list[i+1]; // ID of the removed bead on this slice
			
			// Update the forces of all beads and next_ids on the current time slice:
			if(cnt == 1)
			{ // ancilla slice A
				update[ my_species ].ensemble_update_forces( ensemble, my_species, ktime, 1, &(*config).beadlist_A[ktime], update_id, update_id, &const_store_the_force[i], &const_store_the_species[i] );
				config->next_id_list_A[ktime] = update_id;

			}
			else if(cnt == 2)
			{ // ancilla slice B
				update[ my_species ].ensemble_update_forces( ensemble, my_species, ktime, 2, &(*config).beadlist_B[ktime], update_id, update_id, &const_store_the_force[i], &const_store_the_species[i] );
				config->next_id_list_B[ktime] = update_id;
			}
			else
			{ // main slice;
				update[ my_species ].ensemble_update_forces( ensemble, my_species, ktime, 3, &(*config).beadlist[ktime], update_id, update_id, &const_store_the_force[i], &const_store_the_species[i] );
				config->next_id_list[ktime] = update_id;
			}
			
		} // end loop over m-1 removed beads
		
		
		// Update head, tail, etc:
		
		config->head_kind = new_head_kind;
		config->tail_kind = new_tail_kind;
		
		config->tail_id = id_list[m];
		config->head_id = id_list[0];
		
		config->diag=0;
		
		return 1;
	}
	else // The update has been rejected!
	{
		return 0;
	}
}


