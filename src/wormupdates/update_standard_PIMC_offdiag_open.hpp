/*
 *  Contains the standard PIMC update offdiag_open (combined open+advance)
 */



template <class inter> class update_standard_PIMC_offdiag_open
{
public:
	
	void init( bool new_debug, config_ensemble* new_config_ensemble );
	int execute( int m_max, double c_bar, double p_offdiag_open, double p_offdiag_closez ); // execute the update
	
	
private:
	
	// Variables of the update:
	bool debug; // Execute Update in Debug mode?
	
	config_ensemble* ensemble; // pointer to the ensemble of particle species
	

	// all information about what kind of system we have:
	inter my_interaction;
	
	// Utilize the appropriate toolboxes:
	interaction_toolbox<inter> inter_tools;
	sampling_toolbox<inter> sampling_tools;
	pair_action_toolbox<inter> pair_tools;
};



// init
template <class inter> void update_standard_PIMC_offdiag_open<inter>::init( bool new_debug, config_ensemble* new_config_ensemble )
{
	std::cout << "initialize the standard-PIMC update Wriggle (ensemble)\n";
	
	ensemble = new_config_ensemble;
	debug = new_debug;
	
	my_interaction.init( &(*ensemble).params );

	sampling_tools.init( &(*ensemble).params );
	
	pair_tools.init( &(*ensemble).params );

	std::cout << "initialization successful!\n";
}







// Execute the Monte Carlo update:
template <class inter> int update_standard_PIMC_offdiag_open<inter>::execute( int m_max, double c_bar, double p_offdiag_open, double p_offdiag_close )
{
	if( debug ) std::cout << "************* OFFDIAG_open *****************\n";
	
	/*
	// Species to be updated is the momentum-species
	int my_species = ensemble->params.momentum_species;
	*/
	
	// We must give the possibility to update any species (sampling of exchange for the other species!)
	int N_species = ensemble->species_config.size();
	if( N_species > 2 ) N_species = 2; // TBD right now, we do not allow possibility to open the ions!
	
	
	int my_species = rdm()*N_species;
	
	if( debug ) std::cout << "my_species: " << my_species << "\n";
		
	// obtain a pointer to the worm_config to be updated:
	worm_configuration* config = &(*ensemble).species_config[ my_species ];
	
	if( config->params.n_bead <= 2 ) return 0; // Reject the update if there are too few slices
	
	// Initialize the interaction toolbox with the selected particle species config:
	inter_tools.init( &(*ensemble).species_config[ my_species ] );
	
	
	
	// Select the number of intermediate beads (thus, excluding the head) to be changed:
	int m = m_max; // 1+rdm()*m_max;
	
	if( debug ) std::cout << "m: " << m << "\t m_max: " << m_max << "\n";
	
	
	// Randomly choose a time slice for the new head/tail:
	int tail_slice = rdm()*config->params.n_bead;
	
	// Randomly choose a bead on this time-slice:
	int number = rdm()*config->beadlist[tail_slice].size();
	int tail_id = config->beadlist[tail_slice][number];
	

	
	int current_id = tail_id;

	// Go m+1 steps backward to determine the "fixed bead"
	for(int i=0;i<1+m;i++)
	{
		current_id = config->beads[ current_id ].get_prev_id();
	}
	
	int fixed_id = current_id; // ID of the fixed bead
	
	if( debug ) std::cout << "fixed_id: " << fixed_id << "\t tail_slice: " << config->beads[tail_id].get_time() << "\t fixed_time: " << config->beads[fixed_id].get_time() << "\n";
	
	// Create a list of new beads:
	std::vector<Bead> new_beads( 1, config->beads[ fixed_id ] );
	
	// obtain the next m+1 other beads:
	for(int i=0;i<1+m;i++)
	{
		int my_id = new_beads[ i ].get_next_id();		
		new_beads.push_back( config->beads[ my_id ] );
		
	} // end loop i to 1+m
	

	
	
	
	
	
	// Compute the probability to sample this particular piece of trajectory (for the reverse update):
	
	double old_diffusion_to_sampling_ratio;
	if( config->params.system_type < config->params.n_traps )
	{
 		old_diffusion_to_sampling_ratio = sampling_tools.reverse_wriggle_trap_connect( &new_beads, m ); // For the traps 
	}
	else 
	{
		old_diffusion_to_sampling_ratio = sampling_tools.reverse_wriggle_PBC_connect( &new_beads, m ); // For PBC systems
	}
	
	
	
	
	// Sample a new head:
	double new_head_prob = sampling_tools.sample_new_head( &(*config).beads[fixed_id], &new_beads[m+1] );
	
	if( debug ) std::cout << "new_head_prob: " << new_head_prob << "\n";
	
	

	
	// Re-sample the m beads in between and simultaneously obtain the ratio of new_diffusion-to-new_sampling_prob
	double new_diffusion_to_sampling_ratio; // computed bead-by-bead (no overflow!)
	if( config->params.system_type < config->params.n_traps )
	{
 		new_diffusion_to_sampling_ratio = sampling_tools.wriggle_trap_connect( &new_beads, m ); // For the traps 
	}
	else 
	{
		new_diffusion_to_sampling_ratio = sampling_tools.wriggle_PBC_connect( &new_beads, m ); // For PBC systems
	}

	
	
	if(debug) std::cout << "new_diffusion_to_sampling_ratio: " << new_diffusion_to_sampling_ratio << "\n";
	
	

	
	
	
	
	// Obtain the change in the potential energy for the m intermediate slices. The new head is treated separately!
	double delta_u = 0.0; // OLD - NEW
	
	// Compute the diffusion-ratio [NEW/OLD], for the updating of the diffusion_product 
	double diffusion_ratio = new_beads[0].get_diffusion_element() / config->beads[fixed_id].get_diffusion_element();
	
	// Compute the change in the pair-action part on the m intermediate slices. The head is treated separately!
	double delta_pair = 0.0;
	double delta_pair_derivative = 0.0;
	double delta_m_e_pair_derivative = 0.0;
	double delta_m_p_pair_derivative = 0.0;
	
	// Compute the change in the external potential action
	double delta_ext_pot_action = 0.0;
	double delta_ext_pot_action_derivative = 0.0;
	

	
	
	
	
	
	
		
	// ##########################################################################################################################
	// ### Start::OpenMP parallelization part
	
	
	
	std::vector<double> OpenMP_delta_u( m, 0.0 );
	std::vector<double> OpenMP_delta_pair( m, 0.0 );
	std::vector<double> OpenMP_delta_pair_derivative( m, 0.0 );
	std::vector<double> OpenMP_delta_m_e_pair_derivative( m, 0.0 );
	std::vector<double> OpenMP_delta_m_p_pair_derivative( m, 0.0 );
	std::vector<double> OpenMP_delta_ext_pot_action( m, 0.0 );
	std::vector<double> OpenMP_delta_ext_pot_action_derivative( m, 0.0 );
	
	
	
	
	
	
	
	
	for(int i=1;i<1+m;i++)
	{
		int i_id = new_beads[ i-1 ].get_next_id(); // ID of the current bead
		
		double old_diffusion_element = config->beads[i_id].get_diffusion_element();
		double new_diffusion_element = new_beads[i].get_diffusion_element();
		
		diffusion_ratio *= ( new_diffusion_element / old_diffusion_element );
		
	}
		
		
		
		
		
		
	#pragma omp parallel 
	{
		#pragma omp for
		for(int i=1;i<1+m;i++)
		{	
			int i_id = new_beads[ i-1 ].get_next_id(); // ID of the current bead
			int prev_id = config->beads[i_id].get_prev_id();
			
			std::vector<double> dA;
			dA = pair_tools.change_pair_action( prev_id, i_id, &new_beads[i-1], &new_beads[i], my_species, ensemble );
			// ### OpenMP:: delta_pair += dA[0];
			// ### OpenMP:: delta_pair_derivative += dA[1];
			// ### OpenMP:: delta_m_e_pair_derivative += dA[2];
			// ### OpenMP:: delta_m_p_pair_derivative += dA[3];
			
			OpenMP_delta_pair[ i-1 ] += dA[0];
			OpenMP_delta_pair_derivative[ i-1 ] += dA[1];
			OpenMP_delta_m_e_pair_derivative[ i-1 ] += dA[2];
			OpenMP_delta_m_p_pair_derivative[ i-1 ] += dA[3];
			
			
			
			std::vector<double> old_ext_pot_action, new_ext_pot_action;
			new_ext_pot_action = my_interaction.ext_pot_action( &new_beads[ i-1 ], &new_beads[ i ] );
			old_ext_pot_action = my_interaction.ext_pot_action( &(*config).beads[ prev_id ], &(*config).beads[ i_id ] );
			
			// ### OpenMP:: delta_ext_pot_action += ( old_ext_pot_action[0] - new_ext_pot_action[0] );
			// ### OpenMP:: delta_ext_pot_action_derivative += ( old_ext_pot_action[1] - new_ext_pot_action[1] );
			
			OpenMP_delta_ext_pot_action[ i-1 ] += ( old_ext_pot_action[0] - new_ext_pot_action[0] );
			OpenMP_delta_ext_pot_action_derivative[ i-1 ] += ( old_ext_pot_action[1] - new_ext_pot_action[1] );
			
			
			
			
			
			// Change in the potential energy within my_species, old - new:
			// ### OpenMP:: delta_u += inter_tools.change_interaction( &(*config).beadlist[ new_beads[ i ].get_time() ], &new_beads[ i ], i_id , -1 );
			OpenMP_delta_u[ i-1 ] += inter_tools.change_interaction( &(*config).beadlist[ new_beads[ i ].get_time() ], &new_beads[ i ], i_id , -1 );
			
			
			
			// Change in the potential energy due to all other species:
			for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
			{
				if( iSpecies != my_species )
				{
					double old_sp = inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &(*config).beads[ i_id ] );
					double new_sp = inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ i ] );
					
					// ### OpenMP:: delta_u += ( old_sp - new_sp );
					OpenMP_delta_u[ i-1 ] += ( old_sp - new_sp );
				}
			}
			
			
			
			
		} // end loop i to 1+m
		
	}
	
	
	
	// After OMP post-processing loop!
	for(int i=1;i<1+m;i++)
	{
	
		delta_u += OpenMP_delta_u[ i-1 ];
		delta_pair += OpenMP_delta_pair[ i-1 ];
		delta_pair_derivative += OpenMP_delta_pair_derivative[ i-1 ];
		delta_m_e_pair_derivative += OpenMP_delta_m_e_pair_derivative[ i-1 ];
		delta_m_p_pair_derivative += OpenMP_delta_m_p_pair_derivative[ i-1 ];
		delta_ext_pot_action += OpenMP_delta_ext_pot_action[ i-1 ];
		delta_ext_pot_action_derivative += OpenMP_delta_ext_pot_action_derivative[ i-1 ];
	}
	
	
	
	// ### End::OpenMP parallelization part
	// ##########################################################################################################################

	
	
	
	
	
	
	
	
	
	
	
	if( debug ) std::cout << "delta_u [no-head]: " << delta_u << "\n";
	
	
	// Obtain the change in the action part due to connection involving the new head
	std::vector<double> dA,old_ext_pot_action,new_ext_pot_action;

	dA = pair_tools.change_pair_action( config->beads[ tail_id ].get_prev_id(), tail_id, &new_beads[ new_beads.size()-2 ], &new_beads[ new_beads.size()-1 ], my_species, ensemble );
	delta_pair += dA[0];
	delta_pair_derivative += dA[1];
	delta_m_e_pair_derivative += dA[2];
	delta_m_p_pair_derivative += dA[3];
	
	
	new_ext_pot_action = my_interaction.ext_pot_action( &new_beads[ new_beads.size()-2 ], &new_beads[ new_beads.size()-1 ] );
	old_ext_pot_action = my_interaction.ext_pot_action( &(*config).beads[ config->beads[ tail_id ].get_prev_id() ], &(*config).beads[ tail_id ] );
	
	delta_ext_pot_action += ( old_ext_pot_action[0] - new_ext_pot_action[0] );
	delta_ext_pot_action_derivative += ( old_ext_pot_action[1] - new_ext_pot_action[1] );
	
	
	// Obtain the change in the interaction due to the change in the head
	double old_tail_u = my_interaction.ext_pot( &(*config).beads[ tail_id ] );
	double new_head_u = my_interaction.ext_pot( &new_beads[ 1+m] );
	
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++) // interaction of the head with all other species (unproblematic)
	{
		if( iSpecies != my_species )
		{
			old_tail_u += inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &(*config).beads[ tail_id ] );
			new_head_u += inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ 1+m ] );
		}
	}
	
	// Now let us go over all particles from the same species by hand (no interaction between head and tail!)
	int head_time = config->beads[tail_id].get_time();
	for(int i=0;i<config->beadlist[head_time].size();i++)
	{
		int i_ID = config->beadlist[head_time][i];
		if( i_ID != tail_id )
		{
			old_tail_u += my_interaction.pair_interaction( &(*config).beads[tail_id], &(*config).beads[i_ID] );
			new_head_u += my_interaction.pair_interaction( &new_beads[1+m], &(*config).beads[i_ID] ); 
		}
	}
	
	if( debug ) std::cout << "old_tail_u: " << old_tail_u << "\t new_head_u: " << new_head_u << "\n";
	

	delta_u += 0.50 * ( old_tail_u - new_head_u ); // The interaction with the head only counts half
	
	
	
	
	// Obtain the acceptance ratio:
	double ratio = exp( config->params.epsilon*delta_u ) * exp( config->params.epsilon*delta_pair ) * exp( config->params.epsilon*delta_ext_pot_action ) * double(config->beadlist[0].size()*config->params.n_bead*N_species) * c_bar * new_diffusion_to_sampling_ratio * p_offdiag_close / ( p_offdiag_open * new_head_prob  * old_diffusion_to_sampling_ratio );
	
	

	// Obtain a random number to decide acceptance:
	double choice = rdm();
	
	if(debug) // verbose, check for overflow if !PBC_detailed_balance:
	{
		std::cout << "+++++++++++++++ standard PIMC offdiag_open decision ++++++++++++++++\n";
		std::cout << "delta_u: " << delta_u << "\t head_id: " << config->head_id << "\t tail_id: " << config->tail_id << "\t diag: " << config->diag << std::endl;
		std::cout << "ratio: " << ratio << "\t choice: " << choice << "\n";
		std::cout << "delta_pair: " << delta_pair << "\t delta_pair_derivative: " << delta_pair_derivative << "\n";
		std::cout << "delta_m_e_pair_derivative: " << delta_m_e_pair_derivative << "\t delta_m_p_pair_derivative: " << delta_m_p_pair_derivative << "\n";
	
		if( isnana( ratio ) ) // || !(choice <= ratio) )
		{
			std::cout << "isnana in ratio\n";
			int z;
			std::cin >> z;
		}
	}
	
	if( choice <= ratio ) // The update has been accepted:
	{
		ensemble->total_diffusion_product *= diffusion_ratio;
		
		// book keeping of the total potential energy:
		ensemble->total_energy -= delta_u;
		
		ensemble->total_pair_action -= delta_pair;
		ensemble->total_pair_derivative -= delta_pair_derivative;
		
		ensemble->total_m_e_pair_derivative -= delta_m_e_pair_derivative;
		ensemble->total_m_p_pair_derivative -= delta_m_p_pair_derivative;
		
		ensemble->total_ext_pot_action -= delta_ext_pot_action;
		ensemble->total_ext_pot_action_derivative -= delta_ext_pot_action_derivative;
		
		ensemble->diag = 0;
		ensemble->G_species = my_species;
		
		config->tail_id = tail_id;
		config->diag = 0;
		
		// Replace the m+1 affected beads (fixed_bead because of the changed diffusion element, not the tail. Head is treated extra!)
		int RUN = fixed_id;
		for(int i=0;i<1+m;i++)
		{
			config->beads[ RUN ].copy( &new_beads[ i ] ); // replace old bead by the new one
			RUN = config->beads[RUN].get_next_id();
		} 
		
		// Finally, we have to incorporate the new head:
		int prev_id = config->beads[tail_id].get_prev_id();
		int head_id = config->get_new_bead();
		config->beads[head_id].copy( &new_beads[m+1] );
		config->beads[head_id].set_next_id(-1);
		config->beads[tail_id].set_prev_id(-1);
		config->beads[prev_id].set_next_id( head_id );
		
		
		config->head_id = head_id;
		config->beadlist[tail_slice].push_back(head_id);
		
		
		
		
		if( debug ) // Debug option: Write particle coordinates and bead structure to the disk
		{
			std::cout << "-+my_species: " << my_species << "\n";
			config->standard_PIMC_print_paths( "offdiag_open_paths.dat" );
			config->standard_PIMC_print_beadlist( "offdiag_open_beadlist.dat" );
		}

		return 1;
	}
	else // The update has been rejected
	{
		return 0;
	}

}




























