/*
 *  Contains the standard PIMC update Close (G-sector)
 */



template <class inter> class update_standard_PIMC_close
{
public:
	
	void init( bool new_debug, config_ensemble* new_ensemble );
	int execute( int m_max_open, double c_bar, double p_open, double p_close ); // execute the update, returns acceptance
	
	
private:
	
	// Variables of the update:
	bool debug;
	
	config_ensemble* ensemble; // Pointer to ensemble of particle species
	

	// all information about what kind of system we have:
	inter my_interaction;
	
	// Utilize the appropriate toolboxes:
	interaction_toolbox<inter> inter_tools;
	sampling_toolbox<inter> sampling_tools;
	pair_action_toolbox<inter> pair_tools;
};



// init
template <class inter> void update_standard_PIMC_close<inter>::init( bool new_debug, config_ensemble* new_ensemble )
{
	std::cout << "initialize the standard-PIMC update CLOSE (ensemble)\n";
	
	ensemble = new_ensemble;
	
	debug = new_debug;
	
	my_interaction.init( &(*ensemble).params );

	sampling_tools.init( &(*ensemble).params );
	
	pair_tools.init( &(*ensemble).params );
	
	std::cout << "initialization successful!\n";
}







// Execute the Monte Carlo update:
template <class inter> int update_standard_PIMC_close<inter>::execute( int m_max_open, double c_bar, double p_open, double p_close )
{
	
	
	// obtain the configuration (species) that is in the G-sector:
	int my_species = ensemble->G_species;
	
	// obtain pointer to G-species-config:
	worm_configuration* config = &(*ensemble).species_config[ my_species ];
	
	// initialize the interaction toolbox:
	inter_tools.init( config );
	
	
	// determine the number of "missing" beads between head and tail (forward in imag. time):
	int m = config->beads[ config->tail_id ].get_time() - config->beads[ config->head_id ].get_time() - 1;
	
	if( debug ) // verbose:
	{
		std::cout << "********** std-PIMC update close (ensemble) ************\n";
		std::cout << "m: " << m << "\t head_time: " << config->beads[ config->head_id ].get_time() << "\t tail_time: " << config->beads[ config->tail_id ].get_time() << "\n";
		std::cout << "my_species: " << my_species << "\n";
		std::cout << "head_id: " << config->head_id << "\t tail_id: " << config->tail_id << "\n";
		for(int iDim=0;iDim<config->params.dim;iDim++)
		{
			std::cout << "r_head[" << iDim << "] = " << config->beads[ config->head_id ].get_coord( iDim ) << " | r_tail[" << iDim << "] = " << config->beads[ config->tail_id ].get_coord( iDim ) << "\t";
		}
		std::cout << "\n";
	}
	
	if( m == 0 ) 
	{
		if( debug ) std::cout << "no beads missing, return 0\n";
		return 0; // if no beads are missing, close will be rejected
	}
	if( m < 0 ) m = m + config->params.n_bead; // periodicity in imag. time
	
	
	// Check if the reverse move (open) is possible, otherwise reject (detailed balance):
	if( m > m_max_open )
	{
		if( debug ) std::cout << "too many beads missing, reject!\n";
		return 0;
	}
	
	
	// Create a temporary list for the new trajectory:
	std::vector<Bead>new_beads( 1, config->beads[ config->head_id ] );
	
	// Add m new beads to the list :
	for(int i=0;i<m;i++)
	{
		int time = new_beads[i].get_time() + 1;
		if( time >= config->params.n_bead ) time -= config->params.n_bead;
		
		new_beads.push_back( new_beads[i] );
		new_beads[ 1+i ].set_time( time );
		new_beads[ 1+i ].set_real_time( time*config->params.epsilon );
	}
	
	// The tail is the end (target) of the new trajectory:
	new_beads.push_back( config->beads[ config->tail_id ] );
	
	
	// Sample the 'm' beads in between according to the usual scheme:
	double prob_connect; // total sampling probability
	std::vector<double> probs_connect( m, 0.0 ); // vector containing the sampling probs for every single bead
	
	if( config->params.system_type < config->params.n_traps ) // Connect for the trap:
	{
		prob_connect = sampling_tools.trap_connect( &new_beads, m, probs_connect );
	}
	else // Connect for the PBC:
	{
		prob_connect = sampling_tools.PBC_connect( &new_beads, m, probs_connect );
	}
	
	
	// obtain the change in the interaction and in the diffusion elements:
	double delta_u = 0.0; // OLD - NEW 
	double diffusion_element_product = 1.0;
	double diffusion_sampling_ratio = 1.0;
	
	double delta_pair = 0.0; // OLD - NEW
	double delta_pair_derivative = 0.0;
	
	double delta_ext_pot_action = 0.0; // OLD - NEW 
	double delta_ext_pot_action_derivative = 0.0;
	
	
	
	for(int i=0;i<m+1;i++)
	{
		// obtain new diffusion elements (also for the old head and the connection to the old tail
		double i_element = my_interaction.rho( &new_beads[ i ], &new_beads[ i+1 ], 1.0 );
		new_beads[ i ].set_diffusion_element( i_element );
		diffusion_element_product *= i_element;
		diffusion_sampling_ratio *= i_element;
		if( i < m ) diffusion_sampling_ratio /= probs_connect[i]; // compute the ratio bead-by-bead to prevent overflow
		
		auto new_ext_pot_action = my_interaction.ext_pot_action( &new_beads[i], &new_beads[1+i] );
		delta_ext_pot_action -= new_ext_pot_action[0];
		delta_ext_pot_action_derivative -= new_ext_pot_action[1];
		
		
		int excl_0 = -1;
		int excl_1 = -1;
		if( i == 0 ) excl_0 == config->head_id; // head and tail do already exist and should not be counted double!
		if( i == m ) excl_1 == config->tail_id;
		
		std::vector<double> dA = pair_tools.add_pair_action( &new_beads[i], &new_beads[1+i], excl_0, excl_1, my_species, ensemble );
		delta_pair -= dA[0];
		delta_pair_derivative -= dA[1];
		
		
		
		if( debug ) // verbose:
		{
			std::cout << "*** i: " << i << "\t i_element: " << i_element << "\t diffusion_element_product: " << diffusion_element_product << "\n";
			std::cout << "i_time: " << new_beads[i].get_time() << "\t real: " << new_beads[i].get_real_time() << "\t";
			for(int iDim=0;iDim<config->params.dim;iDim++)
			{
				std::cout << "r[" << iDim << "] = " << new_beads[i].get_coord( iDim ) << "\t";
			}
			std::cout << "\n";
		}
		
		// obtain the new interaction, but treat head (and tail) separately:
		if( i > 0 )
		{
			// interaction within the same species (and ext_pot):
			delta_u -= inter_tools.new_interaction( &(*config).beadlist[ new_beads[ i ].get_time() ], &new_beads[ i ], -1 );
			
			// interaction with all other species:
			for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
			{
				if( iSpecies != my_species )
				{
					delta_u -= inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ i ] );
				}
			}
		}
		
	} // end loop i to m+1
	
	
	// Now treat head and tail separately, with a weight of 1/2:
	// interaction with the same species (and ext_pot):
	delta_u -= 0.50 * inter_tools.old_interaction( &(*config).beadlist[ config->beads[ config->head_id ].get_time() ], config->head_id, -1 );
	delta_u -= 0.50 * inter_tools.old_interaction( &(*config).beadlist[ config->beads[ config->tail_id ].get_time() ], config->tail_id, -1 );
	
	// interaction with all other species:
	for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
	{
		if( iSpecies != my_species )
		{
			delta_u -= 0.50 * inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ 0 ] );
			delta_u -= 0.50 * inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ 1+m ] );
		}
	}
	

	// obtain a random number to decide acceptance of the update:
	double choice = rdm();
	
	// obtain the change in the (mu)-exponent (chem. pot.):
	double delta_exponent = config->params.mu * config->params.epsilon * (m+1.0);
	
	// obtain the acceptance ratio:
	double head_tail_term = my_interaction.rho( &new_beads[ 0 ], &new_beads[ 1+m ], 1+m );
	double ratio = exp( config->params.epsilon*delta_ext_pot_action ) * exp( config->params.epsilon*delta_pair ) * exp( delta_exponent ) * exp( config->params.epsilon*delta_u ) / ( config->beadlist[ config->beads[ config->head_id ].get_time() ].size() * config->params.n_bead * m_max_open * c_bar * ensemble->species_config.size() );

	if( config->params.PBC_detailed_balance ) // If rho is a sum over Gaussians, diffusion and sampling terms do not exactly cancel!
	{
		ratio = ratio * diffusion_sampling_ratio;
	}
	else 
	{
		ratio = ratio * head_tail_term;
	}
	
	
	if( debug ) // verbose, check possible overflows in !PBC_detailed_balance:
	{
		double ratio2 = exp( config->params.epsilon*delta_ext_pot_action ) * exp( config->params.epsilon*delta_pair ) * exp( delta_exponent ) * exp( config->params.epsilon*delta_u ) * diffusion_element_product / ( config->beadlist[ config->beads[ config->head_id ].get_time() ].size() * prob_connect * config->params.n_bead * m_max_open * c_bar * ensemble->species_config.size() );

		if( fabs( ratio/ratio2-1.0) > 1e-13 )
		{
			std::cout << "Error in std_PIMC Close, ratio = " << ratio << "\t ratio2= " << ratio2 << "\n";
			std::cout << "diff: " << fabs(ratio/ratio2-1) << "\n";
			int cin;
			std::cin >> cin;
		}
		
		if( isnana( prob_connect ) || isnana( diffusion_element_product) )
		{
			std::cout << "std_close, prob_connect: " << prob_connect << "\t diffusion_element_product: " << diffusion_element_product << "\n";
			int cin;
			std::cin >> cin;
		}
	}
	
	
	// Artificial potential to favor a given target N:
	// -> Use carefully!
	if( config->params.N_potential_control )
	{
		double old_N_diff =  config->params.N - config->my_exp / ( config->params.mu * config->params.beta );
		double new_N_diff = config->params.N - ( config->my_exp + delta_exponent ) / ( config->params.mu * config->params.beta );
		double W_N_scale = -0.50 / ( config->params.sigma_target * config->params.sigma_target );
		double W_N_new = exp( W_N_scale * new_N_diff * new_N_diff );
		double W_N_old = exp( W_N_scale * old_N_diff * old_N_diff );

		if(debug) std::cout << "W_N_new = " << W_N_new << "\t W_N_old = " << W_N_old << "\t W_N_scale = " << W_N_scale << "\n";
		ratio = ratio * W_N_new / W_N_old;
	}


	// correction due to different update frequencies:
	ratio = ratio * p_open / p_close;
	
	if( debug ) // verbose:
	{
		std::cout << "ratio: " << ratio << "\t choice: " << choice << "\t c_bar: " << c_bar << "\t m_max_open: " << m_max_open << "\t m: " << m << "\t p_open: "  << p_open << "\t p_close: " << p_close << "\n";
		std::cout << "delta_exponent: " << delta_exponent << "\t diffusion_element_product: " << diffusion_element_product << "\t prob_connect: " << prob_connect << "\n";
	
		std::cout << "delta_pair: " << delta_pair << "\t delta_pair_derivative: " << delta_pair_derivative << "\n";
		std::cout << "delta_ext_pot_action: " << delta_ext_pot_action << "\t delta_ext_pot_action_derivative: " << delta_ext_pot_action_derivative << "\n";
	}
	

	if( choice <= ratio ) // The update is accepted:
	{
		// Product of diffusion elements, for debug purposes only!
		ensemble->total_diffusion_product *= diffusion_element_product;
		
		ensemble->total_energy -= delta_u; // update the total energy 
		ensemble->total_exponent += delta_exponent; // update the total exponent
		
		ensemble->total_pair_action -= delta_pair; // // update the total pair action in the system
		ensemble->total_pair_derivative -= delta_pair_derivative;
		
		ensemble->total_ext_pot_action -= delta_ext_pot_action;
		ensemble->total_ext_pot_action_derivative -= delta_ext_pot_action_derivative;
		
		config->my_exp += delta_exponent; // update the exponent of iSpecies

		// Update the head-bead:
		config->beads[ config->head_id ].copy( &new_beads[0] );
		
		// Add the m new beads to the configuration:
		int prev_id = config->head_id;
		for(int i=0;i<m;i++)
		{
			int i_id = config->get_new_bead();
			config->beadlist[ new_beads[ 1+i ].get_time() ].push_back( i_id );
			config->beads[ i_id ].copy( &new_beads[ 1+i ] );
			config->beads[ prev_id ].set_next_id( i_id );
			config->beads[ i_id ].set_prev_id( prev_id );
			
			prev_id = i_id;
		}
		
		// finally include the tail in the prev/next links:
		config->beads[ config->tail_id ].set_prev_id( prev_id );
		config->beads[ prev_id ].set_next_id( config->tail_id );
		
		// now both the ensemble of species and this particular config are diagonal:
		ensemble->diag = 1;
		config->diag = 1;
		config->head_id = -1;
		config->tail_id = -1;
		
		// Finally, update the total number of particles in the ensemble of species:
		ensemble->N_tot = 0;
		for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
		{
			ensemble->N_tot += ensemble->species_config[ iSpecies ].beadlist[ 0 ].size();
		}
		
		
		if( debug ) // Debug option: Write particle coordinates and bead structure to the disk 
		{
			config->standard_PIMC_print_paths( "close_paths.dat" );
			config->standard_PIMC_print_beadlist( "close_beadlist.dat" );
		}
		
		return 1;
	}
	else // The update is rejected
	{
		return 0;
	}

}




























