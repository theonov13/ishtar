/*
 *  Contains the standard PIMC update deform (Bisection)
 */



template <class inter> class update_standard_PIMC_deform
{
public:
	
	void init( bool new_debug, config_ensemble* new_config_ensemble );
	int execute( int m_max, bool proton ); // execute the update
	
	
private:
	
	// Variables of the update:
	bool debug; // Execute Update in Debug mode?
	
	config_ensemble* ensemble; // pointer to the ensemble of particle species
	

	// all information about what kind of system we have:
	inter my_interaction;
	
	// Utilize the appropriate toolboxes:
	interaction_toolbox<inter> inter_tools;
	sampling_toolbox<inter> sampling_tools;
	pair_action_toolbox<inter> pair_tools;
};



// init
template <class inter> void update_standard_PIMC_deform<inter>::init( bool new_debug, config_ensemble* new_config_ensemble )
{
	std::cout << "initialize the standard-PIMC update DEFORM (ensemble)\n";
	
	ensemble = new_config_ensemble;
	debug = new_debug;
	
	my_interaction.init( &(*ensemble).params );

	sampling_tools.init( &(*ensemble).params );
	
	pair_tools.init( &(*ensemble).params );

	std::cout << "initialization successful!\n";
}







// Execute the Monte Carlo update:
template <class inter> int update_standard_PIMC_deform<inter>::execute( int m_max, bool proton )
{
	
	// Randomly select the species to be updated:
	int my_species;
	
	if( proton )
	{
		my_species = 2; // update the protons!
	}
	else 
	{
		int electrons = ensemble->species_config.size();
		if( electrons > 2 ) electrons = 2;
		my_species = electrons*rdm();
	}
	
	if( debug ) std::cout << "my_species: " << my_species << "\t proton: " << proton << "\n";
		
	// obtain a pointer to the worm_config to be updated:
	worm_configuration* config = &(*ensemble).species_config[ my_species ];
	
	if( config->params.n_bead <= 2 ) return 0; // Reject the update if there are too few slices
	
	// Initialize the interaction toolbox with the selected particle speciec config:
	inter_tools.init( &(*ensemble).species_config[ my_species ] );
	
	// Select a start slice at random:
	int start_slice = rdm()* config->params.n_bead;
	
	// Select a random bead ID from this start_slice:
	if( config->beadlist[ start_slice ].size() == 0 ) return 0; // Reject the update, if there are no particles on the selected slice
	int start_id = rdm()*config->beadlist[ start_slice ].size();
	start_id = config->beadlist[ start_slice ][ start_id ];
	
	// Select the number of beads to be changed:
	int m = 1+rdm()*m_max;
	
	// Create a list of new beads:
	std::vector<Bead> new_beads( 1, config->beads[ start_id ] );
	
	// obtain the next m+1 other beads:
	for(int i=0;i<1+m;i++)
	{
		int my_id = new_beads[ i ].get_next_id();
		if( my_id == -1 ) return 0; // Reject the update, if a bead is missing (G-sector only)
		
		new_beads.push_back( config->beads[ my_id ] );
		
	} // end loop i to 1+m
	

	// Re-sample the m beads in between and simultaneously obtain the ratio of reverse and forward sampling prob:
	double sampling_ratio; // computed bead-by-bead (no overflow!)
	if( config->params.system_type < config->params.n_traps )
	{
 		sampling_ratio = sampling_tools.combined_trap_connect( &new_beads, m ); // For the traps 
	}
	else 
	{
		sampling_ratio = sampling_tools.combined_PBC_connect( &new_beads, m ); // For PBC systems
	}
	
	
	if(debug) // verbose:
	{
		std::cout << "+++++++++++++++ update_standard_PIMC_deform (ensemble) \n";
		std::cout << "m: " << m << "\t start_slice: " << start_slice << "\t start_id: " << start_id << "\n";
		std::cout << "sampling_ratio: " << sampling_ratio << "\n";
	}
	
	
	// Compute change in the diffusion elements:
	double diffusion_ratio = 1.0; // ratio of new and old diffusion_elements
	
	// Take care of the diffusion_element of i=0 separately:
	double new_start_diffusion_element = my_interaction.rho( &new_beads[0], &new_beads[1], 1.0 );
	diffusion_ratio = new_start_diffusion_element / new_beads[0].get_diffusion_element();
	new_beads[0].set_diffusion_element( new_start_diffusion_element );
	
	if(debug) std::cout << "new_start_diffusion_element: " << new_start_diffusion_element << "\n";
	
	
	
	
	
	
	
	
	
	// Obtain the change in the potential energy and in the diffusion_elements (this covers the m changed beads in between start and end)
	double delta_u = 0.0;
	
	// At the same time, compute the change in the pair action
	double delta_pair = 0.0;
	double delta_pair_derivative = 0.0;
	
	double delta_m_e_pair_derivative = 0.0;
	double delta_m_p_pair_derivative = 0.0;
	
	// And, thirdly, compute the change in the external potential action!
	double delta_ext_pot_action = 0.0;
	double delta_ext_pot_action_derivative = 0.0;
	
	
	
	
	
	
	
	
	// ##########################################################################################################################
	// ### Start::OpenMP parallelization part
	
	
	
	std::vector<double> OpenMP_delta_u( m, 0.0 );
	std::vector<double> OpenMP_delta_pair( m, 0.0 );
	std::vector<double> OpenMP_delta_pair_derivative( m, 0.0 );
	std::vector<double> OpenMP_delta_m_e_pair_derivative( m, 0.0 );
	std::vector<double> OpenMP_delta_m_p_pair_derivative( m, 0.0 );
	std::vector<double> OpenMP_delta_ext_pot_action( m, 0.0 );
	std::vector<double> OpenMP_delta_ext_pot_action_derivative( m, 0.0 );
	
	
	
	
	#pragma omp parallel 
	{
		#pragma omp for
		for(int i=1;i<1+m;i++)
		{
			int i_id = new_beads[ i-1 ].get_next_id(); // ID of the current bead
			
			// Change in the potential energy within my_species, old - new:
			// ### OpenMP:: delta_u += inter_tools.change_interaction( &(*config).beadlist[ new_beads[ i ].get_time() ], &new_beads[ i ], i_id , -1 );
			
			OpenMP_delta_u[ i-1 ] += inter_tools.change_interaction( &(*config).beadlist[ new_beads[ i ].get_time() ], &new_beads[ i ], i_id , -1 );
			

			// Change in the potential energy due to all other species:
			for(int iSpecies=0;iSpecies<ensemble->species_config.size();iSpecies++)
			{
				if( iSpecies != my_species )
				{
					double old_sp = inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &(*config).beads[ i_id ] );
					double new_sp = inter_tools.standard_PIMC_species_interaction( ensemble, iSpecies, &new_beads[ i ] );
					
					// ### OpenMP:: delta_u += ( old_sp - new_sp );
					OpenMP_delta_u[ i-1 ] += ( old_sp - new_sp );
				}
			}
			
			
			
			
			// pair action:
			int i_id_1 = config->beads[i_id].get_next_id();
			
			std::vector<double> dA;
			dA = pair_tools.change_pair_action( i_id, i_id_1, &new_beads[i], &new_beads[1+i], my_species, ensemble );
			
			// ### OpenMP:: delta_pair += dA[0];
			// ### OpenMP:: delta_pair_derivative += dA[1];
			// ### OpenMP:: delta_m_e_pair_derivative += dA[2];
			// ### OpenMP:: delta_m_p_pair_derivative += dA[3];
			
			OpenMP_delta_pair[ i-1 ] += dA[0];
			OpenMP_delta_pair_derivative[ i-1 ] += dA[1];
			OpenMP_delta_m_e_pair_derivative[ i-1 ] += dA[2];
			OpenMP_delta_m_p_pair_derivative[ i-1 ] += dA[3];
			
			
			// ext-pot action:
			
			std::vector<double> old_ext_pot_action, new_ext_pot_action;
			new_ext_pot_action = my_interaction.ext_pot_action( &new_beads[ i ], &new_beads[ 1+i ] );
			old_ext_pot_action = my_interaction.ext_pot_action( &(*config).beads[ i_id ], &(*config).beads[ i_id_1 ] );
			
			// ### OpenMP:: delta_ext_pot_action += ( old_ext_pot_action[0] - new_ext_pot_action[0] );
			// ### OpenMP:: delta_ext_pot_action_derivative += ( old_ext_pot_action[1] - new_ext_pot_action[1] );
			
			OpenMP_delta_ext_pot_action[ i-1 ] += ( old_ext_pot_action[0] - new_ext_pot_action[0] );
			OpenMP_delta_ext_pot_action_derivative[ i-1 ] += ( old_ext_pot_action[1] - new_ext_pot_action[1] );
			
			
			
		} // end loop i to 1+m
	}
	
	
	// After OMP post-processing loop!
	for(int i=1;i<1+m;i++)
	{
		// diffusion_elements:
		double new_diffusion_element = my_interaction.rho( &new_beads[i], &new_beads[1+i], 1.0 );
		
		diffusion_ratio *= new_diffusion_element / new_beads[i].get_diffusion_element(); // computed bead-by-bead!
		
		new_beads[i].set_diffusion_element( new_diffusion_element );
		
		
		delta_u += OpenMP_delta_u[ i-1 ];
		delta_pair += OpenMP_delta_pair[ i-1 ];
		delta_pair_derivative += OpenMP_delta_pair_derivative[ i-1 ];
		delta_m_e_pair_derivative += OpenMP_delta_m_e_pair_derivative[ i-1 ];
		delta_m_p_pair_derivative += OpenMP_delta_m_p_pair_derivative[ i-1 ];
		delta_ext_pot_action += OpenMP_delta_ext_pot_action[ i-1 ];
		delta_ext_pot_action_derivative += OpenMP_delta_ext_pot_action_derivative[ i-1 ];
	}
	

		
	// ### End::OpenMP parallelization part
	// ##########################################################################################################################

	
	
	
	
	
	
	
	
	// The transition from the unchanged to the first changed bead has to be treated separately for the pair action:
	std::vector<double> dA = pair_tools.change_pair_action( start_id, new_beads[0].get_next_id(), &new_beads[0], &new_beads[1], my_species, ensemble );
	
	delta_pair += dA[0];
	delta_pair_derivative += dA[1];
	delta_m_e_pair_derivative += dA[2];
	delta_m_p_pair_derivative += dA[3];
	
	// and, naturally, the same also holds for the ext_pot_action:
	std::vector<double> old_ext_pot_action, new_ext_pot_action;
	new_ext_pot_action = my_interaction.ext_pot_action( &new_beads[0], &new_beads[1] );
	old_ext_pot_action = my_interaction.ext_pot_action( &new_beads[0], &(*config).beads[ new_beads[0].get_next_id() ] );
	
	delta_ext_pot_action += ( old_ext_pot_action[0] - new_ext_pot_action[0] );
	delta_ext_pot_action_derivative += ( old_ext_pot_action[1] - new_ext_pot_action[1] );
	
	
	
	
	// Obtain the acceptance ratio:
	double ratio = exp( config->params.epsilon*delta_u ) * exp( config->params.epsilon*delta_pair ) * exp( config->params.epsilon*delta_ext_pot_action );
	
	if( config->params.PBC_detailed_balance ) // If rho is a sum over Gaussians, diffusion and sampling terms do not exactly cancel!
	{
		ratio = ratio * diffusion_ratio * sampling_ratio;
	}
	
	
	// Obtain a random number to decide acceptance:
	double choice = rdm();
	
	if(debug) // verbose, check for overflow if !PBC_detailed_balance:
	{
		std::cout << "+++++++++++++++ standard PIMC DEFORM ++++++++++++++++\n";
		std::cout << "delta_u: " << delta_u << "\t head_id: " << config->head_id << "\t tail_id: " << config->tail_id << "\t diag: " << config->diag << std::endl;
		std::cout << "delta_pair: " << delta_pair << "\t delta_pair_derivative: " << delta_pair_derivative << "\n";
		std::cout << "delta_m_e_pair_derivative: " << delta_m_e_pair_derivative << "\t delta_m_p_pair_derivative: " << delta_m_p_pair_derivative << "\n";
		std::cout << "delta_ext_pot_action: " << delta_ext_pot_action << "\t delta_ext_pot_action_derivative: " << delta_ext_pot_action_derivative << "\n";
		std::cout << "ratio: " << ratio << "\t choice: " << choice << "\t diffusion_ratio: " << diffusion_ratio << "\n";// "\t sampling_prob: " << sampling_prob << "\t reverse_sampling_prob: " << reverse_sampling_prob << "\n";
	
		if( isnana( ratio ) ) // || !(choice <= ratio) )
		{
			std::cout << "isnana in ratio\n";
			int z;
			std::cin >> z;
		}
	}
	
	if( choice <= ratio ) // The update has been accepted:
	{
		ensemble->total_diffusion_product *= diffusion_ratio;
		
		// book keeping of the total potential energy:
		ensemble->total_energy -= delta_u;
		
		ensemble->total_pair_action -= delta_pair;
		ensemble->total_pair_derivative -= delta_pair_derivative;
		
		ensemble->total_ext_pot_action -= delta_ext_pot_action;
		ensemble->total_ext_pot_action_derivative -= delta_ext_pot_action_derivative;
		
		ensemble->total_m_e_pair_derivative -= delta_m_e_pair_derivative;
		ensemble->total_m_p_pair_derivative -= delta_m_p_pair_derivative;
		
		// Replace the m affected beads + the start bead because of the changed diffusion_element:
		for(int i=0;i<1+m;i++)
		{
			int i_id = new_beads[ i+1 ].get_prev_id(); // id of the changed bead
			config->beads[ i_id ].copy( &new_beads[ i ] ); // replace old bead by the new one
		} 
		
		if( debug ) // Debug option: Write particle coordinates and bead structure to the disk
		{
			std::cout << "-+my_species: " << my_species << "\n";
			config->standard_PIMC_print_paths( "deform_paths.dat" );
			config->standard_PIMC_print_beadlist( "deform_beadlist.dat" );
		}

		return 1;
	}
	else // The update has been rejected
	{
		return 0;
	}

}
