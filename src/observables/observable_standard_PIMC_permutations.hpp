/*
 * Contains class: observable_standard_PIMC_permutations
 */


template <class inter> class observable_standard_PIMC_permutations
{
public:

	
	// Basic properties:
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	int n_buffer;  // buffer size
	int n_seg; // number of bins for the radial density
	int n_cycle; // number of configs to be skipped between measurements
	int cnt = 0; // config cycle counter
		
	
	std::string bose_name = "boson_permutations";
	std::string fermi_name = "fermion_permutations";
	
	// Methods:
	observable_standard_PIMC_permutations(){ } // TBD: implement constructor
	
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer ); // initialize stuff
	
	void measure( config_ensemble* ens );
	
	 
	
	// measurement properties:
	std::vector<int>tmp;
	
	bool write_all;

};




// initializes stuff
template <class inter> void observable_standard_PIMC_permutations<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer )
{
	n_cycle = new_n_cycle;
	n_buffer = new_n_buffer;
	n_seg = new_n_seg;
	
	std::cout << "Initialization of permutations, n_seg = " << n_seg << "\n";
	
	tmp.assign( n_seg, 0 );

	write_all = new_write_all;
	
	boson_storage.initialize( write_all, bose_name, n_seg, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, n_seg, new_n_buffer, binning_level );
	
}




// perform the actual measurements
template <class inter> void observable_standard_PIMC_permutations<inter>::measure( config_ensemble* ens )
{
	if( cnt < n_cycle ) // Check, if enough configs have been skipped
	{
		cnt++;
	}
	else
	{
		cnt = 0; // Reset cycle counter and perform the actual measurement
	
		// determine the particle number
		int N = ens->N_tot;
		
		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer( N );
		estimator_array* fermion_array = fermion_storage.pointer( N );

		
		for(auto c=ens->species_config.begin();c!=ens->species_config.end();c++) // loop over all particle species
		{
			int start_slice = 0;
			if( !c->diag ) start_slice = c->beads[ c->head_id ].get_time();
			
			std::vector<int> counted_zero_beads;
			int tmp_Npp = 0; // re-compute the number of pair-exchanges in the system on the fly
			
			for(auto b=c->beadlist[ start_slice ].begin();b!=c->beadlist[ start_slice ].end();b++) // loop over all beads on the "start_slice"
			{
				if( !in_vector( (*b), counted_zero_beads ) ) // check if b has already been encountered in a previous exchange cycle
				{
					int b_length = 1; // length of the current permutation cycle
					
					counted_zero_beads.push_back( (*b) );
					int tmp_id = c->beads[ (*b) ].get_prev_id();
					while( tmp_id != (*b) ) // go along the trajectory until we are back at the start (or at the tail)
					{
						if( tmp_id == c->tail_id ) break;
						
						int tmp_time = c->beads[ tmp_id ].get_time();
						if( tmp_time == start_slice ) // if we are back at start_slice, but not the start id, increase the length of the current exchange cycle
						{
							b_length++;
							counted_zero_beads.push_back( tmp_id ); // the bead 'tmp_id' on the start_slice must not be counted again!
						}
						
						tmp_id = c->beads[ tmp_id ].get_prev_id();
					}
					
					// Update the permutation cycle histogram with the length of the current exchange cycle, b_length
					if( b_length < n_seg ) tmp[ b_length ] += 1;
					
					// Update the number of pair-exchanges in the system
					tmp_Npp += (b_length-1);
					
				} // end in_vector condition
				
			} // end loop over all beads on the start slice
			
			tmp[ 0 ] += tmp_Npp; // the 0 element of tmp corresponds to the total number of pair exchanges
			if( ens->params.pp_control ) 
			{
				if( ( c->Npp - tmp_Npp ) != 0 )
				{
					std::cout << "Error in Npp, stored_Npp: " << c->Npp << "\t obtained on the fly: " << tmp_Npp << "\n";
					exit(0);
				}
			}

		} // end loop over all particle species
		
		
		// Submit value of each bin to the estimator_array_set
		for(auto i=tmp.begin();i!=tmp.end();i++)
		{
			// index of this bin
			int index = i - tmp.begin();
			
			boson_array->add_value( index, (*i) );
			fermion_array->add_value( index, (*i) );
				
			// Reset the tmp vector before the next measurement
			(*i) = 0;
		}

		
	} // end measurement cycle condition
	
}












