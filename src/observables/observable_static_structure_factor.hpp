/*
 * Contains class: observable_static_structure_factor
 */


// compute the sum over the squares of all elementes of the vector a
double v_squared( std::vector<double> a )
{
	double ans = 0.0;
	for(auto c=a.begin();c!=a.end();c++)
	{
		ans += (*c)*(*c);
	}
	return ans;
}


// Write the n_k k-vectors with the lowest |k| into ans_k_vectors. 
// Write the used k-vectors into 'static_structure_factor_key.dat'
// Do not use different k-vectors with the same |k|
void create_k_vectors( std::vector< std::vector<double> > *ans_k_vectors, int n_k, double L )
{
	double tol=1e-8;
	std::fstream f;
	f.open("static_structure_factor_key.dat", std::ios::out );
	
	std::vector<std::vector<double> >k_vectors;
	std::vector<double> k_mods; 
	std::vector<double>ol(3,0.0);
	for(int x=0;x<n_k;x++)
	{
		ol[0] = x * 2.0*pi / L;
		for(int y=0;y<n_k;y++)
		{
			ol[1] = y * 2.0*pi / L;
			for(int z=0;z<n_k;z++)
			{
				ol[2] = z * 2.0*pi / L;
				
				if( (x!=0) || (y!=0) || (z!=0) ) // Exclude the k=0 vector
				{
					
					double tmp_k = sqrt( v_squared( ol ) );
					
					
					// go through all elements in k_squares to see if such a |k| has already been included:
					bool ctrl = 1;
					for(auto q=k_mods.begin();q!=k_mods.end();q++)
					{
						double k_diff = fabs( ( tmp_k - (*q) )/tmp_k );
						if( k_diff < tol ) // the |k| has been included before 
						{
							ctrl = 0;
							break;
						}
					}
							

					if( ctrl ) // only include k-vector, whose |k| has not already been included
					{
						k_vectors.push_back( ol );
						k_mods.push_back( tmp_k );
					}
					
				}
			}
		}
	}
	
	

	std::vector<double> tmp_k_vector = k_vectors[0]; // store to swap k_vectors in the sort 
	
	for(int i_level=0;i_level<n_k;i_level++) // loop over the sorting(bubble)-level
	{
		
		double minimum = k_mods[i_level]; // start with sth. which is definitely not the minimum

		for(int i=i_level+1;i<k_mods.size();i++) // loop over all the remaining elements for each sort-level
		{
			
			
			if( k_mods[i] < minimum ) // if the i-element is smaller than the previous minimum, swap the two
			{
					// set the new minimum
					minimum = k_mods[i];
					

					// swap the k_squares
					k_mods[i] = k_mods[i_level];
					k_mods[i_level] = minimum;
					
					// swap the k_vectors
					tmp_k_vector = k_vectors[i_level];
					k_vectors[i_level] = k_vectors[i];
					k_vectors[i] = tmp_k_vector;
			}
			
		} // end loop i
		
		
		// write the new k-vector to k_vectors
		ans_k_vectors->push_back( k_vectors[i_level] );
		

		// write information about the new k-vector to file
		f << i_level << "\t" << ( minimum ) << "\t" << k_vectors[i_level][0] << "\t" << k_vectors[i_level][1] << "\t" << k_vectors[i_level][2] << "\n";
		
		
	} // end loop i_level
	
	f.close();
}


template <class inter> class observable_static_structure_factor
{
public:

	
	// Basic properties:
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
 	
 	estimator_array_set boson_proton_storage, fermion_proton_storage;
	estimator_array_set boson_proton_electron_storage, fermion_proton_electron_storage;
	
	std::vector<std::vector<double> > k_vectors; // store all k-vectors
	int n_k; // number of k-vectors
	double L; // box-length of the simulation cell
	
	int n_buffer;  // buffer size
	
	int n_cycle; // number of configs to be skipped between measurements
	int cnt = 0; // config cycle counter
	
	
	
	std::string bose_name = "boson_static_structure_factor";
	std::string fermi_name = "fermion_static_structure_factor";
	
	std::string bose_name_proton = "boson_proton_static_structure_factor";
	std::string fermi_name_proton = "fermion_proton_static_structure_factor";
	
	std::string bose_name_proton_electron = "boson_proton_electron_static_structure_factor";
	std::string fermi_name_proton_electron = "fermion_proton_electron_static_structure_factor";
	
	// Methods:
	observable_static_structure_factor(){ } // TBD: implement constructor
	
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_k, double new_L, int new_n_buffer ); // initialize stuff
	
	void measure( config_ensemble* ens );

	bool write_all;

};




// initializes stuff
template <class inter> void observable_static_structure_factor<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_k, double new_L, int new_n_buffer )
{
	n_cycle = new_n_cycle;
	n_buffer = new_n_buffer;
	
	n_k = new_n_k;
	L = new_L;
	
	// Fill the k-vectors
	// TBD TBD TBD -> init needs to know the dimensionality of the system in advance!
	create_k_vectors( &k_vectors, n_k, L );

	
	write_all = new_write_all;
	
	boson_storage.initialize( write_all, bose_name, n_k, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, n_k, new_n_buffer, binning_level );
	
	boson_proton_storage.initialize( write_all, bose_name_proton, n_k, new_n_buffer, binning_level );
	fermion_proton_storage.initialize( write_all, fermi_name_proton, n_k, new_n_buffer, binning_level );
	
	boson_proton_electron_storage.initialize( write_all, bose_name_proton_electron, n_k, new_n_buffer, binning_level );
	fermion_proton_electron_storage.initialize( write_all, fermi_name_proton_electron, n_k, new_n_buffer, binning_level );
}




// perform the actual measurements
template <class inter> void observable_static_structure_factor<inter>::measure( config_ensemble* ens )
{

	if( cnt < n_cycle ) // Check, if enough configs have been skipped
	{
		cnt++;
	}
	else
	{
		cnt = 0; // Reset cycle counter and perform the actual measurement
	
	
		if( ens->params.dim < 3 ) return; // ATM, S(k) is defined only for 3D systems
	
		// determine the particle number
		int N = ens->N_tot; 

		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer( N );
		estimator_array* fermion_array = fermion_storage.pointer( N );
		
		estimator_array* boson_array_proton = boson_proton_storage.pointer( N );
		estimator_array* fermion_array_proton = fermion_proton_storage.pointer( N );
		
		estimator_array* boson_array_proton_electron = boson_proton_electron_storage.pointer( N );
		estimator_array* fermion_array_proton_electron = fermion_proton_electron_storage.pointer( N );
		
		
		std::vector<double> tmp( n_k, 0.0 );
		std::vector< std::vector<double> >tmp_cos( ens->params.n_bead, tmp );
		std::vector< std::vector<double> >tmp_sin( ens->params.n_bead, tmp );
		
		std::vector< std::vector<double> >tmp_cos_proton( ens->params.n_bead, tmp );
		std::vector< std::vector<double> >tmp_sin_proton( ens->params.n_bead, tmp );
		
		
		
		int n_species = ens->species_config.size();
		if( n_species > 2 ) n_species = 2; // This is strictly the electron--electron SSF
		
		
		#pragma omp parallel 
		{
			#pragma omp for
			for(int iSlice=0;iSlice<ens->params.n_bead;iSlice++) // loop over all the main slices
			{
				// Loop over all the electron species:
				
				for(int iSpecies=0;iSpecies<n_species;iSpecies++) // loop over all species for first particle 'i'
				{
					for(int i=0;i<ens->species_config[ iSpecies ].beadlist[ iSlice ].size();i++) // loop over all particles of species 'iSpecies' for first particle 'i'
					{

						int i_id = ens->species_config[ iSpecies ].beadlist[ iSlice ][ i ];
							
						std::vector<double>x_j( ens->species_config[ iSpecies ].params.dim, 0.0 );
						for(int iDim=0;iDim<ens->species_config[ iSpecies ].params.dim;iDim++)
						{
							x_j[ iDim ] = ens->species_config[ iSpecies ].beads[ i_id ].get_coord( iDim );
						}
							
						for(int iK=0;iK<n_k;iK++) // loop over all k-vectors
						{
							double cos_arg = 0.0;
							for(int iDim=0;iDim<ens->params.dim;iDim++)
							{
								cos_arg += x_j[ iDim ]*k_vectors[ iK ][ iDim ];
							}
								
							double my_S_k_cos = cos( cos_arg );
							double my_S_k_sin = sin( cos_arg );
									
							tmp_cos[ iSlice ][ iK ] += my_S_k_cos;
							tmp_sin[ iSlice ][ iK ] += my_S_k_sin;
		
						} // end loop over all k-vectors (iK)
						
					} // end loop i to N[iSpecies]
						
				} // end loop iSpecies
				
				
				
				// Loop over the protons, if they exist
				if( ens->species_config.size() > 2 )
				{
					for(int i=0;i<ens->species_config[ 2 ].beadlist[ iSlice ].size();i++)
					{
						int i_id = ens->species_config[ 2 ].beadlist[ iSlice ][ i ];
						
						std::vector<double>x_j( ens->species_config[ 2 ].params.dim, 0.0 );
						for(int iDim=0;iDim<ens->species_config[ 2 ].params.dim;iDim++)
						{
							x_j[ iDim ] = ens->species_config[ 2 ].beads[ i_id ].get_coord( iDim );
						}
							
						for(int iK=0;iK<n_k;iK++) // loop over all k-vectors
						{
							double cos_arg = 0.0;
							for(int iDim=0;iDim<ens->params.dim;iDim++)
							{
								cos_arg += x_j[ iDim ]*k_vectors[ iK ][ iDim ];
							}
								
							double my_S_k_cos = cos( cos_arg );
							double my_S_k_sin = sin( cos_arg );
									
							tmp_cos_proton[ iSlice ][ iK ] += my_S_k_cos;
							tmp_sin_proton[ iSlice ][ iK ] += my_S_k_sin;
		
						} // end loop over all k-vectors (iK)
						
						
					} // end loop i (all protons on iSlice)
					
					
					
					
				} // end "do we have protons?" if-conditions
				
				
				
					
			} // end loop iSlice
		
		
		
		}
		
		

		
		
		for(int iK=0;iK<n_k;iK++)
		{
			double cos_sq = 0.0;
			double sin_sq = 0.0;
			
			double cos_sq_proton = 0.0;
			double sin_sq_proton = 0.0;
			
			double cos_sq_proton_electron = 0.0;
			double sin_sq_proton_electron = 0.0;
			
			for(int iSlice=0;iSlice<ens->params.n_bead;iSlice++)
			{
				cos_sq += tmp_cos[ iSlice ][ iK ] * tmp_cos[ iSlice ][ iK ];
				sin_sq += tmp_sin[ iSlice ][ iK ] * tmp_sin[ iSlice ][ iK ];
				
				cos_sq_proton += tmp_cos_proton[ iSlice ][ iK ] * tmp_cos_proton[ iSlice ][ iK ];
				sin_sq_proton += tmp_sin_proton[ iSlice ][ iK ] * tmp_sin_proton[ iSlice ][ iK ];
				
				cos_sq_proton_electron += tmp_cos[ iSlice ][ iK ] * tmp_cos_proton[ iSlice ][ iK ];
				sin_sq_proton_electron += tmp_sin[ iSlice ][ iK ] * tmp_sin_proton[ iSlice ][ iK ];
			}
			cos_sq = cos_sq / double( ens->params.n_bead );
			sin_sq = sin_sq / double( ens->params.n_bead );
			
			cos_sq_proton = cos_sq_proton / double( ens->params.n_bead );
			sin_sq_proton = sin_sq_proton / double( ens->params.n_bead );
			
			cos_sq_proton_electron = cos_sq_proton_electron / double( ens->params.n_bead );
			sin_sq_proton_electron = sin_sq_proton_electron / double( ens->params.n_bead );
				
			double my_S_k = ( cos_sq + sin_sq ) / double(N);
			double my_S_k_proton = ( cos_sq_proton + sin_sq_proton ) / double(N);
			double my_S_k_proton_electron = ( cos_sq_proton_electron + sin_sq_proton_electron ) / double(N);

			boson_array->add_value(iK, my_S_k);
			fermion_array->add_value(iK, ens->get_sign()*my_S_k);
			
			boson_array_proton->add_value(iK, my_S_k_proton);
			fermion_array_proton->add_value(iK, my_S_k_proton*ens->get_sign());
			
			boson_array_proton_electron->add_value(iK, my_S_k_proton_electron);
			fermion_array_proton_electron->add_value(iK, my_S_k_proton_electron*ens->get_sign());

		}

		
	} // end measurement cycle condition
	
}












