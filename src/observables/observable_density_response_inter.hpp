/*
 * Contains class: observable_density_response_inter
 */







template <class inter> class observable_density_response_inter
{
public:

	
	// Basic properties:
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
 	estimator_array_set boson_storage_proton, fermion_storage_proton;
	estimator_array_set boson_storage_electron_proton, fermion_storage_electron_proton;
	estimator_array_set boson_storage_electron, fermion_storage_electron;
	estimator_array_set boson_storage_full, fermion_storage_full;
	estimator_array_set boson_storage_0, fermion_storage_0;
	estimator_array_set boson_storage_1, fermion_storage_1;
	
	
	
	std::vector<std::vector<double> > k_vectors; // store all k-vectors
	int n_k; // number of k-vectors
	double L; // box-length of the simulation cell
	
	int n_buffer;  // buffer size
	
	int n_cycle; // number of configs to be skipped between measurements
	int cnt = 0; // config cycle counter
	
	
	
	int n_bead;
	
	
	
	std::string bose_name = "boson_density_response_inter";
	std::string fermi_name = "fermion_density_response_inter";

	std::string bose_name_proton = "boson_density_response_proton";
	std::string fermi_name_proton = "fermion_density_response_proton";
	
	std::string bose_name_electron_proton = "boson_density_response_electron_proton";
	std::string fermi_name_electron_proton = "fermion_density_response_electron_proton";
	
	std::string bose_name_electron = "boson_density_response_electron";
	std::string fermi_name_electron = "fermion_density_response_electron";
	
	
	std::string bose_name_full = "boson_density_response_full";
	std::string fermi_name_full = "fermion_density_response_full";
	
	std::string bose_name_0 = "boson_density_response_0";
	std::string fermi_name_0 = "fermion_density_response_0";
	
	std::string bose_name_1 = "boson_density_response_1";
	std::string fermi_name_1 = "fermion_density_response_1";
	
	
	
	
	// Methods:
	observable_density_response_inter(){ } // TBD: implement constructor
	
	void init( int binning_level, int new_n_bead, bool new_write_all, int new_n_cycle, int new_n_k, double new_L, int new_n_buffer ); // initialize stuff
	
	void measure( config_ensemble* ens );

	bool write_all;

};




// initializes stuff
template <class inter> void observable_density_response_inter<inter>::init( int binning_level, int new_n_bead, bool new_write_all, int new_n_cycle, int new_n_k, double new_L, int new_n_buffer )
{
	n_cycle = new_n_cycle;
	n_buffer = new_n_buffer;
	
	n_k = new_n_k;
	L = new_L;
	
	
	n_bead = new_n_bead;
	
	// Fill the k-vectors
	// TBD TBD TBD -> init needs to know the dimensionality of the system in advance!
	create_k_vectors( &k_vectors, n_k, L );

	
	write_all = new_write_all;
	
	boson_storage.initialize( write_all, bose_name, (n_bead+1)*n_k, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, (n_bead+1)*n_k, new_n_buffer, binning_level );
	
	boson_storage_proton.initialize( write_all, bose_name_proton, (n_bead+1)*n_k, new_n_buffer, binning_level );
	fermion_storage_proton.initialize( write_all, fermi_name_proton, (n_bead+1)*n_k, new_n_buffer, binning_level );
	
	boson_storage_electron_proton.initialize( write_all, bose_name_electron_proton, (n_bead+1)*n_k, new_n_buffer, binning_level );
	fermion_storage_electron_proton.initialize( write_all, fermi_name_electron_proton, (n_bead+1)*n_k, new_n_buffer, binning_level );
	
	boson_storage_electron.initialize( write_all, bose_name_electron, (n_bead+1)*n_k, new_n_buffer, binning_level );
	fermion_storage_electron.initialize( write_all, fermi_name_electron, (n_bead+1)*n_k, new_n_buffer, binning_level );
	
	
	boson_storage_full.initialize( write_all, bose_name_full, (n_bead+1)*n_k, new_n_buffer, binning_level );
	fermion_storage_full.initialize( write_all, fermi_name_full, (n_bead+1)*n_k, new_n_buffer, binning_level );
	
	boson_storage_0.initialize( write_all, bose_name_0, (n_bead+1)*n_k, new_n_buffer, binning_level );
	fermion_storage_0.initialize( write_all, fermi_name_0, (n_bead+1)*n_k, new_n_buffer, binning_level );
	
	boson_storage_1.initialize( write_all, bose_name_1, (n_bead+1)*n_k, new_n_buffer, binning_level );
	fermion_storage_1.initialize( write_all, fermi_name_1, (n_bead+1)*n_k, new_n_buffer, binning_level );
	
}




// perform the actual measurements
template <class inter> void observable_density_response_inter<inter>::measure( config_ensemble* ens )
{

	if( cnt < n_cycle ) // Check, if enough configs have been skipped
	{
		cnt++;
	}
	else
	{
		cnt = 0; // Reset cycle counter and perform the actual measurement
	
	//	if( ens->species_config.size() == 1 ) return; // Only one particle species exists, no inter-species observables can be computed !
	
		if( ens->params.dim < 3 ) return; // ATM, S(k) is defined only for 3D systems
	
		// determine the particle number
		int N = ens->N_tot; 

		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer( N );
		estimator_array* fermion_array = fermion_storage.pointer( N );
		
		estimator_array* boson_array_proton = boson_storage_proton.pointer( N );
		estimator_array* fermion_array_proton = fermion_storage_proton.pointer( N );
		
		estimator_array* boson_array_electron = boson_storage_electron.pointer( N );
		estimator_array* fermion_array_electron = fermion_storage_electron.pointer( N );
		
		estimator_array* boson_array_electron_proton = boson_storage_electron_proton.pointer( N );
		estimator_array* fermion_array_electron_proton = fermion_storage_electron_proton.pointer( N );
		
		
		
		estimator_array* boson_array_full = boson_storage_full.pointer( N );
		estimator_array* fermion_array_full = fermion_storage_full.pointer( N );
		
		estimator_array* boson_array_0 = boson_storage_0.pointer( N );
		estimator_array* fermion_array_0 = fermion_storage_0.pointer( N );
		
		estimator_array* boson_array_1 = boson_storage_1.pointer( N );
		estimator_array* fermion_array_1 = fermion_storage_1.pointer( N );
		
		
		

		std::vector<double> tmp( n_k, 0.0 );
		
		std::vector< std::vector<double> >tmp_cos_0( ens->params.n_bead, tmp );
		std::vector< std::vector<double> >tmp_sin_0( ens->params.n_bead, tmp );
		
		std::vector< std::vector<double> >tmp_cos_1( ens->params.n_bead, tmp );
		std::vector< std::vector<double> >tmp_sin_1( ens->params.n_bead, tmp );
		
		std::vector< std::vector<double> >tmp_cos_proton( ens->params.n_bead, tmp );
		std::vector< std::vector<double> >tmp_sin_proton( ens->params.n_bead, tmp );
		
		std::vector< std::vector<double> >tmp_cos_electron( ens->params.n_bead, tmp );
		std::vector< std::vector<double> >tmp_sin_electron( ens->params.n_bead, tmp );
		
		std::vector< std::vector<double> >tmp_cos_full( ens->params.n_bead, tmp );
		std::vector< std::vector<double> >tmp_sin_full( ens->params.n_bead, tmp );
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// ##########################################################################################################################
		// ### Start::OpenMP parallelization part
	
		
		
		
		// Create a list with all the S and C functions
		#pragma omp parallel 
		{
			#pragma omp for
			for(int iSlice=0;iSlice<ens->params.n_bead;iSlice++)
			{
				
				// On each slice, loop over all the particles, separately for species 0 and 1
				
					
				// loop over species 0:
				for(int i=0;i<ens->species_config[ 0 ].beadlist[ iSlice ].size();i++)
				{
					// obtain the particle ID:
					int id = ens->species_config[ 0 ].beadlist[ iSlice ][ i ];
						
					// obtain the particle coords:
					std::vector<double>xi( ens->params.dim, 0.0 );
					for(int iDim=0;iDim<ens->params.dim;iDim++)
					{
						xi[ iDim ] = ens->species_config[ 0 ].beads[ id ].get_coord( iDim );
					}
						
					// Now loop over all the q-vectors
					for(int iQ=0;iQ<n_k;iQ++)
					{
						double argument = 0.0;
						for(int iDim=0;iDim<ens->params.dim;iDim++)
						{
							argument += xi[ iDim ]*k_vectors[ iQ ][ iDim ];
						}
						
						double ans_cos = cos( argument );
						double ans_sin = sin( argument );
							
						tmp_cos_0[ iSlice ][ iQ ] += ans_cos;
						tmp_sin_0[ iSlice ][ iQ ] += ans_sin;
						
						tmp_cos_electron[ iSlice ][ iQ ] += ans_cos;
						tmp_sin_electron[ iSlice ][ iQ ] += ans_sin;
						
						tmp_cos_full[ iSlice ][ iQ ] += ans_cos;
						tmp_sin_full[ iSlice ][ iQ ] += ans_sin;
							
					} // end loop iQ
				}
					
					
					
					
				if( ens->species_config.size() > 1 ) // Do we have a second species of electrons in the system?
				{
					// loop over species 1:
					for(int i=0;i<ens->species_config[ 1 ].beadlist[ iSlice ].size();i++)
					{
						// obtain the particle ID:
						int id = ens->species_config[ 1 ].beadlist[ iSlice ][ i ];
							
						// obtain the particle coords:
						std::vector<double>xi( ens->params.dim, 0.0 );
						for(int iDim=0;iDim<ens->params.dim;iDim++)
						{
							xi[ iDim ] = ens->species_config[ 1 ].beads[ id ].get_coord( iDim );
						}
							
						// Now loop over all the q-vectors
						for(int iQ=0;iQ<n_k;iQ++)
						{
							double argument = 0.0;
							for(int iDim=0;iDim<ens->params.dim;iDim++)
							{
								argument += xi[ iDim ]*k_vectors[ iQ ][ iDim ];
							}
								
							double ans_cos = cos( argument );
							double ans_sin = sin( argument );
								
							tmp_cos_1[ iSlice ][ iQ ] += ans_cos;
							tmp_sin_1[ iSlice ][ iQ ] += ans_sin;
							
							tmp_cos_electron[ iSlice ][ iQ ] += ans_cos;
							tmp_sin_electron[ iSlice ][ iQ ] += ans_sin;
							
							tmp_cos_full[ iSlice ][ iQ ] += ans_cos;
							tmp_sin_full[ iSlice ][ iQ ] += ans_sin;
							
								
						} // end loop iQ
					}
					
				} // end of "do we have two electron species?" if clause
				
				
				
				
				
				if( ens->species_config.size() > 2 ) // We have protons in the system!
				{
					// loop over species 2:
					for(int i=0;i<ens->species_config[ 2 ].beadlist[ iSlice ].size();i++)
					{
						// obtain the particle ID:
						int id = ens->species_config[ 2 ].beadlist[ iSlice ][ i ];
							
						// obtain the particle coords:
						std::vector<double>xi( ens->params.dim, 0.0 );
						for(int iDim=0;iDim<ens->params.dim;iDim++)
						{
							xi[ iDim ] = ens->species_config[ 2 ].beads[ id ].get_coord( iDim );
						}
							
						// Now loop over all the q-vectors
						for(int iQ=0;iQ<n_k;iQ++)
						{
							double argument = 0.0;
							for(int iDim=0;iDim<ens->params.dim;iDim++)
							{
								argument += xi[ iDim ]*k_vectors[ iQ ][ iDim ];
							}
								
							double ans_cos = cos( argument );
							double ans_sin = sin( argument );
								
							tmp_cos_proton[ iSlice ][ iQ ] += ans_cos;
							tmp_sin_proton[ iSlice ][ iQ ] += ans_sin;
							
							tmp_cos_full[ iSlice ][ iQ ] += ans_cos;
							tmp_sin_full[ iSlice ][ iQ ] += ans_sin;
								
						} // end loop iQ
					}

				} // end of "do we have protons?" if clause
				

				
				
			} // end loop iSlice
		
		
		}
		
		
		
		
		
		// ### No OpenMP post processing is needed here!
		
		
		
		
	
	
	
		// ##########################################################################################################################
		// ### Start::OpenMP parallelization part
		

		
		std::vector<double> OpenMP_mvalue( (n_bead+1)*n_k, 0.0 );
		std::vector<double> OpenMP_mvalue_proton( (n_bead+1)*n_k, 0.0 );
		std::vector<double> OpenMP_mvalue_electron( (n_bead+1)*n_k, 0.0 );
		std::vector<double> OpenMP_mvalue_electron_proton( (n_bead+1)*n_k, 0.0 );
		std::vector<double> OpenMP_mvalue_full( (n_bead+1)*n_k, 0.0 );
		std::vector<double> OpenMP_mvalue_0( (n_bead+1)*n_k, 0.0 );
		std::vector<double> OpenMP_mvalue_1( (n_bead+1)*n_k, 0.0 );
		
		
		
		
		#pragma omp parallel 
		{
			#pragma omp for
			for(int iK=0;iK<n_k;iK++)
			{
				
// 				// ### OpenMP:: double integral = 0.0; // inter-electron 
// 				// ### OpenMP:: double integral_proton = 0.0;
// 				// ### OpenMP:: double integral_electron = 0.0;
// 				// ### OpenMP:: double integral_electron_proton = 0.0;
// 				
// 				// ### OpenMP:: double integral_full = 0.0;
// 				// ### OpenMP:: double integral_0 = 0.0;
// 				// ### OpenMP:: double integral_1 = 0.0;
				
				
				for(int m=0;m<ens->params.n_bead;m++) // the integration loop over all slices
				{
// 					// ### OpenMP:: double mvalue = 0.0;
// 					// ### OpenMP:: double mvalue_proton = 0.0;
// 					// ### OpenMP:: double mvalue_electron = 0.0;
// 					// ### OpenMP:: double mvalue_electron_proton = 0.0;
// 					
// 					// ### OpenMP:: double mvalue_full = 0.0;
// 					// ### OpenMP:: double mvalue_0 = 0.0;
// 					// ### OpenMP:: double mvalue_1 = 0.0;
					
					
					for(int alpha=0;alpha<ens->params.n_bead;alpha++)
					{
						int alpha_m = alpha + m;
						if( alpha_m >= ens->params.n_bead )
						{
							alpha_m -= ens->params.n_bead;
						}
						double addend = tmp_cos_0[ alpha ][ iK ]*tmp_cos_1[ alpha_m ][ iK ] + tmp_sin_0[ alpha ][ iK ]*tmp_sin_1[ alpha_m ][ iK ];
						// ### OpenMP:: integral += addend;
						// ### OpenMP:: mvalue += addend;
						
						OpenMP_mvalue[ n_k*(1+m)+iK ] += addend; // mvalue
						OpenMP_mvalue[ iK ] += addend; // integral
						
						
						
						double addend_proton = tmp_cos_proton[ alpha ][ iK ]*tmp_cos_proton[ alpha_m ][ iK ] + tmp_sin_proton[ alpha ][ iK ]*tmp_sin_proton[ alpha_m ][ iK ];
						// ### OpenMP:: integral_proton += addend_proton;
						// ### OpenMP:: mvalue_proton += addend_proton;
						
						OpenMP_mvalue_proton[ n_k*(1+m)+iK ] += addend_proton; // mvalue
						OpenMP_mvalue_proton[ iK ] += addend_proton; // integral
						
						
						
						double addend_electron = tmp_cos_electron[ alpha ][ iK ]*tmp_cos_electron[ alpha_m ][ iK ] + tmp_sin_electron[ alpha ][ iK ]*tmp_sin_electron[ alpha_m ][ iK ];
						// ### OpenMP:: integral_electron += addend_electron;
						// ### OpenMP:: mvalue_electron += addend_electron;
						
						OpenMP_mvalue_electron[ n_k*(1+m)+iK ] += addend_electron; // mvalue
						OpenMP_mvalue_electron[ iK ] += addend_electron; // integral
						
						
						
						double addend_electron_proton = tmp_cos_proton[ alpha ][ iK ]*tmp_cos_electron[ alpha_m ][ iK ] + tmp_sin_proton[ alpha ][ iK ]*tmp_sin_electron[ alpha_m ][ iK ];
						// ### OpenMP:: integral_electron_proton += addend_electron_proton;
						// ### OpenMP:: mvalue_electron_proton += addend_electron_proton;
						
						OpenMP_mvalue_electron_proton[ n_k*(1+m)+iK ] += addend_electron_proton; // mvalue
						OpenMP_mvalue_electron_proton[ iK ] += addend_electron_proton; // integral
						

						
						double addend_full = tmp_cos_full[ alpha ][ iK ]*tmp_cos_full[ alpha_m ][ iK ] + tmp_sin_full[ alpha ][ iK ]*tmp_sin_full[ alpha_m ][ iK ];
						// ### OpenMP:: integral_full += addend_full;
						// ### OpenMP:: mvalue_full += addend_full;
						
						OpenMP_mvalue_full[ n_k*(1+m)+iK ] += addend_full; // mvalue
						OpenMP_mvalue_full[ iK ] += addend_full; // integral
						
						
						
						double addend_0 = tmp_cos_0[ alpha ][ iK ]*tmp_cos_0[ alpha_m ][ iK ] + tmp_sin_0[ alpha ][ iK ]*tmp_sin_0[ alpha_m ][ iK ];
						// ### OpenMP:: integral_0 += addend_0;
						// ### OpenMP:: mvalue_0 += addend_0;
						
						OpenMP_mvalue_0[ n_k*(1+m)+iK ] += addend_0; // mvalue
						OpenMP_mvalue_0[ iK ] += addend_0; // integral
						
						
						
						double addend_1 = tmp_cos_1[ alpha ][ iK ]*tmp_cos_1[ alpha_m ][ iK ] + tmp_sin_1[ alpha ][ iK ]*tmp_sin_1[ alpha_m ][ iK ];
						// ### OpenMP:: integral_1 += addend_1;
						// ### OpenMP:: mvalue_1 += addend_1;
						
						OpenMP_mvalue_1[ n_k*(1+m)+iK ] += addend_1; // mvalue
						OpenMP_mvalue_1[ iK ] += addend_1; // integral
						
					}
					
					
					/* 
					 * ### OpenMP ###
					
					boson_array->add_value( n_k*(1+m)+iK, mvalue/double( ens->params.n_bead ) );
					fermion_array->add_value( n_k*(1+m)+iK, mvalue/double( ens->params.n_bead )*ens->get_sign() );
					
					boson_array_proton->add_value( n_k*(1+m)+iK, mvalue_proton/double( ens->params.n_bead ) );
					fermion_array_proton->add_value( n_k*(1+m)+iK, mvalue_proton/double( ens->params.n_bead )*ens->get_sign() );
					
					boson_array_electron->add_value( n_k*(1+m)+iK, mvalue_electron/double( ens->params.n_bead ) );
					fermion_array_electron->add_value( n_k*(1+m)+iK, mvalue_electron/double( ens->params.n_bead )*ens->get_sign() );
					
					boson_array_electron_proton->add_value( n_k*(1+m)+iK, mvalue_electron_proton/double( ens->params.n_bead ) );
					fermion_array_electron_proton->add_value( n_k*(1+m)+iK, mvalue_electron_proton/double( ens->params.n_bead )*ens->get_sign() );
					
					boson_array_full->add_value( n_k*(1+m)+iK, mvalue_full/double( ens->params.n_bead ) );
					fermion_array_full->add_value( n_k*(1+m)+iK, mvalue_full/double( ens->params.n_bead )*ens->get_sign() );
					
					boson_array_0->add_value( n_k*(1+m)+iK, mvalue_0/double( ens->params.n_bead ) );
					fermion_array_0->add_value( n_k*(1+m)+iK, mvalue_0/double( ens->params.n_bead )*ens->get_sign() );
					
					boson_array_1->add_value( n_k*(1+m)+iK, mvalue_1/double( ens->params.n_bead ) );
					fermion_array_1->add_value( n_k*(1+m)+iK, mvalue_1/double( ens->params.n_bead )*ens->get_sign() );
					
					*/
					
				}
				
				/*
				 * ### OpenMP ###
				boson_array->add_value( iK, integral*ens->params.epsilon/double( ens->params.n_bead ) );
				fermion_array->add_value( iK, ens->get_sign()*integral*ens->params.epsilon/double( ens->params.n_bead ) );
				
				boson_array_proton->add_value( iK, integral_proton*ens->params.epsilon/double( ens->params.n_bead ) );
				fermion_array_proton->add_value( iK, ens->get_sign()*integral_proton*ens->params.epsilon/double( ens->params.n_bead ) );
				
				boson_array_electron->add_value( iK, integral_electron*ens->params.epsilon/double( ens->params.n_bead ) );
				fermion_array_electron->add_value( iK, ens->get_sign()*integral_electron*ens->params.epsilon/double( ens->params.n_bead ) );

				boson_array_electron_proton->add_value( iK, integral_electron_proton*ens->params.epsilon/double( ens->params.n_bead ) );
				fermion_array_electron_proton->add_value( iK, ens->get_sign()*integral_electron_proton*ens->params.epsilon/double( ens->params.n_bead ) );
				
				boson_array_full->add_value( iK, integral_full*ens->params.epsilon/double( ens->params.n_bead ) );
				fermion_array_full->add_value( iK, ens->get_sign()*integral_full*ens->params.epsilon/double( ens->params.n_bead ) );
				
				boson_array_0->add_value( iK, integral_0*ens->params.epsilon/double( ens->params.n_bead ) );
				fermion_array_1->add_value( iK, ens->get_sign()*integral_1*ens->params.epsilon/double( ens->params.n_bead ) );
				*/
				
				
				
			} // end loop n_k

		}
		
		
		
		
		
		// Sequential writing of observables to the estimator structure
		for(int iK=0;iK<n_k;iK++)
		{
			boson_array->add_value( iK, OpenMP_mvalue[iK]*ens->params.epsilon/double( ens->params.n_bead ) );
			fermion_array->add_value( iK, ens->get_sign()*OpenMP_mvalue[iK]*ens->params.epsilon/double( ens->params.n_bead ) );
				
			boson_array_proton->add_value( iK, OpenMP_mvalue_proton[iK]*ens->params.epsilon/double( ens->params.n_bead ) );
			fermion_array_proton->add_value( iK, ens->get_sign()*OpenMP_mvalue_proton[iK]*ens->params.epsilon/double( ens->params.n_bead ) );
				
			boson_array_electron->add_value( iK, OpenMP_mvalue_electron[iK]*ens->params.epsilon/double( ens->params.n_bead ) );
			fermion_array_electron->add_value( iK, ens->get_sign()*OpenMP_mvalue_electron[iK]*ens->params.epsilon/double( ens->params.n_bead ) );

			boson_array_electron_proton->add_value( iK, OpenMP_mvalue_electron_proton[iK]*ens->params.epsilon/double( ens->params.n_bead ) );
			fermion_array_electron_proton->add_value( iK, ens->get_sign()*OpenMP_mvalue_electron_proton[iK]*ens->params.epsilon/double( ens->params.n_bead ) );
				
			boson_array_full->add_value( iK, OpenMP_mvalue_full[iK]*ens->params.epsilon/double( ens->params.n_bead ) );
			fermion_array_full->add_value( iK, ens->get_sign()*OpenMP_mvalue_full[iK]*ens->params.epsilon/double( ens->params.n_bead ) );
				
			boson_array_0->add_value( iK, OpenMP_mvalue_0[iK]*ens->params.epsilon/double( ens->params.n_bead ) );
			fermion_array_0->add_value( iK, ens->get_sign()*OpenMP_mvalue_0[iK]*ens->params.epsilon/double( ens->params.n_bead ) );
				
			boson_array_1->add_value( iK, OpenMP_mvalue_1[iK]*ens->params.epsilon/double( ens->params.n_bead ) );
			fermion_array_1->add_value( iK, ens->get_sign()*OpenMP_mvalue_1[iK]*ens->params.epsilon/double( ens->params.n_bead ) );
			
			
			
			for(int m=0;m<ens->params.n_bead;m++) // the integration loop over all slices
			{
					
				boson_array->add_value( n_k*(1+m)+iK, OpenMP_mvalue[n_k*(1+m)+iK]/double( ens->params.n_bead ) );
				fermion_array->add_value( n_k*(1+m)+iK, OpenMP_mvalue[n_k*(1+m)+iK]/double( ens->params.n_bead )*ens->get_sign() );
					
				boson_array_proton->add_value( n_k*(1+m)+iK, OpenMP_mvalue_proton[n_k*(1+m)+iK]/double( ens->params.n_bead ) );
				fermion_array_proton->add_value( n_k*(1+m)+iK, OpenMP_mvalue_proton[n_k*(1+m)+iK]/double( ens->params.n_bead )*ens->get_sign() );
					
				boson_array_electron->add_value( n_k*(1+m)+iK, OpenMP_mvalue_electron[n_k*(1+m)+iK]/double( ens->params.n_bead ) );
				fermion_array_electron->add_value( n_k*(1+m)+iK, OpenMP_mvalue_electron[n_k*(1+m)+iK]/double( ens->params.n_bead )*ens->get_sign() );
					
				boson_array_electron_proton->add_value( n_k*(1+m)+iK, OpenMP_mvalue_electron_proton[n_k*(1+m)+iK]/double( ens->params.n_bead ) );
				fermion_array_electron_proton->add_value( n_k*(1+m)+iK, OpenMP_mvalue_electron_proton[n_k*(1+m)+iK]/double( ens->params.n_bead )*ens->get_sign() );
					
				boson_array_full->add_value( n_k*(1+m)+iK, OpenMP_mvalue_full[n_k*(1+m)+iK]/double( ens->params.n_bead ) );
				fermion_array_full->add_value( n_k*(1+m)+iK, OpenMP_mvalue_full[n_k*(1+m)+iK]/double( ens->params.n_bead )*ens->get_sign() );
					
				boson_array_0->add_value( n_k*(1+m)+iK, OpenMP_mvalue_0[n_k*(1+m)+iK]/double( ens->params.n_bead ) );
				fermion_array_0->add_value( n_k*(1+m)+iK, OpenMP_mvalue_0[n_k*(1+m)+iK]/double( ens->params.n_bead )*ens->get_sign() );
					
				boson_array_1->add_value( n_k*(1+m)+iK, OpenMP_mvalue_1[n_k*(1+m)+iK]/double( ens->params.n_bead ) );
				fermion_array_1->add_value( n_k*(1+m)+iK, OpenMP_mvalue_1[n_k*(1+m)+iK]/double( ens->params.n_bead )*ens->get_sign() );
			
			
			
			} // end loop m
			
			
		} // end loop iK
		
		
		
		
		
		
		
		
		
		
		
		
		
	} // end measurement cycle condition
	
}


































