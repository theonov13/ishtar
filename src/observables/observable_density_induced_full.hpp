/*
 * Contains class: observable_density_induced_full
 */




template <class inter> class observable_density_induced_full
{
public:

	
	// Basic properties:
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
 	estimator_array_set boson_storage_0, fermion_storage_0;
	estimator_array_set boson_storage_1, fermion_storage_1;
	estimator_array_set boson_storage_2, fermion_storage_2;
	
	int n_buffer;  // buffer size
	int n_cycle; // number of configs to be skipped between measurements
	int cnt = 0; // config cycle counter
	
	
	std::vector<std::vector<double> > k_vectors; // store all k-vectors
	
	int n_k;
	double L;
	
	std::string bose_name = "boson_density_induced_full";
	std::string fermi_name = "fermion_density_induced_full";

	std::string bose_name_0 = "boson_density_induced_full_0";
	std::string fermi_name_0 = "fermion_density_induced_full_0";
	
	std::string bose_name_1 = "boson_density_induced_full_1";
	std::string fermi_name_1 = "fermion_density_induced_full_1";
	
	std::string bose_name_2 = "boson_density_induced_full_2";
	std::string fermi_name_2 = "fermion_density_induced_full_2";
	
	
	
	
	// Methods:
	observable_density_induced_full(){ } // TBD: implement constructor
	
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_buffer, int new_n_k, double new_L ); // initialize stuff
	
	void measure( config_ensemble* ens );

	bool write_all;

};




// initializes stuff
template <class inter> void observable_density_induced_full<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_buffer, int new_n_k, double new_L )
{
	n_cycle = new_n_cycle;
	
	n_buffer = new_n_buffer;

	write_all = new_write_all;
	
	n_k = new_n_k;
	L = new_L;
	
	create_k_vectors( &k_vectors, n_k, L );
	
	std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ full ~~~\n";
	for(int i=0;i<n_k;i++)
	{
		std::cout << "i: " << i;
		for(auto c=k_vectors[i].begin();c!=k_vectors[i].end();c++)
		{
			std::cout << "\t" << (*c);
		}
		std::cout << "\n";
	}
	std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ end ~~~\n";
	
	boson_storage.initialize( write_all, bose_name, n_k, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, n_k, new_n_buffer, binning_level );
	
	boson_storage_0.initialize( write_all, bose_name_0, n_k, new_n_buffer, binning_level );
	fermion_storage_0.initialize( write_all, fermi_name_0, n_k, new_n_buffer, binning_level );
	
	boson_storage_1.initialize( write_all, bose_name_1, n_k, new_n_buffer, binning_level );
	fermion_storage_1.initialize( write_all, fermi_name_1, n_k, new_n_buffer, binning_level );
	
	boson_storage_2.initialize( write_all, bose_name_2, n_k, new_n_buffer, binning_level );
	fermion_storage_2.initialize( write_all, fermi_name_2, n_k, new_n_buffer, binning_level );
	
}


// 

// perform the actual measurements
template <class inter> void observable_density_induced_full<inter>::measure( config_ensemble* ens )
{
		
	if( cnt < n_cycle ) // Check, if enough configs have been skipped
	{
		cnt++;
	}
	else
	{
		cnt = 0; // Reset cycle counter and perform the actual measurement
	
		if( ens->params.dim < 3 ) return; // At the moment only possible for 3D systems!
	
		// determine the particle number
		int N = ens->N_tot; // this is the total particle number of all species
		
		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer( N );
		estimator_array* fermion_array = fermion_storage.pointer( N );
		
		
		estimator_array* boson_array_0 = boson_storage_0.pointer( N );
		estimator_array* fermion_array_0 = fermion_storage_0.pointer( N );
		
		estimator_array* boson_array_1 = boson_storage_1.pointer( N );
		estimator_array* fermion_array_1 = fermion_storage_1.pointer( N );
		
		estimator_array* boson_array_2 = boson_storage_2.pointer( N );
		estimator_array* fermion_array_2 = fermion_storage_2.pointer( N );
		
		/*
		double ans = 0.0;
		double ans2 = 0.0;
		double ans3 = 0.0;
		*/
		
		std::vector<double> ans_vector( n_k, 0.0 );
		std::vector<std::vector<double>> SPECV( 3, ans_vector );
		
		int spec=0;
		for(auto c=ens->species_config.begin();c!=ens->species_config.end();c++) // loop over all particle species
		{
			for(int i=0;i<c->params.n_bead;i++) // loop over all the main slices
			{
				for(int k=0;k<c->beadlist[0].size();k++) // loop over all particles on a particular slice
				{
					int id = c->beadlist[i][k];
						
					// obtain the x,y,z coords
					double x_id = c->beads[id].get_coord( 0 );
					double y_id = c->beads[id].get_coord( 1 );
					double z_id = c->beads[id].get_coord( 2 );
					
					double scale = 1.0;//2.0*pi/ens->params.length;
					
					
					for(int kVec=0;kVec<n_k;kVec++) // loop over all the k-vectors to be measured
					{
						double contrib = cos( scale*(x_id*k_vectors[kVec][0] + y_id*k_vectors[kVec][1] + z_id*k_vectors[kVec][2]) );
						ans_vector[ kVec ] += contrib;
						
						
						SPECV[spec][ kVec ] += contrib;
						
						
					}
					
					
				}
			}
				
			spec++;
		} // end loop over all species
			
		// Submit values to the estimator_array_set
		double inv_fac = 1.0 / ( double( ens->params.n_bead) * ens->params.volume );
		
		
		for(int kVec=0;kVec<n_k;kVec++)
		{
			boson_array->add_value( kVec, ans_vector[kVec]*inv_fac );
			fermion_array->add_value( kVec, ans_vector[kVec]*inv_fac*ens->get_sign() );
			
			
			boson_array_0->add_value( kVec, SPECV[0][kVec]*inv_fac );
			fermion_array_0->add_value( kVec, SPECV[0][kVec]*inv_fac*ens->get_sign() );
			
			boson_array_1->add_value( kVec, SPECV[1][kVec]*inv_fac );
			fermion_array_1->add_value( kVec, SPECV[1][kVec]*inv_fac*ens->get_sign() );
			
			boson_array_2->add_value( kVec, SPECV[2][kVec]*inv_fac );
			fermion_array_2->add_value( kVec, SPECV[2][kVec]*inv_fac*ens->get_sign() );
		}
		

		/*
		
		boson_array->add_value(0, ans*inv_fac);
		fermion_array->add_value(0, ens->get_sign()*ans*inv_fac);
		
		boson_array->add_value(1, ans2*inv_fac);
		fermion_array->add_value(1, ans2*inv_fac*ens->get_sign() );
		
		boson_array->add_value(2, ans3*inv_fac);
		fermion_array->add_value(2, ans3*inv_fac*ens->get_sign() );
		
		*/
		
	} // end measurement cycle condition
	
}












