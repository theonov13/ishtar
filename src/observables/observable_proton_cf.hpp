/*
 * Contains class: observable_proton_cf
 * The 2-particle correlation function between particles from the same species
 */


template <class inter> class observable_proton_cf
{
public:

	
	// Basic properties:
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	double seg; // bin_width of the radial density
	int n_buffer;  // buffer size
	int n_seg; // number of bins for the radial density
	int n_cycle; // number of configs to be skipped between measurements
	int cnt = 0; // config cycle counter
	
	double inv_seg;
	
	
	std::string bose_name = "boson_proton_cf";
	std::string fermi_name = "fermion_proton_cf";
	
	// Methods:
	observable_proton_cf(){ } // TBD: implement constructor
	
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer, double new_seg ); // initialize stuff
	
	void measure( config_ensemble* ens, inter* my_interaction, int intra_species ); // perform the actual measurements
	
	
	
	// measurement properties:
	std::vector<double>tmp;
	std::vector<double>norm;
	
	bool write_all;


};




// initializes stuff
template <class inter> void observable_proton_cf<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer, double new_seg )
{
	n_cycle = new_n_cycle;
	n_seg = new_n_seg;
	n_buffer = new_n_buffer;
	seg = new_seg;
	
	
	inv_seg = 1.0/seg;
	
	
	
	tmp.resize(n_seg);

	write_all = new_write_all;
	
	boson_storage.initialize( write_all, bose_name, n_seg, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, n_seg, new_n_buffer, binning_level );
	
	
	// Calculate the volume of each segment:
	for(int i=0;i<n_seg;i++)
	{
		double r_min = double(i)*seg;
		double r_max = r_min+seg;
		double my_volume = 4.0*pi/3.0 * ( r_max*r_max*r_max - r_min*r_min*r_min );
		norm.push_back( my_volume );
	}
	
	
	
	
}




// perform the actual measurements, measure correlations within the species intra_species
template <class inter> void observable_proton_cf<inter>::measure( config_ensemble *ens, inter* my_interaction, int intra_species )
{	
	if( ens->species_config.size() < 3 ) return; // We need a third species for this
	
	
	if( cnt < n_cycle ) // Check, if enough configs have been skipped
	{
		cnt++;
	}
	else
	{
		cnt = 0; // Reset cycle counter and perform the actual measurement
	
		if( intra_species >= ens->species_config.size() ) return; // Only measure existing species!
	
	
		// obtain a pointer to the requested particle-species:
		worm_configuration* c = &(*ens).species_config[ intra_species ];
	
		// determine the particle number
 		int N = c->beadlist[ 0 ].size();
		int N_tot = ens->N_tot;
		
		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer(N_tot);
		estimator_array* fermion_array = fermion_storage.pointer(N_tot);

		
		// ##########################################################################################################################
		// ### Start::OpenMP parallelization part
		
		std::vector<std::vector<double>> OpenMP_vector( ens->params.n_bead, tmp );
		
		
		// Create a histogram of density bins
		
		#pragma omp parallel 
		{
			#pragma omp for	
			for(int i=0;i<c->params.n_bead;i++) // loop over all the main slices
			{
				for(int k=0;k<N;k++) // loop over all particles on a particular slice
				{
					int k_id = c->beadlist[ i ][ k ];
					
					for(int j=1+k;j<N;j++) // loop over all other particles for each particle
					{
						int j_id = c->beadlist[ i ][ j ];
						
						double my_r = sqrt( my_interaction->distance_sq( &(*c).beads[ k_id ], &(*c).beads[ j_id ] ) );
						
						int my_bin = my_r * inv_seg;
						if( my_bin < n_seg )
						{
							// ### OpenMP:: tmp[ my_bin ] += 1.0;
							OpenMP_vector[ i ][ my_bin ] += 1.0;
						}
						
						
					} // end loop over all other particles, j
						
					
					
					
				} // end loop over all particles, k
			} // end loop over all main slices
		
		
		
		}
		
		
		// ### OpenMP Post Processing Loop:
		
		for(int i=0;i<ens->params.n_bead;i++)
		{
			for(int k=0;k<tmp.size();k++)
			{
				tmp[ k ] += OpenMP_vector[ i ][ k ];
			}
		}
		
		
		
		

		// Submit value of each bin to the estimator_array_set
		for(auto i=tmp.begin();i!=tmp.end();i++)
		{
			// index of this bin
			int index = i - tmp.begin();
			
			double inv_fac = 2.0  / ( norm[ index ] * double( N*(N-1)*ens->params.n_bead ) );

			boson_array->add_value( index, (*i)*inv_fac );
			fermion_array->add_value( index, ens->get_sign()*(*i)*inv_fac );
			
			// Reset the tmp vector before the next measurement
			(*i) = 0.0;
		}


	} // end of the cycle measurement condition
	
}












