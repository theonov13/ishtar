/*
 * Contains class: observable_density_induced
 */


template <class inter> class observable_density_induced
{
public:

	
	// Basic properties:
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	int n_buffer;  // buffer size
	int n_cycle; // number of configs to be skipped between measurements
	int cnt = 0; // config cycle counter
	
	
	double qx, qy, qz;
	
	
	
	std::string bose_name = "boson_density_induced";
	std::string fermi_name = "fermion_density_induced";
	
	// Methods:
	observable_density_induced(){ } // TBD: implement constructor
	
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_buffer, double new_qx, double new_qy, double new_qz ); // initialize stuff
	
	void measure( config_ensemble* ens );

	bool write_all;

};




// initializes stuff
template <class inter> void observable_density_induced<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_buffer, double new_qx, double new_qy, double new_qz )
{
	n_cycle = new_n_cycle;

	qx = new_qx;
	qy = new_qy;
	qz = new_qz;
	
	n_buffer = new_n_buffer;

	write_all = new_write_all;
	
	boson_storage.initialize( write_all, bose_name, 3, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, 3, new_n_buffer, binning_level );
	
}


// 

// perform the actual measurements
template <class inter> void observable_density_induced<inter>::measure( config_ensemble* ens )
{
		
	if( cnt < n_cycle ) // Check, if enough configs have been skipped
	{
		cnt++;
	}
	else
	{
		cnt = 0; // Reset cycle counter and perform the actual measurement
	
		if( ens->params.dim < 3 ) return; // At the moment only possible for 3D systems!
	
		// determine the particle number
		int N = ens->N_tot; // this is the total particle number of all species
		
		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer( N );
		estimator_array* fermion_array = fermion_storage.pointer( N );
		
		double ans = 0.0;
		double ans2 = 0.0;
		double ans3 = 0.0;
		
		for(auto c=ens->species_config.begin();c!=ens->species_config.end();c++) // loop over all particle species
		{
			for(int i=0;i<c->params.n_bead;i++) // loop over all the main slices
			{
				for(int k=0;k<c->beadlist[0].size();k++) // loop over all particles on a particular slice
				{
					int id = c->beadlist[i][k];
						
					// obtain the x,y,z coords
					double x_id = c->beads[id].get_coord( 0 );
					double y_id = c->beads[id].get_coord( 1 );
					double z_id = c->beads[id].get_coord( 2 );
					
					double scale = 2.0*pi/ens->params.length;
						
					double contrib = cos( scale*(x_id*ens->params.pq_x + y_id*ens->params.pq_y + z_id*ens->params.pq_z) );
					ans += contrib;
					
					double contrib2 = cos( scale*(x_id*ens->params.pq_x2 + y_id*ens->params.pq_y2 + z_id*ens->params.pq_z2) );
					ans2 += contrib2;
					
					double contrib3 = cos( scale*(x_id*ens->params.pq_x3 + y_id*ens->params.pq_y3 + z_id*ens->params.pq_z3) );
					ans3 += contrib3;
				}
			}
				
				
		} // end loop over all species
			
		// Submit value to the estimator_array_set
		double inv_fac = 1.0 / ( double( ens->params.n_bead) * ens->params.volume );
		
		boson_array->add_value(0, ans*inv_fac);
		fermion_array->add_value(0, ens->get_sign()*ans*inv_fac);
		
		boson_array->add_value(1, ans2*inv_fac);
		fermion_array->add_value(1, ans2*inv_fac*ens->get_sign() );
		
		boson_array->add_value(2, ans3*inv_fac);
		fermion_array->add_value(2, ans3*inv_fac*ens->get_sign() );
		
	} // end measurement cycle condition
	
}












