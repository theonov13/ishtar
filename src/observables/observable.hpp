/*
 * Contains the class observable
 * -> contains all observable classes to be measured
 */



template <class inter> class observable
{
public:
	
	void init( Parameters* new_p, config_ensemble* new_ensemble );
	int measure();
	
	
private:

	int ref_cnt = 0; // counter for global "number of measurements"; in principle, different observables can have different buffer / number of measurements
	

	
	config_ensemble *ensemble;
	Parameters* p;



	
	observable_radial_density< inter > radial_density;
	
	observable_radial_density_improved< inter > radial_density_improved;
	
	observable_static_structure_factor< inter > static_structure_factor;

	observable_density_response_inter< inter > density_response_inter;

	
	
	
	observable_momentum< inter > momentum;
	observable_offdiagonal_density_matrix< inter > offdiagonal_density_matrix;
	observable_offdiagonal_sign< inter > offdiagonal_sign;
	
	observable_extended_information< inter > extended_information;
	
	
	
	
	observable_density_strip< inter > density_strip;
	observable_density_3D< inter > density_3D;
	
	observable_force_on_ions< inter > force_on_ions;
	
	observable_density_induced< inter > density_induced;
	observable_density_induced_full< inter > density_induced_full;
	
	
	observable_intra_cf< inter > intra_cf;
	observable_inter_cf< inter > inter_cf;
	
	observable_proton_electron_cf< inter > proton_electron_cf;
	observable_proton_cf< inter > proton_cf;
	
	observable_energy< inter > energy;
	
	observable_standard_PIMC_Kelbg_two_component< inter > standard_PIMC_Kelbg;
	
	
	observable_standard_PIMC_energy< inter > standard_PIMC_energy;
	observable_standard_PIMC_trap_superfluidity< inter > standard_PIMC_trap_superfluidity;
	observable_standard_PIMC_lsf< inter > standard_PIMC_lsf;
	
	observable_standard_PIMC_permutations_correlation< inter > standard_PIMC_permutations_correlation;
	
	observable_standard_PIMC_permutations< inter > standard_PIMC_permutations;
	observable_standard_PIMC_winding< inter > standard_PIMC_winding;
	
	
	observable_standard_PIMC_bogoliubov< inter > standard_PIMC_bogoliubov;
	observable_bogoliubov< inter > bogoliubov;
	

	observable_c2p< inter > c2p;
	observable_c2p_lsf< inter > c2p_lsf;
	
	
	// Monopole CF for the estimation of the quantum breathing mode
	observable_monopole_cf< inter > monopole_cf;
	
	


	
    // pair-approximation total energy
    observable_standard_PIMC_energy_pair_approx < inter > pair_approx_HEG_energy;


	
	


	
	inter my_interaction;
	
};



template <class inter> int observable<inter>::measure()
{

	// obtain diagonality criterion
	int diag_crit = ensemble->diag;
	
	if( p->simulation_type == 1 ) diag_crit = 1; // mPB-PIMC is always diagonal (atm)
	
	if( diag_crit ) // measure diagonal observables
	{
		
		if( p->simulation_type == 1 )
		{
			std::cout << "This simulation_type is not implemented in this version.\n";
			exit(1);



		}
		else // the observables for the multi-species PB-PIMC or standard PIMC:
		{
			// Energy estimator depends on the simulation_type:
			if( p->simulation_type >= p->n_multi ) // estimators specifically for standard PIMC (bosons, fermions, boltzmannons)
			{
				standard_PIMC_energy.measure( ensemble, &my_interaction );
				
				if( (p->system_type == 17 ) || (p->system_type == 19) || (p->system_type == 20) || (p->system_type == 14 ) || (p->system_type == 21) || (p->system_type == 22 ) )
				{
					standard_PIMC_Kelbg.measure( ensemble, &my_interaction );
				}
				
				if( (p->system_type == 11) || (p->system_type == 2) ) // for the Bogoliubov scheme:
				{
					standard_PIMC_bogoliubov.measure( ensemble, &my_interaction );
				}
				
				
				// For PBC:
				if( p->system_type >= p->n_traps ) standard_PIMC_winding.measure( ensemble );
				
				
				// For the trap:
				if( p->system_type < p->n_traps ) standard_PIMC_trap_superfluidity.measure( ensemble );
				if( p->system_type < p->n_traps ) standard_PIMC_lsf.measure( ensemble );
				
				if( p->monopole_control ) monopole_cf.measure( ensemble );
				
				if( p->permutation_control )
				{
					standard_PIMC_permutations.measure( ensemble );
					standard_PIMC_permutations_correlation.measure( ensemble );
				}
				
				
			}
			else // estimators specifically for PB-PIMC
			{
				energy.measure( ensemble, &my_interaction );
				
				if( (p->system_type == 11) || (p->system_type == 2) ) // for the Bogoliubov scheme:
				{
					bogoliubov.measure( ensemble, &my_interaction );
				}
				
				if( p->system_type < p->n_traps ) radial_density_improved.measure( ensemble );
			}
				

			// Radial density should only be measured for the harmonic trap
 			if( p->system_type < p->n_traps ) radial_density.measure( ensemble );
			
			
			// Only measure the static structure factor if this is explicitly wanted
			if( p->sk_control )
			{
				static_structure_factor.measure( ensemble );

				density_strip.measure( ensemble );
				density_induced.measure( ensemble );
				density_induced_full.measure( ensemble );

			}
			
			if( p->ITCF_control ){
				density_response_inter.measure( ensemble );
			}


			if(p->system_type == 14) // Check hydrogen here.
            {
                pair_approx_HEG_energy.measure(ensemble, &my_interaction);
            }
			




			
			
			
			if( p->measure_density_3D ) density_3D.measure( ensemble );
			
			if( ( p->system_type == 13 ) || ( p->system_type == 16 ) )
			{
				if( p->measure_force_on_ions ) force_on_ions.measure( ensemble, &my_interaction );
			}
			
			
			
			
			
			

			// Only measure the pair CF if this is explicitly wanted
			if( p->cf_control )
			{
				intra_cf.measure( ensemble, &my_interaction, 0 ); // Obtain the correlations within the species '0'
				inter_cf.measure( ensemble, &my_interaction );
				
				if( (p->system_type == 17) || (p->system_type == 21) || (p->system_type == 22 ) )
				{
					proton_electron_cf.measure( ensemble, &my_interaction );
					proton_cf.measure( ensemble, &my_interaction, 2 );
				}
			}
			
			
			
			if( p->c2p_control )
			{
				c2p.measure( &(*ensemble).species_config[0] );
				c2p_lsf.measure( &(*ensemble).species_config[0] );
			}
			
		} // end simulation_type condition
// 		


		if( p->simulation_type == 5 ) extended_information.measure( ensemble, &my_interaction, p->c_bar );


		// Return 1 if tau_ac configs have been counted and reset counter, otherwise return 0
		if( ref_cnt < p->tau_ac )
		{
			ref_cnt++;
			return 0;
		}
		else
		{
			ref_cnt = 0;
			return 1;
		}
		
	}
	else // measure off-diagonal observables (momentum distribution, Matsubara Green function is TBD)
	{
		
		if( p->simulation_type == 5 )
		{
		
			momentum.measure( ensemble );
			offdiagonal_density_matrix.measure( ensemble );
			offdiagonal_sign.measure( ensemble,  &my_interaction );
			
			
			extended_information.measure( ensemble, &my_interaction, p->c_bar );
			
			// Return 1 if tau_ac configs have been counted and reset counter, otherwise return 0
			if( ref_cnt < p->tau_ac )
			{
				ref_cnt++;
				return 0;
			}
			else
			{
				ref_cnt = 0;
				return 1;
			}
			
		}
		
		return 0;
	}
	
	
	
}



template <class inter> void observable<inter>::init( Parameters* new_p, config_ensemble* new_ensemble )
{
	p = new_p;

	
	int binning_level = p->binning_level;
	int n_buffer = p->n_buffer;
	int tau_ac = p->tau_ac;
	bool write_all = p->write_all;
	
	
	// For PB-PIMC / PIMC, possibly with multiple species:
	ensemble = new_ensemble;
	




	
	
	double seg = p->length / double( p->n_bins ); // width of an angular segment
	int n_energy_slots = 5; // number of slots in 'chin_standard'-estimator
	
	if( p->simulation_type == 1 )
	{
		std::cout << "simulation_type not implemented.\n";
		exit(1);
	}
	else // Initialize the observables for PB-PIMC / standard PIMC:
	{
		// Initialize the energy estimators depeneding on the simulation_type:
		if( p->simulation_type >= p->n_multi ) // Initialize the estimators specifically standard PIMC:
		{
			if( p->permutation_control )
			{
				standard_PIMC_permutations.init( binning_level, write_all, tau_ac, ensemble->N_tot+1, n_buffer );
				standard_PIMC_permutations_correlation.init( binning_level, write_all, tau_ac, ensemble->N_tot * ensemble->N_tot, n_buffer );
			}
				
			standard_PIMC_winding.init( binning_level, write_all, tau_ac, p->dim, n_buffer );
			standard_PIMC_energy.init( binning_level, write_all, tau_ac, n_energy_slots, n_buffer );
			
			if( (p->system_type == 17 ) || (p->system_type == 19) || (p->system_type == 20) || (p->system_type == 14 ) || (p->system_type == 21) || (p->system_type == 22 ) )
			{
				standard_PIMC_Kelbg.init( binning_level, write_all, tau_ac, n_energy_slots+1+4, n_buffer );
			}
			
            if(p->system_type == 14)
            {
                pair_approx_HEG_energy.init(binning_level, write_all, tau_ac, n_energy_slots, n_buffer);
            }

			if( (p->system_type == 11) || (p->system_type == 2) ) // for the Bogoliubov scheme:
			{
				standard_PIMC_bogoliubov.init( binning_level, write_all, tau_ac, n_energy_slots+1, n_buffer );
			}
			
			
			
			standard_PIMC_trap_superfluidity.init( binning_level, write_all, tau_ac, 2, n_buffer );
			standard_PIMC_lsf.init( binning_level, write_all, tau_ac, p->n_bins, n_buffer, seg );
			std::cout << "All observables for standard PIMC have been succesfully initialized. \n";
		}
		else // Initialize specifically for PB-PIMC
		{
			energy.init( binning_level, write_all, tau_ac, n_energy_slots, n_buffer );
			
			if( (p->system_type == 11) || (p->system_type == 2) ) // for the Bogoliubov scheme:
			{
				bogoliubov.init( binning_level, write_all, tau_ac, n_energy_slots+1, n_buffer );
			}
			
			
			if( p->system_type < p->n_traps ) radial_density_improved.init( binning_level, write_all, tau_ac, p->n_bins, n_buffer, seg );
		}
		
		
		// Initialization of the diagonal estimators is equal for PB-PIMC / standard_PIMC 
		
		if( p->system_type < p->n_traps ) radial_density.init( binning_level, write_all, tau_ac, p->n_bins, n_buffer, seg ); // radial density for trap systems

		// static structure factor
		if( p->sk_control )
		{
			static_structure_factor.init( binning_level, write_all, tau_ac, p->n_bins, p->length, n_buffer );


			density_strip.init( binning_level, write_all, tau_ac, p->n_bins, n_buffer, seg );


			double qx = 2.0*pi*p->pq_x/p->length;
			double qy = 2.0*pi*p->pq_y/p->length;
			double qz = 2.0*pi*p->pq_z/p->length;

			density_induced.init( binning_level, write_all, tau_ac, n_buffer, qx, qy, qz );
			density_induced_full.init( binning_level, write_all, tau_ac, n_buffer, p->n_bins, p->length );


		}
		
		if( p->simulation_type == 5 ) 
		{
			momentum.init( binning_level, write_all, tau_ac, p->n_bins, p->length, n_buffer );
			offdiagonal_density_matrix.init( binning_level, write_all, tau_ac, p->n_bins, p->length, n_buffer );
			offdiagonal_sign.init( binning_level, write_all, tau_ac, n_energy_slots, n_buffer );
			extended_information.init( binning_level, write_all, tau_ac, n_energy_slots, n_buffer );
		}
		
		if( p->ITCF_control )
		{
			density_response_inter.init( binning_level, p->n_bead, write_all, tau_ac, p->n_bins, p->length, n_buffer );
		}
			

		
		
		if( p->measure_density_3D ) density_3D.init( binning_level, write_all, tau_ac, p->n_bins, n_buffer, seg );
		
		if( ( p->system_type == 13 ) || ( p->system_type == 16 ) )
		{
			if( p->measure_force_on_ions ) force_on_ions.init( binning_level, write_all, tau_ac, p->dim*p->ions.get_num_ions(), n_buffer );
		}
		
		
		// Pair correlation functions
		if( p->cf_control )
		{
			intra_cf.init( binning_level, write_all, tau_ac, p->n_bins, n_buffer, seg );
			inter_cf.init( binning_level, write_all, tau_ac, p->n_bins, n_buffer, seg );
			
			if( (p->system_type == 17) || (p->system_type == 21) || (p->system_type == 22 ) )
			{
				proton_electron_cf.init( binning_level, write_all, tau_ac, p->n_bins, n_buffer, seg );
				proton_cf.init( binning_level, write_all, tau_ac, p->n_bins, n_buffer, seg );
			}
			
		}
		
		
		// Center-2-Particle CF
		if( p->c2p_control )
		{
			c2p.init( binning_level, write_all, tau_ac, p->n_bins, p->c2p_buffer, &(*ensemble).species_config[0] );
			c2p_lsf.init( binning_level, write_all, tau_ac, p->n_bins, p->c2p_buffer, &(*ensemble).species_config[0] );
		}
		
		// monopole cf for estimation of the quantum breathing mode (traps)
		if( p->monopole_control )
		{
			monopole_cf.init( binning_level, p->n_bead, write_all, tau_ac, n_buffer );
		}
		
	}

	// end simulation_type condition

	// Initialize the interaction class
	my_interaction.init( p );
	
}

