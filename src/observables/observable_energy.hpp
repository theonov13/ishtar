/*
 * Contains class: observable_energy
 */


template <class inter> class observable_energy
{
public:

	// Basic properties:
	
 	estimator_array_set boson_storage, fermion_storage; // estimator_arrays for each sector
	
	int n_buffer;  // buffer size
	int n_energy; // number of slots
	int n_cycle; // number of configs to be skipped between measurements
	
	int cnt = 0; // cycle counter for configs
	
	
	std::string bose_name = "boson_chin_standard";
	std::string fermi_name = "fermion_chin_standard";
	
	// Methods:
	observable_energy(){ } // TBD: implement constructor
	void init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer  );
	
	
	void measure( config_ensemble *ensemble, inter* my_interaction ); // perform the actual measurements
	
	
	bool write_all;
	

};




// constructor:: initializes stuff
template <class inter> void observable_energy<inter>::init( int binning_level, bool new_write_all, int new_n_cycle, int new_n_seg, int new_n_buffer )
{
	n_cycle = new_n_cycle;
	n_energy = new_n_seg;
	n_buffer = new_n_buffer;
	
	write_all = new_write_all;

	boson_storage.initialize( write_all, bose_name, new_n_seg, new_n_buffer, binning_level );
	fermion_storage.initialize( write_all, fermi_name, new_n_seg, new_n_buffer, binning_level );

	
}




template <class inter> void observable_energy<inter>::measure(config_ensemble* ensemble, inter* my_interaction)
{
	if( cnt < n_cycle )
	{
		cnt++;
	}
	else{
		cnt = 0;
	
	
		// determine the total number of particles of all species
		int N = (*ensemble).N_tot;
		
		
		// obtain a pointer to the appropriate estimator_array within the storage:
		estimator_array* boson_array = boson_storage.pointer(N);
		estimator_array* fermion_array = fermion_storage.pointer(N);

		int n_bead = (*ensemble).params.n_bead;
		double signum = (*ensemble).get_sign();
	
		(*boson_array).add_value(0, signum);
		(*fermion_array).add_value(0, signum);

		double epsilon = (*ensemble).params.beta/double((*ensemble).params.n_bead);
		
		// First term
		double pre = 3.0 * (*ensemble).params.dim * N *0.50 / epsilon;
	
	
		double lambda_t1_sq = 2.0*pi*epsilon*(*ensemble).params.t1;
		double lambda_t0_sq = 2.0*pi*epsilon*2.0*(*ensemble).params.t0;
		
		double lambda_t1 = sqrt( lambda_t1_sq );
		double lambda_t0 = sqrt( lambda_t0_sq );
		
		double inv_dim_t1 = 1.0 / pow( lambda_t1, (*ensemble).params.dim );
		double inv_dim_t0 = 1.0 / pow( lambda_t0, (*ensemble).params.dim );
		
		double ans_k = 0.0;
		double ans_kA = 0.0;
		double ans_kB = 0.0;

	
		for(int k=0;k<(*ensemble).params.n_bead;k++) // loop over all propagators
		{
			for(int iSpecies=0;iSpecies<(*ensemble).n_species;iSpecies++) // loop over all species
			{
			
				worm_configuration* c = &(*ensemble).species_config[iSpecies];
			
				// Calculate the inverse matrices
				gsl_matrix *inv_k, *inv_kA, *inv_kB;
			
				int my_n = (*c).params.N;

				
				inv_k  = gsl_matrix_calloc( my_n, my_n );
				inv_kA  = gsl_matrix_calloc( my_n, my_n );
				inv_kB  = gsl_matrix_calloc( my_n, my_n );
			
				(*c).matrix_list[k].inverse(inv_k);
				(*c).matrix_list_A[k].inverse(inv_kA);
				(*c).matrix_list_B[k].inverse(inv_kB);

				// Obtain the index of the next propagator
				int k_plus = k + 1;
				if(k_plus == (*c).params.n_bead)
				{
					k_plus = 0;
				}
			

				for(int kappa=0;kappa<(*c).params.N;kappa++) // loop over all particles
				{
					for(int xi=0;xi<(*c).params.N;xi++) // loop over all particles for each particle
					{
						
						
						
						// Usual imaginary time kinetic part for infinite systems (traps)
						
						if( (*c).params.system_type < c->params.n_traps )
						{
						
							double psi_k_ele = (*c).matrix_list[k].get_value(xi, kappa) * gsl_matrix_get(inv_k, kappa, xi);

							int id_k_kappa = (*c).beadlist[k][kappa];
							int id_kA_xi = (*c).beadlist_A[k][xi];
							
							double k_diff = (*my_interaction).distance_sq( &(*c).beads[id_k_kappa], &(*c).beads[id_kA_xi] );
							ans_k += psi_k_ele * k_diff;
							

//  							std::cout << "psi_k_ele: " << psi_k_ele << "\t k_diff: " << k_diff << "\t inv: " << gsl_matrix_get(inv_k, kappa, xi) << "\t element: " << (*c).matrix_list[k].get_value(xi, kappa)  << "\n";
		
							
							double psi_kA_ele = (*c).matrix_list_A[k].get_value(xi, kappa) * gsl_matrix_get(inv_kA, kappa, xi);
							
							int id_kA_kappa = (*c).beadlist_A[k][kappa];
							int id_kB_xi = (*c).beadlist_B[k][xi];
							
							double kA_diff = (*my_interaction).distance_sq( &(*c).beads[id_kA_kappa], &(*c).beads[id_kB_xi] );
							ans_kA += psi_kA_ele * kA_diff;
							
							
							
							

							double psi_kB_ele = (*c).matrix_list_B[k].get_value(xi, kappa) * gsl_matrix_get(inv_kB, kappa, xi);
							
							int id_kB_kappa = (*c).beadlist_B[k][kappa];
							int id_kplus_xi = (*c).beadlist[k_plus][xi];
							
							double kB_diff = (*my_interaction).distance_sq( &(*c).beads[id_kB_kappa], &(*c).beads[id_kplus_xi] );
							ans_kB += psi_kB_ele * kB_diff;
						
						}
						else // kinetic part for the box (e.g. HEG)
						{
							
							double sum_k = 0.0;
							double sum_kA = 0.0;
							double sum_kB = 0.0;
							
							int id_k_kappa = (*c).beadlist[k][kappa];
							int id_kA_xi = (*c).beadlist_A[k][xi];
							
							int id_kA_kappa = (*c).beadlist_A[k][kappa];
							int id_kB_xi = (*c).beadlist_B[k][xi];
							
							int id_kB_kappa = (*c).beadlist_B[k][kappa];
							int id_kplus_xi = (*c).beadlist[k_plus][xi];
							
							
							
							// loop over all the images:
							
							std::vector<double>R(3,0.0);
							
							for(int nx=-(*c).params.n_boxes;nx<(*c).params.n_boxes+1;nx++)
							{
								double R_x = nx * (*c).params.length;
								R[0] = R_x;
								for(int ny=-(*c).params.n_boxes;ny<(*c).params.n_boxes+1;ny++)
								{
									double R_y = ny * (*c).params.length;
									R[1] = R_y;
									for(int nz=-(*c).params.n_boxes;nz<(*c).params.n_boxes+1;nz++)
									{
										double R_z = nz * (*c).params.length;
										R[2] = R_z;
							
										double k_tmp = 0.0;
										double kA_tmp = 0.0;
										double kB_tmp = 0.0;
										
										for(int iDim=0;iDim<(*c).params.dim;iDim++)
										{
											double a_tmp = (*c).beads[id_k_kappa].get_coord(iDim) - (*c).beads[id_kA_xi].get_coord(iDim) + R[iDim];
											k_tmp += a_tmp * a_tmp;
											
											a_tmp = (*c).beads[id_kA_kappa].get_coord(iDim) - (*c).beads[id_kB_xi].get_coord(iDim) + R[iDim];
											kA_tmp += a_tmp * a_tmp;
											
											a_tmp = (*c).beads[id_kB_kappa].get_coord(iDim) - (*c).beads[id_kplus_xi].get_coord(iDim) + R[iDim];
											kB_tmp += a_tmp * a_tmp;
										}
										
										sum_k += k_tmp * exp( -pi * k_tmp / lambda_t1_sq );
										sum_kA += kA_tmp * exp( -pi * kA_tmp / lambda_t1_sq );
										sum_kB += kB_tmp * exp( -pi * kB_tmp / lambda_t0_sq );
										
										
										
									} // end nz
								}
							} // end nx
							
							// The invs are commented out to compensate for the "missing normalization" in Rho() from the interaction class
							ans_k += sum_k * gsl_matrix_get(inv_k, kappa, xi) * inv_dim_t1;
							ans_kA += sum_kA * gsl_matrix_get(inv_kA, kappa, xi) * inv_dim_t1;
							ans_kB += sum_kB * gsl_matrix_get(inv_kB, kappa, xi) * inv_dim_t0;
							
							
						} // end of system type condition

						

					} // end of loop over all particles for each particle
				} // end of loop over all particles
				

				// Free the inverse matrices
				gsl_matrix_free(inv_k);
				gsl_matrix_free(inv_kA);
				gsl_matrix_free(inv_kB);
				
			} // end loop over species
			
		} // end loop over all propagators
	

	
		ans_k = ans_k * pi/(epsilon * double((*ensemble).params.n_bead) * lambda_t1_sq);
		ans_kA = ans_kA * pi/(epsilon * double((*ensemble).params.n_bead) * lambda_t1_sq);
		ans_kB = ans_kB * pi/(epsilon * double((*ensemble).params.n_bead) * lambda_t0_sq);
		
		
//  		std::cout << "ans_k: " << ans_k << "\t ans_kA: " << ans_kA << "\t ans_kB: " << ans_kB << "\t pre: " << pre << "\n";
		
		
		// Combination of terms involving interactions
		double my_energy = (*ensemble).total_energy;
		double my_force_sq = (*ensemble).total_force_sq;
		double poti = ( my_energy + 3.0*epsilon*epsilon * (*ensemble).params.u0 * my_force_sq ) / double(n_bead) ;

		// total energy
		double imba = pre + poti - (ans_k + ans_kA + ans_kB);

		// Potential energy
		double total_potential = ( my_energy + 2.0*epsilon*epsilon * (*ensemble).params.u0 * my_force_sq ) / double(n_bead);
		
		// Kinetic energy
		double total_kinetic =  pre - (ans_k + ans_kA + ans_kB) + epsilon*epsilon * (*ensemble).params.u0 * my_force_sq  / double(n_bead) ;
	
	
		// Slot 1 is empty at the moment
		// TBD: maybe interaction energy in standard diagonal form?
		// Let's keep track of the sign of a single species, e.g. only spin-up electrons
		double v_ext = 0.0;

		
// 		(*boson_array).add_value( 1, (*ensemble).species_config[0].my_sign ); 
// 		(*fermion_array).add_value( 1, (*ensemble).species_config[0].my_sign);
		
		boson_array->add_value( 1, (ans_k + ans_kA + ans_kB) );
		fermion_array->add_value( 1, (ans_k + ans_kA + ans_kB)*signum );
		
// 		
		(*boson_array).add_value(2,total_potential); 
		(*fermion_array).add_value(2,signum*total_potential);
		
		(*boson_array).add_value(3,total_kinetic);
		(*boson_array).add_value(4,imba);
		
		(*fermion_array).add_value(3,signum*total_kinetic);
		(*fermion_array).add_value(4,signum*imba);

	}

}
