.. _introduction:

Introduction
************

.. image:: https://gitlab.com/ishtar_QMC/ishtar/-/raw/main/docs/_static/logo/logo.png
   :class: only-light

.. image:: https://gitlab.com/ishtar_QMC/ishtar/-/raw/main/docs/_static/logo/logo.png
   :class: only-dark

.. image:: https://img.shields.io/badge/language-C++-blue
   :target: https://cplusplus.com/

.. image:: https://img.shields.io/badge/license-Apache2.0-yellowgreen
   :target: https://gitlab.com/ishtar_QMC/ishtar/-/raw/main/LICENSE

.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.10497098.svg
   :target: https://doi.org/10.5281/zenodo.10497098


**I** maginary-time **S** tochastic **H** igh-performance **T** ool for **A** b initio **R** esearch (ISHTAR)

The ISHTAR code offers a vast variarity   
of path-integral Monte-Carlo (PIMC) implementations   
with the possibility to calculate observables    
like the static structure factor,   
the static dielectric function,   
the local field correction and many more.   
