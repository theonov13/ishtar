.. _installation:

Installation
************

| The code is written using C++ (C++17 standard).
| The following packages are needed for a minimum working installation

* `BOOST <https://www.boost.org/>`_
* `GSL <https://www.gnu.org/software/gsl/>`_
* `HDF5 <https://www.hdfgroup.org/solutions/hdf5/>`_

All packages are publicly visible.

Installation with cmake
=======================

The `package <https://gitlab.com/ishtar_QMC/ishtar>`_ can be installed with

.. code-block:: console

   git clone https://gitlab.com/ishtar_QMC/ishtar
   cd ishtar/src/
   mkdir build 
   cd build 
   cmake .. 
   make 

