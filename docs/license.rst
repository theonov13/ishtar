.. _license:

License
*******

| ISHTAR is licensed under the Apache 2.0 License. The license text can be found below.
| All dependencies and extra packages are publicly visible.
| Those packages and licences are

.. list-table::
   :widths: 50 50
   :header-rows: 1

   * - Package
     - License
   * - ISTHAR 
     - Apache-2.0
   * - BOOST 
     - Boost Software License
   * - GSL 
     - MIT License
   * - HDF5 
     - BSD-style Open Source 

Apache License
==============

.. literalinclude:: ../LICENSE
   :language: text
